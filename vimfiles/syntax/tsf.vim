if exists("b:current_syntax")
    finish
endif

set enc=utf8

let b:current_syntax = "tsf"

syn keyword tsfwords sql left right top bottom bpm newpage small smaller tiny
highlight link tsfwords Keyword

syn match tsfverse /^verse:/
highlight link tsfverse Keyword

syn match tsfkey /^[CKLNTXZcsw]:/
highlight link tsfkey Keyword

syn match tsfMeasure /!/
highlight link tsfMeasure Identifier

"syn match nn /:[drmfsltdb-]\=[ei]\=/
"highlight link nn Constant

syn match an /;[drmfsltdb-]\=[ei]\=/ contained
highlight link an Constant

syn match mn /![drmfsltdb-]\=[ei]\=/ contained
highlight link mn Identifier

syn match hn /\.[drmfsltdb-]\=[ei]\=/ contained
highlight link hn PreProc


syn match dmn /!![drmfsltdb-]\=[ei]\=/ contained
highlight link dmn Type
			
syn match ul /[_=][^ ]*/ contained
highlight link ul Type 

syn match notesline /^[^:]*\(:[^:]\+\)\{4,\}/  contains=an,mn,hn,dmn,ul
highlight link notesline Text 

syn match vspace /^[0-9]\+$/
highlight link vspace Type 
