package de.jm.tonic.gui;

import org.junit.Test;

import java.io.File;

import static org.junit.Assert.*;

public class TonicToTest {
    @Test
    public void testGetFilenameWithoutSuffix() {
        File midiFile = TonicTo.getMidiFile("name.suffix");
        assertEquals("name.mid", midiFile.getName());
        assertEquals(null, midiFile.getParent());


        midiFile = TonicTo.getMidiFile("name");
        assertEquals("name.mid", midiFile.getName());
        assertEquals(null, midiFile.getParent());

        midiFile = TonicTo.getMidiFile("hello/name.suffix");
        assertEquals("name.mid", midiFile.getName());
        assertEquals("hello", midiFile.getParent());

        midiFile = TonicTo.getMidiFile("hello/name");
        assertEquals("name.mid", midiFile.getName());
        assertEquals("hello", midiFile.getParent());
    }

}