package de.jm.tonic;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by johann on 16.07.17.
 *
 */
public class Notes2LatexTest {
    private Notes2Latex notes2Latex;

    @Before
    public void setUp() throws Exception {
        notes2Latex = new Notes2Latex();
    }

    /*
          d r m f s l t di ri fi si li ra ma sa la ta
          :d :
          ;d :
          !d :
          !!d :

          !d :r ;m :f !
          !d :r ;m :f
         */
    @Test
    public void testSplitIntoQuarters() {
        String line = "!d :r ;m :f ";
        List<String> expected = Arrays.asList("!d ", ":r ", ";m ", ":f ");
        List<String> parts = notes2Latex.splitIntoQuarters(line);
        assertEquals(expected, parts);

        line = "!d :r ;m :f";
        expected = Arrays.asList("!d ", ":r ", ";m ", ":f");
        parts = notes2Latex.splitIntoQuarters(line);
        assertEquals(expected, parts);

        line = "!d .d :r ;m :f ,f .d ";
        expected = Arrays.asList("!d .d ", ":r ", ";m ", ":f ,f .d ");
        parts = notes2Latex.splitIntoQuarters(line);
        assertEquals(expected, parts);

        line = "!d .d:r;m:f ,f .d";
        expected = Arrays.asList("!d .d", ":r", ";m", ":f ,f .d");
        parts = notes2Latex.splitIntoQuarters(line);
        assertEquals(expected, parts);

        line = "!d !!";
        expected = Arrays.asList("!d ", "!!");
        parts = notes2Latex.splitIntoQuarters(line);
        assertEquals(expected, parts);

        line = ": ";
        expected = Arrays.asList(": ");
        parts = notes2Latex.splitIntoQuarters(line);
        assertEquals(expected, parts);
    }


    @Test
    public void testLexQuarter() {
        String quarter;
        List<String> expected;
        List<String> parts;

        quarter = ":d'_1.5";
        expected = Arrays.asList("\\nhu[1.5]{d} ");
        parts = notes2Latex.lexQuarter(quarter);
        assertEquals(expected, parts);

        quarter = ":d";
        expected = Arrays.asList("\\nn{d} ");
        parts = notes2Latex.lexQuarter(quarter);
        assertEquals(expected, parts);

        quarter = ":d .d";
        expected = Arrays.asList("\\nn{d} ", "\\hn{d} ");
        parts = notes2Latex.lexQuarter(quarter);
        assertEquals(expected, parts);

        quarter = ":d ,d .-";
        expected = Arrays.asList("\\nn{d} ", "\\qn{d} ", "\\hc ");
        parts = notes2Latex.lexQuarter(quarter);
        assertEquals(expected, parts);

        quarter = ":d'";
        expected = Arrays.asList("\\nh{d} ");
        parts = notes2Latex.lexQuarter(quarter);
        assertEquals(expected, parts);

        quarter = ":d,";
        expected = Arrays.asList("\\nl{d} ");
        parts = notes2Latex.lexQuarter(quarter);
        assertEquals(expected, parts);

        quarter = ":d, ,d .-";
        expected = Arrays.asList("\\nl{d} ", "\\qn{d} ", "\\hc ");
        parts = notes2Latex.lexQuarter(quarter);
        assertEquals(expected, parts);

        quarter = ":d, * * ";
        expected = Arrays.asList("\\nl{d} ", "& ", "& ");
        parts = notes2Latex.lexQuarter(quarter);
        assertEquals(expected, parts);
    }

    @Test
    public void noteTypeTest() {
        String note = "!!d";
        String expected = "!!";
        String type = notes2Latex.noteType(note);
        assertEquals(expected, type);

        note = "!d";
        expected = "!";
        type = notes2Latex.noteType(note);
        assertEquals(expected, type);

        note = ";d";
        expected = ";";
        type = notes2Latex.noteType(note);
        assertEquals(expected, type);

        note = ":d";
        expected = ":";
        type = notes2Latex.noteType(note);
        assertEquals(expected, type);

        note = ".,d";
        expected = ".,";
        type = notes2Latex.noteType(note);
        assertEquals(expected, type);

        note = ".d";
        expected = ".";
        type = notes2Latex.noteType(note);
        assertEquals(expected, type);

        note = "/d";
        expected = "/";
        type = notes2Latex.noteType(note);
        assertEquals(expected, type);

        note = ",d";
        expected = ",";
        type = notes2Latex.noteType(note);
        assertEquals(expected, type);
    }

    @Test
    public void testLexNote() {
        String[] notes;
        String[] expect;

        notes  = new String[] {":d_2 "     , ":d=2.5 "};
        expect = new String[] {"\\nnu[2]{d} ", "\\nnp[2.5]{d} "};

        assertNotes(notes, expect);

        notes  = new String[] {":d"      , ";r"      , "!m"      , "!!f"     , ".,s"      , ".l"      , "/t"   , ",d" };
        expect = new String[] {"\\nn{d} ", "\\an{r} ", "\\mn{m} ", "\\dn{f} ", "\\hqn{s} ", "\\hn{l} ", "\\tn{t} ", "\\qn{d} " };

        assertNotes(notes, expect);

        notes  = new String[] {":d'"     , ";r'"     , "!m'"     , "!!f'"    , ".,s'"     , ".l'"     , "/t'"     , ",d'" };
        expect = new String[] {"\\nh{d} ", "\\ah{r} ", "\\mh{m} ", "\\dh{f} ", "\\hqh{s} ", "\\hh{l} ", "\\th{t} ", "\\qh{d} " };

        assertNotes(notes, expect);

        notes  = new String[] {",d,", ":d,"     , ";r,"     , "!m,"     , "!!f,"    , ".,s,"     , ".l,"     , "/t,"     , ",d," };
        expect = new String[] {"\\ql{d} ", "\\nl{d} ", "\\al{r} ", "\\ml{m} ", "\\dl{f} ", "\\hql{s} ", "\\hl{l} ", "\\tl{t} ", "\\ql{d} " };

        assertNotes(notes, expect);

        notes  = new String[] {":-"     , ": "};
        expect = new String[] {"\\nc ", "\\nn{} "};

        assertNotes(notes, expect);

        notes  = new String[] {":d_"     , ":d= "};
        expect = new String[] {"\\nnu[1]{d} ", "\\nnp[1]{d} "};
        assertNotes(notes, expect);

        notes  = new String[] {":d*", "!m,*"};
        expect = new String[] {"\\dnn{d} ", "\\dml{m} "};
        assertNotes(notes, expect);

        notes  = new String[] {":d*", "!d'**", ":d'+.t", "!d,+.,t'", ";d'+.l+,r*"};
        expect = new String[] {"\\dnn{d} ", "\\tmh{d} ", "\\nn{\\hi{d}\\hs{t}} ", "\\mn{\\lo{d}\\hs\\qs{\\hi{t}}} ",
                "\\dan{\\hi{d}\\hs{l}\\qs{r}} "};
        assertNotes(notes, expect);
    }

    private void assertNotes(String[] notes, String[] expect) {
        for (int i = 0; i < notes.length; i++) {
            String note = notes[i];
            String expected = expect[i];
            String latex = notes2Latex.lexNote(note);
            // todo assertEquals(expected, latex);
        }
    }

    @Test
    public void testLexNotes() {
        String line = "!d :r' .m, ;f ,s .- : ";
        String expected = "\\mn{d} \\nh{r} \\hl{m} \\an{f} \\qn{s} \\hc \\nn{} ";
        String latex = notes2Latex.lexNotes(line);
        assertEquals(expected, latex);

        line = "!d  :r'  .m, ;f ,s .- : ";
        expected = "\\mn{d} \\nh{r} \\hl{m} \\an{f} \\qn{s} \\hc \\nn{} ";
        latex = notes2Latex.lexNotes(line);
        assertEquals(expected, latex);
    }

    @Test
    public void testKeyChange2Latex() {
        String note = "r-l";
        String expected = "\\hi{r}l";
        String latex = notes2Latex.keyChange2latex(note, "");

        //assertEquals(expected, latex);

    }

    @Test
    public void testGetPlainNote() {
        String note;
        String latex;
        String expected;
        Notes2Latex notes2Latex = new Notes2Latex();

        note = "d";
        expected = "d";
        latex = notes2Latex.getPlainNote(note);
        assertEquals(expected, latex);

        note = "r'";
        expected = "\\hi{r}";
        latex = notes2Latex.getPlainNote(note);
        assertEquals(expected, latex);

        note = "m''";
        expected = "\\hi[2]{m}";
        latex = notes2Latex.getPlainNote(note);
        assertEquals(expected, latex);

        note = "f,";
        expected = "\\lo{f}";
        latex = notes2Latex.getPlainNote(note);
        assertEquals(expected, latex);

        note = "s,,";
        expected = "\\lo[2]{s}";
        latex = notes2Latex.getPlainNote(note);
        assertEquals(expected, latex);
    }

    @Test
    public void testSubNotes2latex() {
        String note;
        String latex;
        String expected;
        Notes2Latex notes2Latex = new Notes2Latex();

        note = "s+.r";
        expected = "\\pn{s\\hs{r}} ";
        latex = notes2Latex.subNotes2latex(note, new StringBuilder());
        assertEquals(expected, latex);

        note = "s,+,r";
        expected = "\\pn{\\lo{s}\\qs{r}} ";
        latex = notes2Latex.subNotes2latex(note, new StringBuilder());
        assertEquals(expected, latex);

        note = "s''+d,";
        expected = "\\pn{\\hi[2]{s}\\lo{d}} ";
        latex = notes2Latex.subNotes2latex(note, new StringBuilder());
        assertEquals(expected, latex);

    }

}

