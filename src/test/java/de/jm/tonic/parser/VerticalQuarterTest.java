package de.jm.tonic.parser;

import de.jm.tonic.model.TSFNote;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class VerticalQuarterTest {

    @Test
    public void getWidth() {
        VerticalQuarter quarter = new VerticalQuarter();
        List<TSFNote> tsfNotes;
        TSFNote note;

        tsfNotes = new ArrayList<>();
        note = new TSFNote("d", TSFNote.LENGTH.THREEQUARTES, 0, TSFNote.Accent.NORMAL);
        tsfNotes.add(note);
        note = new TSFNote("m", TSFNote.LENGTH.QUARTER, 0, TSFNote.Accent.LENGTH);
        note.setPrefix(",,");
        tsfNotes.add(note);

        quarter.add(tsfNotes);

        int width = quarter.getWidth();
        int expected = VerticalQuarter.FULL | VerticalQuarter.HALF;

        assertEquals(expected, width);
    }
}
