package de.jm.tonic.parser;

import de.jm.tonic.model.TSFNote;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class QuarterIteratorTest {

    @Test
    public void next() {
        List<TSFNote> quarter;
        List<TSFNote> notes;
        QuarterIterator iter;

        notes = new ArrayList<>();
        iter = new QuarterIterator(notes);
        assertFalse(iter.hasNext());

        notes = new ArrayList<>();
        notes.add(new TSFNote("d", TSFNote.LENGTH.FULL, 0, TSFNote.Accent.MEASURE));
        iter = new QuarterIterator(notes);
        assertTrue(iter.hasNext());
        quarter = iter.next();
        //System.out.println(quarter);

        notes = new ArrayList<>();
        notes.add(new TSFNote("d", TSFNote.LENGTH.FULL, 0, TSFNote.Accent.MEASURE));
        notes.add(new TSFNote("m", TSFNote.LENGTH.FULL, 0, TSFNote.Accent.NORMAL));
        iter = new QuarterIterator(notes);
        quarter = iter.next();
        //System.out.println(quarter);

        notes = new ArrayList<>();
        notes.add(new TSFNote("d", TSFNote.LENGTH.HALF, 0, TSFNote.Accent.MEASURE));
        notes.add(new TSFNote("m", TSFNote.LENGTH.HALF, 0, TSFNote.Accent.NORMAL));
        notes.add(new TSFNote("s", TSFNote.LENGTH.HALF, 0, TSFNote.Accent.NORMAL));
        iter = new QuarterIterator(notes);
        quarter = iter.next();
        //System.out.println(quarter);

        notes = new ArrayList<>();
        notes.add(new TSFNote("d", TSFNote.LENGTH.THIRD, 0, TSFNote.Accent.MEASURE));
        notes.add(new TSFNote("m", TSFNote.LENGTH.THIRD, 0, TSFNote.Accent.NORMAL));
        notes.add(new TSFNote("s", TSFNote.LENGTH.THIRD, 0, TSFNote.Accent.NORMAL));
        notes.add(new TSFNote("s", TSFNote.LENGTH.HALF, 0, TSFNote.Accent.NORMAL));
        iter = new QuarterIterator(notes);
        quarter = iter.next();
        //System.out.println(quarter);
        quarter = iter.next();
        //System.out.println(quarter);

        notes = new ArrayList<>();
        TSFNote note = new TSFNote("?", TSFNote.LENGTH.FULL, 0, TSFNote.Accent.NORMAL);
        note.setPostfix(" **");
        notes.add(note);
        notes.add(new TSFNote("s", TSFNote.LENGTH.FULL, 0, TSFNote.Accent.NORMAL));
        iter = new QuarterIterator(notes);
        quarter = iter.next();
        System.out.println(quarter);
        quarter = iter.next();
        System.out.println(quarter);
    }
}
