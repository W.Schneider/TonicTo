package de.jm.tonic.parser;

import de.jm.tonic.latex.TSFNote2Latex;
import de.jm.tonic.model.TSFNote;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class QuarterTest {

    @Test
    public void asQuarterList() {
        Voice voice1 = new Voice();
        Voice voice2 = new Voice();
        List<Voice> songrow = new ArrayList<>();
        songrow.add(voice1);
        songrow.add(voice2);

        voice1.add(new TSFNote("d", TSFNote.LENGTH.FULL, 0, TSFNote.Accent.MEASURE));
        voice1.add(new TSFNote("m", TSFNote.LENGTH.HALF, 0, TSFNote.Accent.NORMAL));
        voice1.add(new TSFNote("s", TSFNote.LENGTH.HALF, 0, TSFNote.Accent.NORMAL));

        voice2.add(new TSFNote("d", TSFNote.LENGTH.HALF, 0, TSFNote.Accent.MEASURE));
        voice2.add(new TSFNote("m", TSFNote.LENGTH.QUARTER, 0, TSFNote.Accent.NORMAL));
        voice2.add(new TSFNote("s", TSFNote.LENGTH.QUARTER, 0, TSFNote.Accent.NORMAL));
        voice2.add(new TSFNote("s", TSFNote.LENGTH.FULL, 0, TSFNote.Accent.NORMAL));

        List<VerticalQuarter> verticalQuarters = VerticalQuarter.asQuarterList(songrow);
        System.out.println(verticalQuarters);
    }
}