package de.jm.tonic;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Wolfgang Schneider on 01.09.2017.
 *
 */
public class Tonic2LatexWithStatesTest {

    @Test
    public void testTonic2LatexWithState() {
        String lines =
            "verse: 1. hello\n" +
            "world\n\n";

        String expected = "1. hello \\\\\n" +
            "world\n\n";
        String latex = Tonic2Latex.toLatex(lines);
        assertEquals(expected, latex);

        lines =
            "verse: 1. hello\n" +
                "world\n";

        expected = "1. hello \\\\\n" +
            "world\n";
        latex = Tonic2Latex.toLatex(lines);
        assertEquals(expected, latex);

        lines =
                "verse: 1. hello\n" +
                        "world\n" +
                "verse: 2. holla\n";

        expected =
                "\\parbox{\\textwidth/2-0.25cm}{1. hello \\\\\n" +
                "world}\\hspace{0.25cm}\\parbox{\\textwidth/2-0.25cm}{2. holla}\n";

        latex = Tonic2Latex.toLatex(lines);
        assertEquals(expected, latex);


        lines = "sql: 14";
        expected = "\\setqlength{14pt}\n";
        latex = Tonic2Latex.toLatex(lines);
        assertEquals(expected, latex);

        lines = "sql: 14\nlatex: \\latex";
        expected = "\\setqlength{14pt}\n\\latex\n";
        latex = Tonic2Latex.toLatex(lines);
        assertEquals(expected, latex);

        lines = "c: comment";
        expected = "% comment\n";
        latex = Tonic2Latex.toLatex(lines);
        assertEquals(expected, latex);

        lines = "s: %\n!d : s\n!he- lo";
        expected = "\\begin{tonic-row}{\\hspace{0pt}\\rse}\n" +
        "{F F L}\n" +
        "\\mn{d} \\nn{} \\\\\n" +
        "\\mt{he-} \\nt{lo} \\\\\n" +
        "\\end{tonic-row}\n";

        latex = Tonic2Latex.toLatex(lines);
        assertEquals(expected, latex);

        lines = "s: %\n!d : s\n!he- lo\nc: comment";
        expected =
        "\\begin{tonic-row}{\\hspace{0pt}\\rse}\n" +
        "{F F L}\n" +
        "\\mn{d} \\nn{} \\\\\n" +
        "\\mt{he-} \\nt{lo} \\\\\n" +
        "\\end{tonic-row}\n" +
        "% comment\n";

        latex = Tonic2Latex.toLatex(lines);
        assertEquals(expected, latex);
    }

}
