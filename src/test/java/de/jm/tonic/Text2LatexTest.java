package de.jm.tonic;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by johann on 16.07.17.
 *
 */
public class Text2LatexTest {

    private Text2Latex tonicFrame;

    @Before
    public void setUp() throws Exception {
        tonicFrame = new Text2Latex();
    }

    /*
       "!text"
       "!"
       "!*"
       "!!text"
       "!!"
       "!!*"

       "* ** ***"
       "text"
       "text* text** text***"
       "*text **text ***text"
       "text**text"
       "sil-**be"

    */
    @Test
    public void lex() {
        String expected = "\\tnt{le- x-} \\nt{er-} \\mt{er} ";
        String line = tonicFrame.lexText("le-*x- er- !er");
        System.out.println(line);
        assertEquals(expected, line);

        //line = tonicFrame.lexText("sal- *va- !dor *   na- ** !sceu * en *beth- !lem *  pra !nos  *");
        //line = tonicFrame.lexText("va- *mos !la  *  che- ** !gar *  e- *ma- !nue- * el  *na- !sceu * !!");
        //line = tonicFrame.lexText("to- *do !mun- * *do can- !tai * to- *do !mun- **do lou- ** !vai * ");
        //line = tonicFrame.lexText("o  *me- !ni  * *no  na- ** !sceu *  en *beth- !lem * pra- !*  **  nos !!");
        line = tonicFrame.lexText("o-ho");
        System.out.println(line);
    }

    @Test
    public void lexPart() {
        String expected = "\\nt{word} ";
        String latex = tonicFrame.lexTextPart("word");
        assertEquals(expected, latex);

        expected = "\\dnt{word} ";
        latex = tonicFrame.lexTextPart("word* ");
        assertEquals(expected, latex);

        expected = "\\tntr{word\\,} ";
        latex = tonicFrame.lexTextPart(" **word ");
        assertEquals(expected, latex);

        expected = "\\tnt{word word} ";
        latex = tonicFrame.lexTextPart(" word*word ");
        assertEquals(expected, latex);

        expected = "\\fnt{word word} ";
        latex = tonicFrame.lexTextPart(" word**word ");
        assertEquals(expected, latex);

        expected = "\\mtr{\\,} ";
        latex = tonicFrame.lexTextPart("!* ");
        assertEquals(expected, latex);
        expected = "\\mt{} ";
        latex = tonicFrame.lexTextPart("! ");
        assertEquals(expected, latex);

        expected = "& & ";
        latex = tonicFrame.lexTextPart(" ** ");
        assertEquals(expected, latex);

        expected = " ";
        latex = tonicFrame.lexTextPart(" !!");
        assertEquals(expected, latex);

        expected = "\\nt{o} ";
        latex = tonicFrame.lexTextPart("o ");
        assertEquals(expected, latex);

        expected = "\\dnt{o-ho} ";
        latex = tonicFrame.lexTextPart("o-ho* ");
        assertEquals(expected, latex);

        expected = "\\dnt{o ho} ";
        latex = tonicFrame.lexTextPart("o_ho* ");
        assertEquals(expected, latex);
    }

    @Test
    public void testGetSymbol() {
        String expected;
        String latex;

        expected = "\\lpp";
        latex = Text2Latex.getSymbol("pp");
        assertEquals(expected, latex);

        expected = "\\sign{low}";
        latex = Text2Latex.getSymbol("low");
        assertEquals(expected, latex);
    }

    @Test
    public void testGetSymbolLatex() {
        String expected;
        String latex;

        expected = "\\ms\\lpp";
        latex = Text2Latex.getSymbolLatex("M", 1, "pp");
        assertEquals(expected, latex);

        expected = "\\ms\\hfill\\lpp";
        latex = Text2Latex.getSymbolLatex("MR", 1, "pp");
        assertEquals(expected, latex);

        expected = "\\ms\\sign{low}";
        latex = Text2Latex.getSymbolLatex("M", 1, "low");
        assertEquals(expected, latex);

        expected = "\\multicolumn{2}{L}{\\ms\\lpp}";
        latex = Text2Latex.getSymbolLatex("ML", 2, "pp");
        assertEquals(expected, latex);

        expected = "\\multicolumn{2}{L}{\\ms\\lpp}";
        latex = Text2Latex.getSymbolLatex("M", 2, "pp");
        assertEquals(expected, latex);

        expected = "\\multicolumn{2}{L}{\\ds\\lpp}";
        latex = Text2Latex.getSymbolLatex("D", 2, "pp");
        assertEquals(expected, latex);

        expected = "\\multicolumn{2}{L}{\\ds\\hfill\\lpp}";
        latex = Text2Latex.getSymbolLatex("DR", 2, "pp");
        assertEquals(expected, latex);

        expected = "\\multicolumn{3}{L}{\\lpp}";
        latex = Text2Latex.getSymbolLatex("N", 3, "pp");
        assertEquals(expected, latex);
    }
}

