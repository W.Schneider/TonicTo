package de.jm.tonic.latex;

import de.jm.tonic.midi.TSFNoteParser;
import de.jm.tonic.midi.TSFScanner;
import de.jm.tonic.model.TSFNote;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static de.jm.tonic.latex.TSFNote2Latex.getFloat;
import static org.junit.Assert.*;

public class TSFNote2LatexTest {

    @Test
    public void toLatexNote() {
        TSFNote note;
        String latex;

        note = new TSFNote("d", TSFNote.LENGTH.FULL, -1, TSFNote.Accent.NORMAL);
        latex = TSFNote2Latex.toLatex(note);
        assertEquals("\\nl{d} ", latex);

        /*todo
        note = new TSFNote("d,", TSFNote.LENGTH.FULL, -1, TSFNote.Accent.MEASURE);
        note.setPostfix(" (");
        latex = TSFNote2Latex.toLatex(note);
        assertEquals("\\ml{d} (& ", latex);

        note = new TSFNote("d,", TSFNote.LENGTH.FULL, -1, TSFNote.Accent.MEASURE);
        note.setPostfix(">(");
        latex = TSFNote2Latex.toLatex(note);
        assertEquals("\\ml{d}\\hfill (& ", latex);

        note = new TSFNote("d", TSFNote.LENGTH.FULL, 0, TSFNote.Accent.MEASURE);
        note.setPostfix(")");
        latex = TSFNote2Latex.toLatex(note);
        assertEquals("\\ml{d})& ", latex);
        */
    }

    @Test
    public void toLatexNotes() {
        String latex;

        List<TSFNote> notes = new ArrayList<>();
        TSFNote note = new TSFNote("d", TSFNote.LENGTH.HALF, -1, TSFNote.Accent.MEASURE);
        note.setPostfix("+");
        notes.add(note);

        note = new TSFNote("m", TSFNote.LENGTH.HALF, -1, TSFNote.Accent.LENGTH);
        notes.add(note);

        latex = TSFNote2Latex.toLatex(notes);
        assertEquals("\\ms\\pn{\\lo{d}\\hs{\\lo{m}}}\\\\", latex);

        notes = new ArrayList<>();
        note = new TSFNote("d", TSFNote.LENGTH.HALF, -1, TSFNote.Accent.ACCENTED);
        note.setPostfix("&");
        notes.add(note);

        note = new TSFNote("m", TSFNote.LENGTH.HALF, 0, TSFNote.Accent.LENGTH);
        notes.add(note);

        latex = TSFNote2Latex.toLatex(notes);
        assertEquals("\\as\\dpn{\\lo{d}\\hs{m}}\\\\", latex);

        // !d,-.m,
        notes = new ArrayList<>();
        note = new TSFNote("d", TSFNote.LENGTH.HALF, -1, TSFNote.Accent.MEASURE);
        note.setPostfix("-");
        notes.add(note);

        note = new TSFNote("m", TSFNote.LENGTH.HALF, 1, TSFNote.Accent.LENGTH);
        notes.add(note);

        latex = TSFNote2Latex.toLatex(notes);
        assertEquals("\\ms\\hit{\\lo{d}}\\hi{m}&\\\\", latex);

        // !d, .m, ,s, ;d'
        notes = new ArrayList<>();
        note = new TSFNote("d", TSFNote.LENGTH.HALF, -1, TSFNote.Accent.MEASURE);
        notes.add(note);

        note = new TSFNote("m", TSFNote.LENGTH.QUARTER, -1, TSFNote.Accent.LENGTH);
        note.setPrefix(".");
        notes.add(note);

        note = new TSFNote("s", TSFNote.LENGTH.QUARTER, -1, TSFNote.Accent.LENGTH);
        note.setPrefix(",");
        notes.add(note);

        note = new TSFNote("d", TSFNote.LENGTH.FULL, 0, TSFNote.Accent.ACCENTED);
        notes.add(note);

        latex = TSFNote2Latex.toLatex(notes);
        assertEquals("\\ml{d} \\hl{m} \\ql{s} \\an{d} \\\\", latex);

        // .d, **
        notes = new ArrayList<>();
        note = new TSFNote("d", TSFNote.LENGTH.HALF, -1, TSFNote.Accent.LENGTH);
        note.setPostfix(" **");
        note.setPrefix(".");
        notes.add(note);

        latex = TSFNote2Latex.toLatex(notes);
        assertEquals("\\hl{d} &&\\\\", latex);
    }

    @Test
    public void getNoteString() {
        TSFNote note = new TSFNote();
        note.setPostfix("post");
        note.setValue("d,");
        note.setPrefix("pre");
        assertEquals("pred,post", TSFNote2Latex.getNoteString(note));
    }

    @Test
    public void testLine() {
        String line = "!s  .m  ,m   :d  .m  !- .m  :s  .s  !f  :- !f  .m ,f      :-   *  ";
        TSFNoteParser parser = new TSFNoteParser(line);
        List<TSFNote> notes = parser.parse();
        System.out.println(TSFNote2Latex.toLatex(notes));

        line = "!f, .f,_ *";
        parser = new TSFNoteParser(line);
        notes = parser.parse();
        System.out.println(TSFNote2Latex.toLatex(notes));

        line = ":m'+.r' :d' !s  :f    ";
        line = "!   :  :    !   :  :   !   :  :    !   :m'+.r' :d' !   :  :    !  :   :    !   :  :    !   :s  :f    ";
        parser = new TSFNoteParser(line);
        notes = parser.parse();
        System.out.println(TSFNote2Latex.toLatex(notes));
    }

    @Test
    public void testParseUnderPointOrLine() {

        String postfix = "_1.2";

        String number = getFloat(postfix);
        assertEquals("1.2", number);

        postfix = "=1";
        number = getFloat(postfix);
        assertTrue(postfix.matches(".*([0-9]+\\.?[0-9]*).*"));
        assertEquals("1", number);

        postfix = "= * ";
        number =getFloat(postfix);
        if (number.isEmpty()) {
            number = "1";
        }
        assertEquals("1", number);

        StringBuilder postfixBuilder;
        TSFNote note = new TSFNote("d", TSFNote.LENGTH.FULL, -1, TSFNote.Accent.NORMAL);
        note.setPostfix("_1.2");
        postfixBuilder = new StringBuilder(note.getPostfix());
        TSFScanner scanner = new TSFScanner(note.getPostfix());
        String latex = TSFNote2Latex.parseUnderPointOrLine(scanner);
        assertEquals("u[1.2]", latex);

        note = new TSFNote("d", TSFNote.LENGTH.FULL, -1, TSFNote.Accent.NORMAL);
        note.setPostfix("=2");
        postfixBuilder = new StringBuilder(note.getPostfix());
        scanner = new TSFScanner(note.getPostfix());
        latex = TSFNote2Latex.parseUnderPointOrLine(scanner);
        assertEquals("p[2]", latex);
    }

    @Test
    public void testToLatexNew() {
        TSFNote note = new TSFNote("d", TSFNote.LENGTH.FULL, -1, TSFNote.Accent.NORMAL);
        note.setPrefix(":");
        String latex = TSFNote2Latex.toLatexNew(note);
        assertEquals("\\nl{d}", latex);

        note = new TSFNote("r", TSFNote.LENGTH.HALF, 1, TSFNote.Accent.LENGTH);
        note.setPrefix(".");
        latex = TSFNote2Latex.toLatexNew(note);
        assertEquals("\\hh{r}", latex);

        note = new TSFNote("m", TSFNote.LENGTH.QUARTER, 0, TSFNote.Accent.LENGTH);
        note.setPrefix(",");
        latex = TSFNote2Latex.toLatexNew(note);
        assertEquals("\\qn{m}", latex);

        note = new TSFNote("m", TSFNote.LENGTH.THIRD, 0, TSFNote.Accent.LENGTH);
        note.setPrefix("/");
        note.setPostfix("_");
        latex = TSFNote2Latex.toLatexNew(note);
        assertEquals("\\tnu{m}", latex);

        note = new TSFNote("mi", TSFNote.LENGTH.THREEQUARTES, 1, TSFNote.Accent.LENGTH);
        note.setPrefix(".,");
        note.setPostfix("_1.2");
        latex = TSFNote2Latex.toLatexNew(note);
        assertEquals("\\hqhu[1.2]{mi}", latex);

        note = new TSFNote("t", TSFNote.LENGTH.TWOTHIRDS, 1, TSFNote.Accent.LENGTH);
        note.setPrefix("//");
        note.setPostfix("*");
        latex = TSFNote2Latex.toLatexNew(note);
        assertEquals("\\dtth{t}", latex);

        note = new TSFNote("l", TSFNote.LENGTH.FULL, 2, TSFNote.Accent.NORMAL);
        note.setPrefix(":");
        note.setPostfix(">(");
        latex = TSFNote2Latex.toLatexNew(note);
        assertEquals("\\nh[2]{l}\\hfill{(}", latex);

        note = new TSFNote("ta", TSFNote.LENGTH.THREEQUARTES, -2, TSFNote.Accent.LENGTH);
        note.setPrefix(",,");
        note.setPostfix("=2");
        latex = TSFNote2Latex.toLatexNew(note);
        // todo: doesn't work yet
        //assertEquals("hqnp[2]{\\lo[2]{mi}}", latex);
    }

    @Test
    public void testToLatex2Notes() {
        String latex;
        TSFNote previous;
        TSFNote note;

        previous = new TSFNote("d", TSFNote.LENGTH.FULL, 0, TSFNote.Accent.NORMAL);
        previous.setPostfix("+");
        note = new TSFNote("m", TSFNote.LENGTH.FULL, 0, TSFNote.Accent.NORMAL);
        latex = TSFNote2Latex.toLatexNew(previous, note);
        assertEquals("\\nn{d}\\nn{m}", latex);

        previous = new TSFNote("d", TSFNote.LENGTH.FULL, 1, TSFNote.Accent.NORMAL);
        previous.setPostfix("+");
        note = new TSFNote("m", TSFNote.LENGTH.FULL, -1, TSFNote.Accent.NORMAL);
        latex = TSFNote2Latex.toLatexNew(previous, note);
        assertEquals("\\nh{d}\\nl{m}", latex);

        previous = new TSFNote("d", TSFNote.LENGTH.FULL, 0, TSFNote.Accent.NORMAL);
        previous.setPostfix("&");
        note = new TSFNote("m", TSFNote.LENGTH.FULL, 0, TSFNote.Accent.NORMAL);
        latex = TSFNote2Latex.toLatexNew(previous, note);
        assertEquals("\\multi{\\nn{d}\\nn{m}}", latex);

        previous = new TSFNote("d", TSFNote.LENGTH.FULL, 0, TSFNote.Accent.NORMAL);
        previous.setPostfix("-");
        note = new TSFNote("m", TSFNote.LENGTH.FULL, 0, TSFNote.Accent.NORMAL);
        latex = TSFNote2Latex.toLatexNew(previous, note);
        //Todo
        //assertEquals("\\hit{\\nn{d}}\\nn{m}", latex);

        previous = new TSFNote("s", TSFNote.LENGTH.FULL, 0, TSFNote.Accent.NORMAL);
        previous.setPostfix("-");
        note = new TSFNote("d", TSFNote.LENGTH.FULL, 1, TSFNote.Accent.NORMAL);
        latex = TSFNote2Latex.toLatexNew(previous, note);
        //Todo
        //assertEquals("\\hit{\\nn{s}}\\nh{d}", latex);

        note = new TSFNote("d", TSFNote.LENGTH.FULL, 1, TSFNote.Accent.NORMAL);
        note.setPostfix("*");
        latex = TSFNote2Latex.toLatexNew(note);
        assertEquals("\\dnh{d}", latex);
    }

    @Test
    public void testParseNumber() {
        TSFScanner scanner = new TSFScanner("1");
        String number;
        scanner = new TSFScanner("1");
        number = TSFNote2Latex.parseNumber(scanner);
        assertEquals("1", number);

        scanner = new TSFScanner("1.2");
        number = TSFNote2Latex.parseNumber(scanner);
        assertEquals("1.2", number);

        scanner = new TSFScanner("1.3alsdfkj");
        number = TSFNote2Latex.parseNumber(scanner);
        assertEquals("1.3", number);
    }

    @Test
    public void toLatexNewQuarter() {

        List<TSFNote> notes = new ArrayList<>();
        List<String> widths = new ArrayList<>();

        TSFNote note =  new TSFNote("m", TSFNote.LENGTH.HALF, -1, TSFNote.Accent.MEASURE);
        note.setPostfix("*");
        notes.add(note);
        note =  new TSFNote("r", TSFNote.LENGTH.HALF, -1, TSFNote.Accent.LENGTH);
        notes.add(note);

        widths.add("QQQQ");
        String latex = TSFNote2Latex.toLatexNewQuarter(notes, widths);
        assertEquals("\\dml{m}& \\hl{r}&  \\\\", latex);


        notes = new ArrayList<>();
        widths = new ArrayList<>();
        note =  new TSFNote("m", TSFNote.LENGTH.HALF, 0, TSFNote.Accent.MEASURE);
        notes.add(note);
        note =  new TSFNote("r", TSFNote.LENGTH.HALF, 0, TSFNote.Accent.LENGTH);
        notes.add(note);

        widths.add("QQQQ");
        latex = TSFNote2Latex.toLatexNewQuarter(notes, widths);
        assertEquals("\\mn{m}& & \\hn{r}&  \\\\", latex);

        notes = new ArrayList<>();
        widths = new ArrayList<>();
        note =  new TSFNote("", TSFNote.LENGTH.QUARTER, 0, TSFNote.Accent.NORMAL);
        notes.add(note);
        note =  new TSFNote("l", TSFNote.LENGTH.HALF, -1, TSFNote.Accent.LENGTH);
        note.setPrefix(",");
        notes.add(note);
        note =  new TSFNote("d", TSFNote.LENGTH.QUARTER, 0, TSFNote.Accent.LENGTH);
        note.setPrefix(".,");
        notes.add(note);

        widths.add("QQQQ");
        latex = TSFNote2Latex.toLatexNewQuarter(notes, widths);
        assertEquals("\\nn{}& \\ql{l}& \\hqn{d} \\\\", latex);

        notes = new ArrayList<>();
        widths = new ArrayList<>();
        note =  new TSFNote("", TSFNote.LENGTH.THREEQUARTES, 0, TSFNote.Accent.NORMAL);
        notes.add(note);
        note =  new TSFNote("d", TSFNote.LENGTH.QUARTER, 0, TSFNote.Accent.LENGTH);
        note.setPrefix(".,");
        notes.add(note);

        widths.add("QQQQ");
        latex = TSFNote2Latex.toLatexNewQuarter(notes, widths);
        assertEquals("\\nn{}& & \\hqn{d} \\\\", latex);

        notes = new ArrayList<>();
        widths = new ArrayList<>();
        note =  new TSFNote("d", TSFNote.LENGTH.FULL, 0, TSFNote.Accent.NORMAL);
        notes.add(note);

        widths.add("QQH");
        latex = TSFNote2Latex.toLatexNewQuarter(notes, widths);
        assertEquals("\\nn{d}& &  \\\\", latex);
    }

}
