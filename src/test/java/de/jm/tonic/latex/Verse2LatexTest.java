package de.jm.tonic.latex;

import de.jm.tonic.model.VersePart;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import static org.junit.Assert.*;

public class Verse2LatexTest {

    @Test
    public void testToLatex() {
        List<String> lines = new ArrayList<>();
        lines.add("verse: 1. Strophe");
        lines.add("2. Zeile");

        ListIterator<String> iter = lines.listIterator();
        VersePart versePart = VersePart.parse(iter);

        String latex = Verse2Latex.toLatex(versePart);
        String expected = "1. Strophe\\\\\n2. Zeile\\\\\n";
        assertEquals(expected, latex);

        versePart.setStartColumns(true);
        latex = Verse2Latex.toLatex(versePart);
        expected = "\\parbox{\\textwidth/2-0.25cm}{1. Strophe\\\\\n2. Zeile\\\\\n}\\hspace{0.25cm}\\parbox{\\textwidth/2-0.25cm}{";
        assertEquals(expected, latex);

        versePart.setStartColumns(false);
        versePart.setEndColumns(true);
        latex = Verse2Latex.toLatex(versePart);
        expected = "1. Strophe\\\\\n2. Zeile\\\\\n}\n";
        assertEquals(expected, latex);

    }

}