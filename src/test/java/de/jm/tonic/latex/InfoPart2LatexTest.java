package de.jm.tonic.latex;

import de.jm.tonic.model.InfoPart;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import static org.junit.Assert.*;

public class InfoPart2LatexTest {
    @Test
    public void toLatex() throws Exception {
        List<String> lines = new ArrayList<>();
        lines.add("C: composer");
        lines.add("L: language");
        lines.add("T: title");
        lines.add("X: number");
        lines.add("Z: transcriptor");
        lines.add("c: comment");
        lines.add("K: key");
        lines.add("latex: latex");
        lines.add("sql: sql");
        lines.add("left: left");
        lines.add("right: right");
        lines.add("top: top");
        lines.add("bottom: bottom");
        lines.add("bpm: bpm");
        lines.add("small:");
        lines.add("smaller:");
        lines.add("tiny:");
        lines.add("newpage:");

        ListIterator<String> iter = lines.listIterator();
        InfoPart part = InfoPart.parse(iter);
        assertNotNull(part);
        assertEquals("\\renewcommand\\SongComposer{composer}\n", InfoPart2Latex.toLatex(part));

        part = InfoPart.parse(iter);
        assertNotNull(part);
        assertEquals("\\renewcommand\\SongLanguage{language}\n", InfoPart2Latex.toLatex(part));

        part = InfoPart.parse(iter);
        assertNotNull(part);
        assertEquals("\\renewcommand\\SongTitle{title}\n", InfoPart2Latex.toLatex(part));

        part = InfoPart.parse(iter);
        assertNotNull(part);
        assertEquals("\\renewcommand\\SongNumber{number}\n", InfoPart2Latex.toLatex(part));

        part = InfoPart.parse(iter);
        assertNotNull(part);
        assertEquals("\\renewcommand\\SongTranscription{transcriptor}\n", InfoPart2Latex.toLatex(part));

        part = InfoPart.parse(iter);
        assertNotNull(part);
        assertEquals("% comment\n", InfoPart2Latex.toLatex(part));

        part = InfoPart.parse(iter);
        assertNotNull(part);
        assertEquals("\\renewcommand\\SongPitch{key}\n", InfoPart2Latex.toLatex(part));

        part = InfoPart.parse(iter);
        assertNotNull(part);
        assertEquals("latex\n", InfoPart2Latex.toLatex(part));

        part = InfoPart.parse(iter);
        assertNotNull(part);
        assertEquals("\\setqlength{sqlpt}\n", InfoPart2Latex.toLatex(part));

        part = InfoPart.parse(iter);
        assertNotNull(part);
        assertEquals("\n", InfoPart2Latex.toLatex(part));

        part = InfoPart.parse(iter);
        assertNotNull(part);
        assertEquals("\n", InfoPart2Latex.toLatex(part));

        part = InfoPart.parse(iter);
        assertNotNull(part);
        assertEquals("\n", InfoPart2Latex.toLatex(part));

        part = InfoPart.parse(iter);
        assertNotNull(part);
        assertEquals("\n", InfoPart2Latex.toLatex(part));

        part = InfoPart.parse(iter);
        assertNotNull(part);
        assertEquals("\n", InfoPart2Latex.toLatex(part));

        part = InfoPart.parse(iter);
        assertNotNull(part);
        assertEquals("\\small\n", InfoPart2Latex.toLatex(part));

        part = InfoPart.parse(iter);
        assertNotNull(part);
        assertEquals("\\footnotesize\n", InfoPart2Latex.toLatex(part));

        part = InfoPart.parse(iter);
        assertNotNull(part);
        assertEquals("\\tiny\n", InfoPart2Latex.toLatex(part));

        part = InfoPart.parse(iter);
        assertNotNull(part);
        assertEquals("\\newpage\n", InfoPart2Latex.toLatex(part));

    }

}
