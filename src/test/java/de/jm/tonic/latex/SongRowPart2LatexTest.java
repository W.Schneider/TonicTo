package de.jm.tonic.latex;

import de.jm.tonic.latex.SongRowPart2Latex;
import de.jm.tonic.model.SongRowPart;
import de.jm.tonic.parser.VerticalQuarter;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class SongRowPart2LatexTest {

    @Test
    public void toLatex() {
        List<String> lines = new ArrayList<>();
        lines.add("s:                                                 DC*8");
        lines.add("s: %");
        lines.add("!s   .t   ,l  :s  .s  !s  .r  :m  .r  !d  :-  !-  :- ] !!");
        lines.add("~!refrain * * p");
        lines.add("!m   .s   ,f  :m  .m  !m  .t, :d  .t, !s, :l, !s, :- ] !!");
        lines.add("!mpa- ka-bi*   bue le !ba  la  li  si !ye  si !ye  *   !!");
        lines.add("!mo-  kon-ji*  na- ba !kon jia bo  ta !mi  ta !mi  *   !!");
        lines.add("!d'  .r'  ,d' :d' .d' !d' .s  :s  .f  !m  :f  !m  :- ] !!");
        lines.add("!d   .d   ,d  :d  .d  !d  .s, :s, .s, !d  :-  !-  :- ] !!");

        SongRowPart songRowPart = SongRowPart.parse(lines.listIterator());
        System.out.println(SongRowPart2Latex.toLatex(songRowPart));
    }

    @Test
    public void testWidth2String() {
        int width = VerticalQuarter.FULL;
        assertEquals("F ", SongRowPart2Latex.widthToString(width));

        width = VerticalQuarter.FULL | VerticalQuarter.HALF;
        assertEquals("HH ", SongRowPart2Latex.widthToString(width));

        width = VerticalQuarter.FULL | VerticalQuarter.HALF | VerticalQuarter.FOURTHQUARTER;
        assertEquals("HQQ ", SongRowPart2Latex.widthToString(width));

        width = VerticalQuarter.FULL | VerticalQuarter.SECONDQUARTER | VerticalQuarter.HALF | VerticalQuarter.FOURTHQUARTER;
        assertEquals("QQQQ ", SongRowPart2Latex.widthToString(width));

        width = VerticalQuarter.FULL |VerticalQuarter.SECONDTHIRD | VerticalQuarter.THIRDTHIRD;
        assertEquals("TTT ", SongRowPart2Latex.widthToString(width));

        width = VerticalQuarter.LEFTFULL;
        assertEquals("L ", SongRowPart2Latex.widthToString(width));
    }

    @Test
    public void testGetLengthString() {
        List<Integer> lengths = new ArrayList<>();
        lengths.add(VerticalQuarter.FULL);
        lengths.add(VerticalQuarter.FULL | VerticalQuarter.HALF);
        lengths.add(VerticalQuarter.FULL | VerticalQuarter.HALF | VerticalQuarter.FOURTHQUARTER);
        lengths.add(VerticalQuarter.FULL | VerticalQuarter.SECONDQUARTER | VerticalQuarter.HALF | VerticalQuarter.FOURTHQUARTER);
        lengths.add(VerticalQuarter.FULL | VerticalQuarter.SECONDTHIRD | VerticalQuarter.THIRDTHIRD);

        assertEquals("F HH HQQ QQQQ TTT ", SongRowPart2Latex.getLengthString(lengths));
    }
}
