package de.jm.tonic;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Wolfgang Schneider on 28.08.2017.
 *
 */
public class Symbol2LatexTest {
    @Test
    public void toLatex() throws Exception {
        String line;
        String expected;
        String result;

        line = "s: cresc";
        result = Symbol2Latex.toLatex(line);
        expected = "\\cresc";
        assertEquals(expected, result);

        line = "s: mf_cresc*7";
        result = Symbol2Latex.toLatex(line);
        expected = "\\tfb[\\lmf\\ \\cresc]{7} ";
        assertEquals(expected, result);

        line = "s:  mf_cresc*7                        f>______________________________________DS*9";
        result = Symbol2Latex.toLatex(line);
        expected = "\\tfb[\\lmf\\ \\cresc]{7} \\tfb[\\lf\\ \\hfill\\ \\rds]{9} ";
        assertEquals(expected, result);

        line = "s:  mf_cresc_adagio*7                 f>______________________________________DS*9";
        result = Symbol2Latex.toLatex(line);
        expected = "\\tfb[\\lmf\\ \\cresc\\ \\sign{adagio}]{7} \\tfb[\\lf\\ \\hfill\\ \\rds]{9} ";
        assertEquals(expected, result);

        line = "s: DS*8";
        result = Symbol2Latex.toLatex(line);
        expected = "\\DS{8} ";
        assertEquals(expected, result);

        line = "s: DC_f*8";
        result = Symbol2Latex.toLatex(line);
        expected = "\\DC[\\lf]{8} ";
        assertEquals(expected, result);

        line = "s: 1.*8";
        result = Symbol2Latex.toLatex(line);
        expected = "\\first{8} ";
        assertEquals(expected, result);

        line = "s: 2._DS*8";
        result = Symbol2Latex.toLatex(line);
        expected = "\\second[\\rds]{8} ";
        assertEquals(expected, result);

        line = "s: *8";
        result = Symbol2Latex.toLatex(line);
        expected = "\\tfb{8} ";
        assertEquals(expected, result);

        line = "s: % ";
        result = Symbol2Latex.toLatex(line);
        expected = "\\rse";
        assertEquals(expected, result);

        line = "s: $ ";
        result = Symbol2Latex.toLatex(line);
        expected = "\\coda";
        assertEquals(expected, result);

        line = "s: 5 -4pt 3em -10 ";
        result = Symbol2Latex.toLatex(line);
        expected = "\\hspace{5pt}\\hspace{-4pt}\\hspace{3em}\\hspace{-10pt}";
        assertEquals(expected, result);

        line = "s: Coda*8_ ";
        result = Symbol2Latex.toLatex(line);
        expected = "\\tfub[\\sign{Coda}]{8} ";
        assertEquals(expected, result);
    }

    @Test
    public void testIsFullBox() {
        String part;
        part = "mf_cresc";
        assertFalse(Symbol2Latex.isFullBox(part));

        part = "mf_cresc*";
        assertTrue(Symbol2Latex.isFullBox(part));

        part = "mf_cresc*2";
        assertTrue(Symbol2Latex.isFullBox(part));

    }

    @Test
    public void testGetBoxLength() {
        String part;

        part = "mv_cresc*";
        assertEquals( "1", Symbol2Latex.getBoxLength(part));

        part = "mv_cresc*7";
        assertEquals("7", Symbol2Latex.getBoxLength(part));

        part = "mv_cresc*17";
        assertEquals("17", Symbol2Latex.getBoxLength(part));
    }

    @Test
    public void testGetPartValue() {
        String part;
        String expected;

        part = "mv_cresc";
        expected = "mv cresc";
        assertEquals(expected, Symbol2Latex.getPartValue(part));

        part = "mv_cresc*";
        expected = "mv cresc";
        assertEquals(expected, Symbol2Latex.getPartValue(part));

        part = "mv_cresc*7";
        expected = "mv cresc";
        assertEquals(expected, Symbol2Latex.getPartValue(part));

        part = "mv_______cresc*7";
        expected = "mv cresc";
        assertEquals(expected, Symbol2Latex.getPartValue(part));
    }
}
