package de.jm.tonic;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Wolfgang Schneider on 09.08.2017.
 *
 */
public class QuarterTest {

    @Test
    public void testToString() {
        String expectedTex;
        Quarter quarter;
        String quarterTex;

        /*
        expectedTex = "\\nn{d} \\hn{d} ";
        quarter = new Quarter(":d (.d)");
        quarterTex = quarter.toString();
        assertEquals(expectedTex, quarterTex);
        */

        expectedTex = "\\nn{d} ";
        quarter = new Quarter(":d ");
        quarterTex = quarter.toString();
        assertEquals(expectedTex, quarterTex);

        expectedTex = "\\mn{d} \\hn{d} ";
        quarter = new Quarter("!d .d");
        quarterTex = quarter.toString();
        assertEquals(expectedTex, quarterTex);

        expectedTex = "\\an{d} \\qn{d} \\hn{d} ";
        quarter = new Quarter(";d ,d .d");
        quarterTex = quarter.toString();
        assertEquals(expectedTex, quarterTex);

        expectedTex = "\\dn{d} \\hqn{d} ";
        quarter = new Quarter("!!d .,d");
        quarterTex = quarter.toString();
        assertEquals(expectedTex, quarterTex);

    }

    @Test
    public void testWidth() {
        int expected = Quarter.full;
        Quarter quarter = new Quarter(":d");
        int width = quarter.getWidth();
        assertEquals(expected, width);

        expected = Quarter.full | Quarter.half;
        quarter = new Quarter(":d .d");
        width = quarter.getWidth();
        assertEquals(expected, width);

        expected = Quarter.full | Quarter.secondQuarter | Quarter.half;
        quarter = new Quarter(":d ,d .d");
        width = quarter.getWidth();
        assertEquals(expected, width);

        expected = Quarter.full  | Quarter.half | Quarter.fourthQuarter;
        quarter = new Quarter(":d .d ,d");
        width = quarter.getWidth();
        assertEquals(expected, width);

        expected = Quarter.full  | Quarter.half | Quarter.fourthQuarter;
        quarter = new Quarter(":d .,d");
        width = quarter.getWidth();
        assertEquals(expected, width);

        expected = Quarter.full  | Quarter.secondQuarter | Quarter.half | Quarter.fourthQuarter;
        quarter = new Quarter(":d ,d .- ,d");
        width = quarter.getWidth();
        assertEquals(expected, width);

        quarter = new Quarter(":d ,d .  ,d");
        width = quarter.getWidth();
        assertEquals(expected, width);

        quarter = new Quarter(":d ,d .d ,d");
        width = quarter.getWidth();
        assertEquals(expected, width);

        quarter = new Quarter(":d ,d .,d");
        width = quarter.getWidth();
        assertEquals(expected, width);

        expected = Quarter.full  | Quarter.secondThird | Quarter.thirdThird;
        quarter = new Quarter(":d /d /d");
        width = quarter.getWidth();
        assertEquals(expected, width);

        quarter = new Quarter(":d /  /d");
        width = quarter.getWidth();
        assertEquals(expected, width);

        quarter = new Quarter(":d //d");
        width = quarter.getWidth();
        assertEquals(expected, width);
    }
}
