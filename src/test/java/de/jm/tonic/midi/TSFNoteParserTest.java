package de.jm.tonic.midi;

import de.jm.tonic.model.TSFNote;
import org.junit.Test;

import java.util.List;

import static de.jm.tonic.model.TSFNote.LENGTH.FULL;
import static de.jm.tonic.model.TSFNote.LENGTH.QUARTER;
import static org.junit.Assert.*;

public class TSFNoteParserTest {

    @Test
    public void parseNoteLength() {
        TSFNoteParser  parser;
        List<TSFNote> notes;
        TSFNote note;
        TSFNote.LENGTH[] expectedLength;

        parser = new TSFNoteParser("!f, .f,_ *");
        expectedLength = new TSFNote.LENGTH[]{TSFNote.LENGTH.HALF, TSFNote.LENGTH.HALF};
        notes = parser.parse();
        assertLengths(expectedLength, notes);

        parser = new TSFNoteParser(":d ,d .d ,d");
        expectedLength = new TSFNote.LENGTH[]{TSFNote.LENGTH.QUARTER, TSFNote.LENGTH.QUARTER, TSFNote.LENGTH.QUARTER, TSFNote.LENGTH.QUARTER};
        notes = parser.parse();
        assertLengths(expectedLength, notes);

        parser = new TSFNoteParser(":d ,d .d");
        expectedLength = new TSFNote.LENGTH[]{TSFNote.LENGTH.QUARTER, TSFNote.LENGTH.QUARTER, TSFNote.LENGTH.HALF};
        notes = parser.parse();
        assertLengths(expectedLength, notes);

        parser = new TSFNoteParser(":d .d ,d");
        expectedLength = new TSFNote.LENGTH[]{TSFNote.LENGTH.HALF, TSFNote.LENGTH.QUARTER, TSFNote.LENGTH.QUARTER};
        notes = parser.parse();
        assertLengths(expectedLength, notes);

        parser = new TSFNoteParser(":d .d");
        expectedLength = new TSFNote.LENGTH[]{TSFNote.LENGTH.HALF, TSFNote.LENGTH.HALF};
        notes = parser.parse();
        assertLengths(expectedLength, notes);

        parser = new TSFNoteParser(":d");
        expectedLength = new TSFNote.LENGTH[]{FULL};
        notes = parser.parse();
        assertLengths(expectedLength, notes);

        expectedLength = new TSFNote.LENGTH[]{TSFNote.LENGTH.THREEQUARTES, TSFNote.LENGTH.QUARTER};
        parser = new TSFNoteParser(":d .,d");
        notes = parser.parse();
        assertLengths(expectedLength, notes);

        expectedLength = new TSFNote.LENGTH[]{TSFNote.LENGTH.QUARTER, TSFNote.LENGTH.HALF, TSFNote.LENGTH.QUARTER};
        parser = new TSFNoteParser(":d ,d .,d");
        notes = parser.parse();
        assertLengths(expectedLength, notes);

        expectedLength = new TSFNote.LENGTH[]{TSFNote.LENGTH.THREEQUARTES, TSFNote.LENGTH.QUARTER};
        parser = new TSFNoteParser(":d ,,d");
        notes = parser.parse();
        assertLengths(expectedLength, notes);

        expectedLength = new TSFNote.LENGTH[]{TSFNote.LENGTH.THIRD, TSFNote.LENGTH.THIRD, TSFNote.LENGTH.THIRD};
        parser = new TSFNoteParser(":d /d /d");
        notes = parser.parse();
        assertLengths(expectedLength, notes);

        expectedLength = new TSFNote.LENGTH[]{TSFNote.LENGTH.TWOTHIRDS, TSFNote.LENGTH.THIRD};
        parser = new TSFNoteParser(":d //d");
        notes = parser.parse();
        assertLengths(expectedLength, notes);
    }

    private void assertLengths(TSFNote.LENGTH[] expectedLength, List<TSFNote> notes) {
        assertEquals(expectedLength.length, notes.size());

        for (int i = 0; i < expectedLength.length; i++) {
            assertEquals(expectedLength[i], notes.get(i).getLength());
        }
    }

    @Test
    public void parse() {

        TSFNoteParser  parser;
        List<TSFNote> notes;
        TSFNote note;

        parser = new TSFNoteParser(":d-s,");
        notes = parser.parse();
        assertEquals(1, notes.size());
        note = notes.get(0);
        assertEquals("-s,", note.getPostfix());

        parser = new TSFNoteParser(":d (.d)");
        notes = parser.parse();
        assertEquals(2, notes.size());
        note = notes.get(0);
        assertEquals(" (", note.getPostfix());
        note = notes.get(1);
        assertEquals(")", note.getPostfix());

        parser = new TSFNoteParser(":d,+,,d,");
        notes = parser.parse();
        assertEquals(2, notes.size());
        note = notes.get(0);
        assertEquals("+", note.getPostfix());
        note = notes.get(1);
        assertEquals(",,", note.getPrefix());

        parser = new TSFNoteParser(":d,&.,d,");
        notes = parser.parse();
        assertEquals(2, notes.size());
        note = notes.get(0);
        assertEquals("&", note.getPostfix());
        note = notes.get(1);
        assertEquals(".,", note.getPrefix());

        parser = new TSFNoteParser(":d ,d .d ,d");
        notes = parser.parse();
        assertEquals(4, notes.size());
        note = notes.get(0);
        assertEquals("d", note.getValue());
        assertEquals(TSFNote.LENGTH.QUARTER, note.getLength());
        note = notes.get(1);
        assertEquals("d", note.getValue());
        assertEquals(TSFNote.LENGTH.QUARTER, note.getLength());
        note = notes.get(2);
        assertEquals("d", note.getValue());
        assertEquals(TSFNote.LENGTH.QUARTER, note.getLength());
        note = notes.get(3);
        assertEquals("d", note.getValue());
        assertEquals(TSFNote.LENGTH.QUARTER, note.getLength());

        parser = new TSFNoteParser(": ,d .-");
        notes = parser.parse();
        assertEquals(3, notes.size());
        note = notes.get(0);
        assertEquals("", note.getValue());
        assertEquals(TSFNote.LENGTH.QUARTER, note.getLength());
        note = notes.get(1);
        assertEquals("d", note.getValue());
        assertEquals(TSFNote.LENGTH.QUARTER, note.getLength());
        note = notes.get(2);
        assertEquals("-", note.getValue());
        assertEquals(TSFNote.LENGTH.HALF, note.getLength());

        parser = new TSFNoteParser(":d .");
        notes = parser.parse();
        assertEquals(2, notes.size());
        note = notes.get(0);
        assertEquals("d", note.getValue());
        assertEquals( TSFNote.LENGTH.HALF, note.getLength());

        parser = new TSFNoteParser(":d .,r");
        notes = parser.parse();
        assertEquals(2, notes.size());
        note = notes.get(0);
        assertEquals("d", note.getValue());
        assertEquals( TSFNote.LENGTH.THREEQUARTES, note.getLength());
        note = notes.get(1);
        assertEquals(TSFNote.LENGTH.QUARTER, note.getLength());

        parser = new TSFNoteParser("!d :  !!");
        notes = parser.parse();
        assertEquals(3, notes.size());
        note = notes.get(0);
        assertEquals("d", note.getValue());
        assertEquals(TSFNote.Accent.MEASURE, note.getAccent());

        parser = new TSFNoteParser(":r ,d . :m /f /s ;t .r, .d");
        notes = parser.parse();
        assertEquals(9, notes.size());
        note = notes.get(0);
        assertEquals("r", note.getValue());
        assertEquals( TSFNote.Accent.NORMAL, note.getAccent());
        assertEquals(TSFNote.LENGTH.QUARTER, note.getLength());

        parser = new TSFNoteParser("!d :m, ;- : .f ,r");

        notes = parser.parse();
        assertEquals(6, notes.size());
        note = notes.get(0);
        assertEquals("d", note.getValue());
        assertEquals( TSFNote.Accent.MEASURE, note.getAccent());

        TSFNote second = notes.get(1);
        assertEquals("m", second.getValue());
        assertEquals(-1, second.getPitch());
        assertEquals( TSFNote.Accent.NORMAL, second.getAccent());

        parser = new TSFNoteParser("!d !!");
        notes = parser.parse();
        assertEquals(2, notes.size());
        note = notes.get(0);
        assertEquals("d", note.getValue());
        assertEquals(TSFNote.Accent.MEASURE, note.getAccent());

        note = notes.get(1);
        assertEquals("!!!", note.getValue());
        assertTrue(note.isMeta());
        assertTrue(note.isEndOfPart());

        parser = new TSFNoteParser("!d !! ");
        notes = parser.parse();
        assertEquals(2, notes.size());
        note = notes.get(0);
        assertEquals("d", note.getValue());
        assertEquals(TSFNote.Accent.MEASURE, note.getAccent());

        note = notes.get(1);
        assertEquals(TSFNote.Accent.DOUBLEMEASURE, note.getAccent());
        assertTrue(note.isBreak());
        assertFalse(note.isEndOfPart());

        parser = new TSFNoteParser("[1");
        notes = parser.parse();
        assertEquals(1, notes.size());
        note = notes.get(0);
        assertEquals("[1", note.getValue());
        assertTrue(note.isMeta());

        parser = new TSFNoteParser("[");
        notes = parser.parse();
        assertEquals(1, notes.size());
        note = notes.get(0);
        assertEquals("[", note.getValue());
        assertTrue(note.isMeta());

        parser = new TSFNoteParser("]");
        notes = parser.parse();
        assertEquals(1, notes.size());
        note = notes.get(0);
        assertEquals("]", note.getValue());
        assertTrue(note.isMeta());

        parser = new TSFNoteParser("[1 !d");
        notes = parser.parse();
        assertEquals(2, notes.size());
        note = notes.get(0);
        assertEquals("[1", note.getValue());
        assertTrue(note.isMeta());

        note = notes.get(1);
        assertEquals("d", note.getValue());
        assertEquals(TSFNote.Accent.MEASURE, note.getAccent());
    }

    @Test
    public void skipToNextNote() {
        TSFNoteParser  parser;
        TSFNote note;

        parser = new TSFNoteParser("!d-s,");
        parser.skipToNextNote();
        note = parser.parseNote();
        assertEquals(TSFNote.Accent.MEASURE, note.getAccent());
        assertEquals("d", note.getValue());
        assertEquals(0, note.getPitch());
        assertEquals(FULL, note.getLength());
        assertEquals(TSFNote.NoteType.NORMAL, note.getType());

        parser = new TSFNoteParser("!d");
        parser.skipToNextNote();
        note = parser.parseNote();
        assertEquals(TSFNote.Accent.MEASURE, note.getAccent());
        assertEquals("d", note.getValue());
        assertEquals(0, note.getPitch());
        assertEquals(FULL, note.getLength());
        assertEquals(TSFNote.NoteType.NORMAL, note.getType());

        parser = new TSFNoteParser(" ;d,");
        parser.skipToNextNote();
        note = parser.parseNote();
        assertEquals(TSFNote.Accent.ACCENTED, note.getAccent());
        assertEquals("d", note.getValue());
        assertEquals(-1, note.getPitch());
        assertEquals(FULL, note.getLength());
        assertEquals(TSFNote.NoteType.NORMAL, note.getType());

        parser = new TSFNoteParser("hello :d'");
        parser.skipToNextNote();
        note = parser.parseNote();
        assertEquals(TSFNote.Accent.NORMAL, note.getAccent());
        assertEquals("d", note.getValue());
        assertEquals(1, note.getPitch());
        assertEquals(FULL, note.getLength());
        assertEquals(TSFNote.NoteType.NORMAL, note.getType());

        parser = new TSFNoteParser("hello");
        parser.skipToNextNote();
        assertEquals(null, parser.parseNote());

        parser = new TSFNoteParser("=1.3 !d,");
        String skipped = parser.skipToNextNote();
        note = parser.parseNote();
        assertEquals(TSFNote.Accent.MEASURE, note.getAccent());
        assertEquals("d", note.getValue());
        assertEquals(-1, note.getPitch());
        assertEquals(FULL, note.getLength());
        assertEquals(TSFNote.NoteType.NORMAL, note.getType());
    }

    @Test
    public void parseNote() {
        TSFNoteParser  parser;
        TSFNote note;

        parser = new TSFNoteParser("!d");
        note = parser.parseNote();
        assertEquals(TSFNote.Accent.MEASURE, note.getAccent());
        assertEquals("d", note.getValue());
        assertEquals(0, note.getPitch());
        assertEquals(FULL, note.getLength());
        assertEquals(TSFNote.NoteType.NORMAL, note.getType());

        parser = new TSFNoteParser("!");
        note = parser.parseNote();
        assertEquals(TSFNote.Accent.MEASURE, note.getAccent());
        assertEquals("", note.getValue());
        assertEquals(0, note.getPitch());
        assertEquals(FULL, note.getLength());
        assertEquals(TSFNote.NoteType.BREAK, note.getType());

        parser = new TSFNoteParser(";-");
        note = parser.parseNote();
        assertEquals(TSFNote.Accent.ACCENTED, note.getAccent());
        assertEquals("-", note.getValue());
        assertEquals(0, note.getPitch());
        assertEquals(FULL, note.getLength());
        assertEquals(TSFNote.NoteType.CONTINUE, note.getType());

        parser = new TSFNoteParser(",,d,=1.8 (");
        note = parser.parseNote();
        assertEquals(TSFNote.Accent.LENGTH, note.getAccent());
        assertEquals("d", note.getValue());
        assertEquals(-1, note.getPitch());
        assertEquals(",,", note.getPrefix());
        assertEquals("=1.8 (", note.getPostfix());
        assertEquals(QUARTER, note.getLength());

        parser = new TSFNoteParser(",?");
        note = parser.parseNote();
        assertEquals(TSFNote.Accent.LENGTH, note.getAccent());
        assertEquals("?", note.getValue());
        assertEquals(0, note.getPitch());
        assertEquals(",", note.getPrefix());
        assertEquals(QUARTER, note.getLength());

        parser = new TSFNoteParser(":?");
        note = parser.parseNote();
        assertEquals(TSFNote.Accent.NORMAL, note.getAccent());
        assertEquals("?", note.getValue());
        assertEquals(0, note.getPitch());
        assertEquals(FULL, note.getLength());
    }

    @Test
    public void isAccent() {
        TSFNoteParser  parser;
        parser = new TSFNoteParser("");

        assertTrue(parser.isAccent('!'));
        assertTrue(parser.isAccent(';'));
        assertTrue(parser.isAccent(':'));
    }

    @Test
    public void parseAccent() {
        assertEquals(TSFNote.Accent.DOUBLEMEASURE, TSFNoteParser.parseAccent("!!"));
        assertEquals(TSFNote.Accent.MEASURE, TSFNoteParser.parseAccent("!"));
        assertEquals(TSFNote.Accent.ACCENTED, TSFNoteParser.parseAccent(";"));
        assertEquals(TSFNote.Accent.NORMAL, TSFNoteParser.parseAccent(":"));
        assertEquals(null, TSFNoteParser.parseAccent(""));
        assertEquals(null, TSFNoteParser.parseAccent("a"));
    }

    @Test
    public void isLength() {
        TSFNoteParser  parser;
        parser = new TSFNoteParser("");

        assertTrue(parser.isLength('.'));
        assertTrue(parser.isLength(','));
        assertTrue(parser.isLength('/'));
        assertFalse(parser.isLength('a'));
    }

    @Test
    public void parseLength() {
        assertEquals(TSFNote.LENGTH.HALF, TSFNoteParser.parseLength("."));
        assertEquals(TSFNote.LENGTH.QUARTER, TSFNoteParser.parseLength(","));
        assertEquals(TSFNote.LENGTH.THIRD, TSFNoteParser.parseLength("/"));
        assertEquals(FULL, TSFNoteParser.parseLength(""));
        assertEquals(FULL, TSFNoteParser.parseLength("a"));
    }

    @Test
    public void isValue() {
        TSFNoteParser  parser;

        parser = new TSFNoteParser("");
        assertTrue(parser.isValue('d'));
        assertTrue(parser.isValue('r'));
        assertTrue(parser.isValue('m'));
        assertTrue(parser.isValue('f'));
        assertTrue(parser.isValue('s'));
        assertTrue(parser.isValue('l'));
        assertTrue(parser.isValue('t'));
        assertTrue(parser.isValue('b'));
        assertTrue(parser.isValue('-'));
    }

    @Test
    public void parseValue() {
        TSFNoteParser  parser;

        parser = new TSFNoteParser("d");
        assertEquals("d", parser.parseValue());

        parser = new TSFNoteParser("da");
        assertEquals("da", parser.parseValue());

        parser = new TSFNoteParser("de");
        assertEquals("de", parser.parseValue());

        parser = new TSFNoteParser("ba");
        assertEquals("ba", parser.parseValue());

        parser = new TSFNoteParser("re");
        assertEquals("re", parser.parseValue());

        parser = new TSFNoteParser("me");
        assertEquals("m", parser.parseValue());

        parser = new TSFNoteParser("-");
        assertEquals("-", parser.parseValue());
    }

    @Test
    public void isPitch() {
        TSFNoteParser  parser;
        parser = new TSFNoteParser("");

        Character c = '\'';
        assertTrue(parser.isPitch(c));

        c = ',';
        assertTrue(parser.isPitch(c));

        c = 'd';
        assertFalse(parser.isPitch(c));
    }

    @Test
    public void parsePitch() {
        TSFNoteParser  parser;
        parser = new TSFNoteParser("");
        assertEquals(0, parser.parsePitch());

        parser = new TSFNoteParser(" ");
        assertEquals(0, parser.parsePitch());

        parser = new TSFNoteParser("d");
        assertEquals(0, parser.parsePitch());

        parser = new TSFNoteParser("'");
        assertEquals(1, parser.parsePitch());

        parser = new TSFNoteParser("''");
        assertEquals(2, parser.parsePitch());

        parser = new TSFNoteParser(",");
        assertEquals(-1, parser.parsePitch());

        parser = new TSFNoteParser(",,");
        assertEquals(-2, parser.parsePitch());

    }

    @Test
    public void testSkipPlainNote() {
        TSFNoteParser  parser;
        parser = new TSFNoteParser("d :d");
        String plainNote= parser.skipNextPlainNote();
        assertEquals("d", plainNote);

        parser = new TSFNoteParser("d' :d");
        plainNote= parser.skipNextPlainNote();
        assertEquals("d'", plainNote);

        parser = new TSFNoteParser("d, :d");
        plainNote= parser.skipNextPlainNote();
        assertEquals("d,", plainNote);

        parser = new TSFNoteParser("da, :d");
        plainNote= parser.skipNextPlainNote();
        assertEquals("da,", plainNote);

        parser = new TSFNoteParser("di, :d");
        plainNote= parser.skipNextPlainNote();
        assertEquals("di,", plainNote);

        parser = new TSFNoteParser("de, :d");
        plainNote= parser.skipNextPlainNote();
        assertEquals("de,", plainNote);

        parser = new TSFNoteParser("do, :d");
        plainNote= parser.skipNextPlainNote();
        assertEquals("d", plainNote);

    }

}
