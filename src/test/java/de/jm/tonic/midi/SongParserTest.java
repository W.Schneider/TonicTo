
package de.jm.tonic.midi;

import de.jm.tonic.model.SongModel;
import de.jm.tonic.model.SongPart;
import org.junit.Test;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiUnavailableException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class SongParserTest {
    @Test
    public void testParse() throws InvalidMidiDataException, MidiUnavailableException {
        List<String> rows = new ArrayList<>();
        rows.add(":s,    !d  :- .d    :d    :m  !r  :- .d  :r  :m    !d  :- .d  :m   :s    !l :- :- ");
        rows.add("Nehmt !Ab- *schied, Brü- der !un-   *ge- wiss ist !al- *  le  Wie- der- !kehr,*");
        rows.add(":m,    !m, :- .m,   :m,   :s, !f, :- .m, :f, :s,   !m, :- .m, :s,  :m    !f :- :- ");
        rows.add("");
        rows.add(":l  !s  :- .m  :m    :d  !r   :- .d   :r  :m  .r_ !d   :- .l, :l,  :s,  !d  :- :- ");
        rows.add("die !Zu- *kunft liegt in !Fins- *ter- nis  _und*  !macht *das  Herz uns !schwer.* !!");
        rows.add(":f  !m  :- .d  :d    :l, !t,  :- .l,  :t, :s,  *  !l,  :- .f, :f,  :s,  !m, :- :- ");
        SongParser parser = new SongParser();
        SongModel model = parser.parse(rows.listIterator());
        //MidiPlayer player = new MidiPlayer();
        //player.playSongModel(model);
    }

    public void testParseFile() throws InvalidMidiDataException, MidiUnavailableException, IOException {
        List<String> rows = Files.readAllLines(Paths.get("c:\\Users\\johann\\git\\sanjola\\tsf\\ChantonsSansCesse.tsf"));
        // List<String> rows = Files.readAllLines(Paths.get("/home/johann/Tonic_Sol-fa/sanjola/tsf/ChantonsSansCesse.tsf"));
        SongParser parser = new SongParser();
        List<SongPart> parts = parser.parseParts(rows.listIterator());
        for (SongPart part : parts) {
            //System.out.println(part.toLatex());
        }
    }

}
