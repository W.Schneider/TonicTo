package de.jm.tonic.midi;

import de.jm.tonic.model.TSFNote;
import de.jm.tonic.model.TSFNote2MidiNote;
import org.junit.Test;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiUnavailableException;
import java.util.ArrayList;
import java.util.List;

public class MidiPlayerTest {
    //@Test
    public void play() throws Exception {
        List<MidiNote> notes = new ArrayList<>();

        MidiNote note = new MidiNote();
        note.setValue(60);
        note.setLength(4);
        notes.add(note);

        note = new MidiNote();
        note.setValue(62);
        note.setLength(4);
        notes.add(note);

        note = new MidiNote();
        note.setValue(64);
        note.setLength(4);
        notes.add(note);

        note = new MidiNote();
        note.setValue(65);
        note.setLength(4);
        notes.add(note);

        MidiPlayer player = new MidiPlayer();
        player.play(notes);
    }

    //@Test
    public void playTSF() throws Exception {
        TSFNoteParser  parser;

        parser = new TSFNoteParser(":d .r :m .f :s :s :l .l :l .l :s :f .f :f .f :m :m :r .r :r .r :d");
        List<TSFNote> notes = parser.parse();
        List<MidiNote> midiNotes = new ArrayList<>();
        MidiNote lastNote = null;
        for (TSFNote note : notes) {
            MidiNote midiNote = TSFNote2MidiNote.toMidiNote(note, lastNote, false);
            lastNote = midiNote;
            midiNotes.add(midiNote);
        }

        MidiPlayer player = new MidiPlayer();
        player.play(midiNotes);
    }

    //@Test
    public void playPartitur() throws Exception {
        List<List<MidiNote>> partitur = new ArrayList<>();

        TSFNoteParser  parser;

        parser = new TSFNoteParser(":d .r :m .f :s :s :l .l :l .l :s :f .f :f .f :m :m :r .r :r .r :s");
        List<TSFNote> notes = parser.parse();
        List<MidiNote> midiNotes = new ArrayList<>();
        MidiNote lastNote = null;
        for (TSFNote note : notes) {
            MidiNote midiNote = TSFNote2MidiNote.toMidiNote(note, lastNote, false);
            lastNote = midiNote;
            midiNotes.add(midiNote);
        }

        partitur.add(midiNotes);

        parser = new TSFNoteParser(":d .t :d .f :m :s :f .f :r .r :m :f .f :f .f :d :d :r .r :r .r :d");
        notes = parser.parse();
        midiNotes = new ArrayList<>();
        lastNote = null;
        for (TSFNote note : notes) {
            MidiNote midiNote = TSFNote2MidiNote.toMidiNote(note, lastNote, true);
            lastNote = midiNote;
            midiNotes.add(midiNote);
        }

        partitur.add(midiNotes);

        MidiPlayer player = new MidiPlayer();
        player.playPartitur(partitur);
    }


    //@Test
    public  void testJump() throws Exception {

        TSFNoteParser parser;
        //parser = new TSFNoteParser(": :d .r :m .f :s :s | :l .l :l .l :s :- ] :f .f :f .f :m :m :r .r :r .r :d ]");
        //parser = new TSFNoteParser(": :d .r :m .f :s :s |1 :l .l :l .l :s :- ] :f .f :f .f :m :m :r .r :r .r :d ]1");
        //parser = new TSFNoteParser(": :d .r :m .f :s :s [1 :l .l :l .l :s :- ] |1 :f .f :f .f :m :m :r .r :r .r :d ");
        parser = new TSFNoteParser(": :d .r :m .f :s :s :l .l :l .l :s :- [f :f .f :f .f :m :m :r .r :r .r :d ");
        List<TSFNote> tsfNotes = parser.parse();
        System.out.println(tsfNotes);

        List<MidiNote> midiNotes = MidiPlayer.getMidiNotes(tsfNotes, true, 0);
        System.out.println(midiNotes);

        MidiPlayer player = new MidiPlayer();
        player.play(midiNotes);
    }
}
