package de.jm.tonic.model;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class VerticalSpacePartTest {
    @Test
    public void matches() throws Exception {
        List<String> lines = new ArrayList<>();
        lines.add("10");
        assertTrue(VerticalSpacePart.matches(lines.listIterator()));

        lines = new ArrayList<>();
        lines.add("10pt");
        assertTrue(VerticalSpacePart.matches(lines.listIterator()));

        lines = new ArrayList<>();
        lines.add("10cm");
        assertTrue(VerticalSpacePart.matches(lines.listIterator()));

        lines = new ArrayList<>();
        lines.add("10cm ");
        assertTrue(VerticalSpacePart.matches(lines.listIterator()));

        lines = new ArrayList<>();
        lines.add("10 cm ");
        assertFalse(VerticalSpacePart.matches(lines.listIterator()));
    }

    @Test
    public void toLatex() throws Exception {
    }

}