package de.jm.tonic;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Wolfgang Schneider on 09.08.2017.
 *
 */
public class Tonic2LatexTest {
    @Test
    public void testNumbers() {
        String tonic =
                "* :d   :l \n" +
                        "1. Hal- lo";
        String result = Tonic2Latex.toLatex(tonic);
        String expected = "\\begin{tonic-row}{\\hspace{0pt}}\n" +
                "{L F F L}\n" +
                "& \\nn{d} \\nn{l} \\\\\n" +
                "1.& \\nt{Hal-} \\nt{lo} \\\\\n" +
                "\\end{tonic-row}\n";

        assertEquals(expected, result);

        assertEquals(expected, result);

        tonic = "c: comment\n" +
                "s: %\n" +
                "* :d   :l \n" +
                "1. Hal- lo";
        result = Tonic2Latex.toLatex(tonic);
        expected = "% comment\n" +
                "\\begin{tonic-row}{\\hspace{0pt}\\rse}\n" +
                "{L F F L}\n" +
                "& \\nn{d} \\nn{l} \\\\\n" +
                "1.& \\nt{Hal-} \\nt{lo} \\\\\n" +
                "\\end{tonic-row}\n";

        assertEquals(expected, result);

        tonic = "\n";
        result = Tonic2Latex.toLatex(tonic);
        expected = "\n";

        assertEquals(expected, result);

        tonic = "     \n";
        result = Tonic2Latex.toLatex(tonic);
        expected = "\n";

        assertEquals(expected, result);

        tonic = ":    \n";
        result = Tonic2Latex.toLatex(tonic);
        expected = ":\n";

        assertEquals(expected, result);

        tonic = ": q    \n";
        result = Tonic2Latex.toLatex(tonic);
        expected = ": q\n";

        assertEquals(expected, result);
    }

}
