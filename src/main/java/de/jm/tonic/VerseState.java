package de.jm.tonic;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wolfgang Schneider on 01.09.2017.
 *
 */
class VerseState extends TonicStateBase {
    private static final String prefix = "verse:";
    private List<String> lines = new ArrayList<>();
    private StringBuilder latex = new StringBuilder();
    private boolean onOneLine = false;

    VerseState(List<TonicState> globalStates) {
        super(globalStates);
    }

    @Override
    public TonicState next(String line) {
        if (line == null || line.isEmpty()) {
            globalStates.add(this);
            return new DispatcherState(globalStates).next(line);
        } else if (matches(line) && !lines.isEmpty()) {
            latex.append("}");
            globalStates.add(this);
            onOneLine = true;
            return new VerseState(globalStates).next(line);
        } else {
            lines.add(line);
            return this;
        }
    }

    private void process(String line) {
        if (latex.length() == 0) {
            //latex.append("\\parbox{\\textwidth/2-0.25cm}{");
        } else {
            latex.append(" \\\\\n");
        }
        latex.append(getValue(line).replaceAll("\\|", "\\\\textbar"));
        
    }

    @Override
    public String toLatex() {
        latex = new StringBuilder();

        for (String line : lines) {
            process(line);
        }

        if (onOneLine) {
            latex.insert(0, "\\parbox{\\textwidth/2-0.25cm}{");
            latex.append("}");
            latex.append("\\hspace{0.25cm}");
            latex.append("\\parbox{\\textwidth/2-0.25cm}{");
        } else {
            if (isPreviousOnOneLine()) {
                latex.append("}");
            }
            latex.append("\n");
        }
        return latex.toString();
    }

    private TonicState getPrevious() {
        int index = globalStates.indexOf(this);
        if (index > 0) {
            return globalStates.get(index - 1);
        } else {
            return null;
        }
    }

    private boolean isPreviousOnOneLine() {
        TonicState state = getPrevious();
        return state instanceof VerseState && ((VerseState) state).onOneLine;
    }

    static boolean matches(String line) {
        return line.startsWith(prefix);
    }
}
