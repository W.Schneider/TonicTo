package de.jm.tonic.model;

import de.jm.tonic.latex.NotesPart2Latex;
import de.jm.tonic.midi.TSFNoteParser;
import de.jm.tonic.parser.Voice;

import java.util.List;
import java.util.ListIterator;

public class NotesPart extends SingleLinePart implements SongPart {
    private List<String> widths;

    private NotesPart(String line) {
        super(line);
    }

    public static boolean matches(ListIterator<String> lines) {
        if (lines.hasNext()) {
            String line = SongPart.getLine(lines);

            return isNotesLine(line);
        } else {
            return false;
        }
    }

    private static boolean isNotesLine(String line) {
        final String validChars = "drmfsltaeib_=-*0123456789:;!.,/'? []|cCF%()+>&";
        int colonCount = getCount(':', line);
        int bangCount = getCount('!', line);
        for (char c : line.toCharArray()) {
            if (validChars.indexOf(c) < 0) {
                return false;
            }
        }
        return colonCount > 0 && (colonCount + bangCount > 1);
    }

    private static int getCount(char ch, String line) {
        int count = 0;
        for (char c : line.toCharArray()) {
            if (c == ch) {
                count++;
            }
        }
        return count;
    }

    public static NotesPart parse(ListIterator<String> lines) {
        if (matches(lines)) {
            return new NotesPart(lines.next());
        } else {
            return null;
        }
    }

    @Override
    public String toLatex() {
        return NotesPart2Latex.toLatex(this);
    }

    public Voice getNotes() {
        TSFNoteParser parser = new TSFNoteParser(getLine());
        return parser.parse();
    }

    public boolean isEndOfPart() {
        List<TSFNote> notes = getNotes();
        TSFNote last = notes.get(notes.size() - 1);
        return last.isEndOfPart();
    }

    public List<String> getWidths() {
        return widths;
    }

    public void setWidths(List<String> widths) {
        this.widths = widths;
    }
}

