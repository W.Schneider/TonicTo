package de.jm.tonic.model;

import java.util.ListIterator;

public interface SongPart {
    String toLatex();

    static String getLine(ListIterator<String> lines) {
        String line = lines.next();
        lines.previous();
        return line;
    }
}
