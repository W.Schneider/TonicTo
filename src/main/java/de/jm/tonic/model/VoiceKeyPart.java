package de.jm.tonic.model;

import de.jm.tonic.Symbol2Latex;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class VoiceKeyPart extends SingleLinePart implements SongPart {
    public VoiceKeyPart(String line) {
        super(line);
    }

    @Override
    public String toLatex() {
        return "";
    }


    public static boolean matches(ListIterator<String> lines) {
        if (lines.hasNext()) {
            String line = SongPart.getLine(lines);
            return line.startsWith("v:");
        } else {
            return false;
        }
    }

    public static VoiceKeyPart parse(ListIterator<String> lines) {
        if (matches(lines)) {
            return new VoiceKeyPart(lines.next());
        } else {
            return null;
        }
    }

    public List<String> getVoiceKeys() {
        List<String> keys = new ArrayList<>();
        String value = Symbol2Latex.getValue(getLine());
        for (char c : value.toCharArray()) {
            keys.add(String.valueOf(c));
        }
        return keys;
    }
}
