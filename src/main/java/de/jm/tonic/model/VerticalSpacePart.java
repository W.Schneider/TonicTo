package de.jm.tonic.model;

import java.util.ListIterator;

public class VerticalSpacePart extends SingleLinePart implements SongPart {
    public VerticalSpacePart(String line) {
        super(line);
    }

    public static boolean matches(ListIterator<String> lines) {
        if (lines.hasNext()) {
            String line = SongPart.getLine(lines).trim();
            return line.matches("^-?\\d+\\.?(\\d+)?(pt|cm)?");
        } else {
            return false;
        }
    }

    @Override
    public String toLatex() {
        if (getLine().endsWith("pt") || getLine().endsWith("cm"))  {
            return "\\vspace*{" + getLine() + "}\n";
        } else {
            return "\\vspace*{" + getLine() + "pt}\n";
        }
    }

    public static VerticalSpacePart parse(ListIterator<String> lines) {
        if (matches(lines)) {
            return new VerticalSpacePart(lines.next().trim());
        } else {
            return null;
        }
    }
}
