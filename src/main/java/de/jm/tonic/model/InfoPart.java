package de.jm.tonic.model;

import de.jm.tonic.latex.InfoPart2Latex;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class InfoPart extends SingleLinePart implements SongPart {

    private InfoPart(String line) {
        super(line);
    }

    @Override
    public String toLatex() {
        return InfoPart2Latex.toLatex(this);
    }

    private static List<String> KNOWN_KEYWORDS = new ArrayList<>();
    static {
        KNOWN_KEYWORDS.add("bpm");
        KNOWN_KEYWORDS.add("K");
        KNOWN_KEYWORDS.add("T");
        KNOWN_KEYWORDS.add("C");
        KNOWN_KEYWORDS.add("X");
        KNOWN_KEYWORDS.add("Z");
        KNOWN_KEYWORDS.add("L");
        KNOWN_KEYWORDS.add("sql");
        KNOWN_KEYWORDS.add("c");
        KNOWN_KEYWORDS.add("latex");
        KNOWN_KEYWORDS.add("top");
        KNOWN_KEYWORDS.add("bottom");
        KNOWN_KEYWORDS.add("left");
        KNOWN_KEYWORDS.add("right");
        KNOWN_KEYWORDS.add("small");
        KNOWN_KEYWORDS.add("smaller");
        KNOWN_KEYWORDS.add("smallest");
        KNOWN_KEYWORDS.add("tiny");
        KNOWN_KEYWORDS.add("newpage");
        KNOWN_KEYWORDS.add("landscape");
    }


    public static boolean matches(ListIterator<String> lines) {
        if (lines.hasNext()) {
            String line = SongPart.getLine(lines);
            String key = getKey(line);
            return KNOWN_KEYWORDS.contains(key);
        } else {
            return false;
        }
    }

    private static String getKey(String line) {
        return line.replaceFirst(":.*$", "");
    }

    public String getKey() {
        return getLine().replaceFirst(":.*$", "");
    }

    public String getValue() {
        return getLine().replaceFirst(getKey() + ":", "").trim();
    }

    public static InfoPart parse(ListIterator<String> lines) {
        if (matches(lines)) {
            return new InfoPart(lines.next());
        } else {
            return null;
        }
    }

}
