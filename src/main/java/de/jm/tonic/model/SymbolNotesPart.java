package de.jm.tonic.model;

import de.jm.tonic.latex.SymbolNotesPart2Latex;

import java.util.ListIterator;

public class SymbolNotesPart extends SingleLinePart implements SongPart {
    public SymbolNotesPart(String line) {
        super(line);
    }

    public static boolean matches(ListIterator<String> lines) {
        if (lines.hasNext()) {
            String line = SongPart.getLine(lines);

            return line.startsWith("~");
        } else {
            return false;
        }
    }

    public static SymbolNotesPart parse(ListIterator<String> lines) {
        if (matches(lines)) {
            return new SymbolNotesPart(lines.next());
        } else {
            return null;
        }
    }

    @Override
    public String toLatex() {
        return SymbolNotesPart2Latex.toLatex(this);
    }
}
