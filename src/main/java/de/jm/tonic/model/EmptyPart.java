package de.jm.tonic.model;

public class EmptyPart implements SongPart {
    @Override
    public String toLatex() {
        return "\n";
    }
}
