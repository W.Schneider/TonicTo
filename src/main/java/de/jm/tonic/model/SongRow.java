package de.jm.tonic.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SongRow {

    SongRow(int voices) {
        this();
        for (int i = 0; i < voices; i++) {
            TSFVoice voice = new TSFVoice();
            voice.maleVoice = voices - i <= 2;
            this.voices.add(voice);
        }
    }

    public SongRow() {
        this.voices = new ArrayList<>();
    }

    void adjustVoices(SongRow lastAddedSongRow) {
        int maxLength = getLength();
        for (List<TSFNote> voice : getNoteLines()) {
            int length = length(voice);
            if (length < maxLength) {
                voice.addAll(lastAddedSongRow.getBreakLine());
            }
        }
    }

    private Collection<TSFNote> getBreakLine() {
        List<TSFNote> noteLine = voices.get(0).notes;
        List<TSFNote> breakLine = new ArrayList<>();
        for (TSFNote note : noteLine) {
            TSFNote breakNote = new TSFNote();
            if (note.isMeta()) {
                breakNote.setValue(note.getValue());
            } else {
                breakNote.setAccent(note.getAccent());
                breakNote.setBreak();
                breakNote.setLength(note.getLength());
            }
            breakLine.add(breakNote);
        }
        return breakLine;
    }

    private int getLength() {
        int maxLength = 0;
        for (List<TSFNote> voice : getNoteLines()) {
            int length = length(voice);
            if (length > maxLength) {
                maxLength = length;
            }
        }
        return maxLength;
    }

    class TSFVoice {
        String voiceString;
        List<TSFNote> notes = new ArrayList<>();
        boolean maleVoice = false;
    }

    private List<TSFVoice> voices;

    public void addNoteLine(List<TSFNote> noteLine, String voice) {
        TSFVoice tsfVoice = new TSFVoice();
        tsfVoice.notes = noteLine;
        if (voice != null) {
            tsfVoice.voiceString = voice;
        }
        voices.add(tsfVoice);
    }

    void addNoteLine(List<TSFNote> noteLine, int voice) {
        TSFVoice tsfVoice = getVoice(voice);
        tsfVoice.notes.addAll(noteLine);
    }

    private TSFVoice getVoice(int voice) {
        return voices.get(voice);
    }

    public List<List<TSFNote>> getNoteLines() {
        List<List<TSFNote>> notes = new ArrayList<>();

        for (TSFVoice voice : voices) {
            notes.add(voice.notes);
        }

        return notes;
    }

    private static int length(Collection<TSFNote> notes) {
        int length = 0;
        for (TSFNote note : notes) {
            if (note.isMeta()) {
                continue;
            }
            length += TSFNote2MidiNote.toLength(note.getLength());
        }
        return length;
    }


}

