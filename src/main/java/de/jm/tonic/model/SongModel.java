package de.jm.tonic.model;

import java.util.ArrayList;
import java.util.List;

public class SongModel {
    private Integer bpm = 120;
    private SongInfo songInfo = new SongInfo();

    private List<SongRow> songRows = new ArrayList<>();

    public List<SongRow> getSongRows() {
        return songRows;
    }

    public SongRow getSongRow() {
        return concatSongRows();
    }

    private SongRow concatSongRows() {
        int maxRowCount = getMaxRowCount();

        SongRow oneSongRow = new SongRow(maxRowCount);
        int voice;

        for (SongRow songRow : getSongRows()) {
            int rowCount = songRow.getNoteLines().size();
            voice = maxRowCount - rowCount;
            for (List<TSFNote> notes : songRow.getNoteLines()) {
                oneSongRow.addNoteLine(notes, voice++);
            }
            oneSongRow.adjustVoices(songRow);
        }

        return oneSongRow;
    }

    private int getMaxRowCount() {
        int max = 0;
        for (SongRow songRow : getSongRows()) {
            int length = songRow.getNoteLines().size();
            if (length > max) {
                max = length;
            }
        }

        return max;
    }

    public void addSongRow(SongRow songRow) {
        songRows.add(songRow);
    }

    public void setBPM(Integer bpm) {
        this.bpm = bpm;
    }

    public Integer getBPM() {
        return bpm;
    }

    public SongInfo getSongInfo() {
        return songInfo;
    }
}

