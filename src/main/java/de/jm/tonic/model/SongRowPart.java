package de.jm.tonic.model;

import de.jm.tonic.latex.SongRowPart2Latex;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class SongRowPart implements SongPart {
    private List<SongPart> parts = new ArrayList<>();

    @Override
    public String toLatex() {
        return SongRowPart2Latex.toLatex(this);
    }

    public static boolean matches(ListIterator<String> lines) {
        return NotesPart.matches(lines) || SymbolPart.matches(lines) || VoiceKeyPart.matches(lines);
    }

    public static SongRowPart parse(ListIterator<String> lines) {
        SongRowPart rowPart= new SongRowPart();

        while (matches(lines) || SongTextPart.matches(lines) || SymbolNotesPart.matches(lines)) {
            SongPart part = getPart(lines);
            if (part != null) {
                rowPart.add(part);
            } else {
                lines.previous();
            }
        }

        return rowPart;
    }

    private static SongPart getPart(ListIterator<String> lines) {
        if (SymbolPart.matches(lines)) {
            return SymbolPart.parse(lines);
        } else if (NotesPart.matches(lines)) {
            return NotesPart.parse(lines);
        } else if (SymbolNotesPart.matches(lines)) {
            return SymbolNotesPart.parse(lines);
        } else if (SongTextPart.matches(lines)) {
            return SongTextPart.parse(lines);
        } else if (VoiceKeyPart.matches(lines)) {
            return VoiceKeyPart.parse(lines);
        } else {
            return null;
        }
    }

    private void add(SongPart part) {
        parts.add(part);
    }

    public List<SongPart> getParts() {
        return parts;
    }

    public List<String> getVoiceKeys() {
        for (SongPart part : parts) {
            if (part instanceof VoiceKeyPart) {
                return ((VoiceKeyPart) part).getVoiceKeys();
            }
        }
        return new ArrayList<>();
    }

    public List<NotesPart> getNotesParts() {
        List<NotesPart> notesParts = new ArrayList<>();
        for (SongPart part : parts) {
            if (part instanceof NotesPart) {
                notesParts.add((NotesPart) part);
            }
        }
        return notesParts;
    }
}
