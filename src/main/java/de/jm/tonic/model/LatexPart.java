package de.jm.tonic.model;

public class LatexPart extends SingleLinePart implements SongPart {

    public LatexPart(String line) {
        super(line);
    }

    @Override
    public String toLatex() {
        return getLine();
    }
}
