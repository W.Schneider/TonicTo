package de.jm.tonic.model;

import de.jm.tonic.midi.MidiNote;

import java.util.HashMap;
import java.util.Map;

public class TSFNote2MidiNote {
    private static int OKTAVE = 12;
    private static int FEMALEBASE = 60;
    private static int MALEBASE = FEMALEBASE - OKTAVE;
    public static Map<String, Integer> diffs = new HashMap<>();
    static {
        diffs.put("d", 0);
        diffs.put("de", 1);
        diffs.put("di", 1);
        diffs.put("ra", 1);
        diffs.put("r", 2);
        diffs.put("re", 3);
        diffs.put("ri", 3);
        diffs.put("ma", 3);
        diffs.put("m", 4);
        diffs.put("f", 5);
        diffs.put("fe", 6);
        diffs.put("fi", 6);
        diffs.put("sa", 6);
        diffs.put("s", 7);
        diffs.put("se", 8);
        diffs.put("si", 8);
        diffs.put("ba", 8);
        diffs.put("la", 8);
        diffs.put("l", 9);
        diffs.put("le", 10);
        diffs.put("li", 10);
        diffs.put("ta", 10);
        diffs.put("t", 11);
    }

    public static MidiNote toMidiNote(TSFNote tsfNote, MidiNote previous, boolean male) {
        return toMidiNote(tsfNote, previous, male, 0);
    }

    public static MidiNote toMidiNote(TSFNote tsfNote, MidiNote previous, boolean male, int transpose) {
        MidiNote midiNote = new MidiNote();
        int value = male ? MALEBASE : FEMALEBASE;
        int length = toLength(tsfNote.getLength());

        if (tsfNote.isBreak()) {
            midiNote.setValue(value);
            midiNote.setVolumne(0);
        } else if (tsfNote.isContinue()) {
            if (previous == null || previous.isBreak()) {
                midiNote.setValue(value);
                midiNote.setVolumne(0);
            } else {
                previous.addLength(length);
                return null;
            }
        } else {
            if (tsfNote.isAccented()) {
                midiNote.setAccentedVolume();
            } else if (tsfNote.isMeasure() ||tsfNote.isDoubleMeasure()) {
                midiNote.setMeasureVolume();
            }
            midiNote.setValue(value + diffs.get(tsfNote.getValue()) + (OKTAVE * tsfNote.getPitch()) + transpose);
        }

        midiNote.setLength(length);

        if (tsfNote.isContinue()) {
            if (previous == null || previous.isBreak()) {
                System.out.println("Continue without previous, changed to break.");
            } else {
                midiNote.setLength(previous.getLength() + length);
                return null;
            }
        }
        return midiNote;
    }

    public static int toLength(TSFNote.LENGTH length) {
        switch (length) {
            case THREEQUARTES:
                return 9;
            case TWOTHIRDS:
                return 8;
            case HALF:
                return 6;
            case THIRD:
                return 4;
            case QUARTER:
                return 3;

            default:
                return 12;
        }
    }
}


