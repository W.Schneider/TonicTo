package de.jm.tonic.model;

import de.jm.tonic.latex.Verse2Latex;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class VersePart  implements SongPart {
    private List<String> partLines = new ArrayList<>();
    private boolean startColumns = false;
    private boolean endColumns = false;

    @Override
    public String toLatex() {
        return Verse2Latex.toLatex(this);
    }

    public static boolean matches(ListIterator<String> lines) {
        if (lines.hasNext()) {
            String line = SongPart.getLine(lines);

            return line.startsWith("verse:");
        } else {
            return false;
        }
    }

    public static VersePart parse(ListIterator<String> lines) {
        VersePart part = new VersePart();

        if (matches(lines)) {
            String line = lines.next().replaceFirst("^verse:", "").trim();
            part.add(line);
            if (lines.hasNext()) {
                line = lines.next();
            } else {
                return part;
            }

            while (!line.startsWith("verse:") && !line.trim().isEmpty()) {
                part.add(line);
                if (lines.hasNext()) {
                    line = lines.next();
                } else {
                    line = null;
                    break;
                }
            }
            if (line != null) {
                lines.previous();
            }
            return part;
        } else {
            return null;
        }
    }

    private void add(String line) {
        partLines.add(line);
    }

    public List<String> getPartLines() {
        return partLines;
    }

    public boolean isStartColumns() {
        return startColumns;
    }

    public void setStartColumns(boolean startColumns) {
        this.startColumns = startColumns;
    }

    public boolean isEndColumns() {
        return endColumns;
    }

    public void setEndColumns(boolean endColumns) {
        this.endColumns = endColumns;
    }
}
