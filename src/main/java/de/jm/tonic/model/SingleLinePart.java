package de.jm.tonic.model;

public class SingleLinePart {
    private String line;

    public SingleLinePart(String line) {
        this.line = line;
    }

    public String getLine() {
        return line;
    }
}
