package de.jm.tonic.model;

public class TSFNote {

    public enum Accent {DOUBLEMEASURE, MEASURE, ACCENTED, NORMAL, LENGTH}
    public enum NoteType {NORMAL, CONTINUE, BREAK, DC, DS}
    public enum LENGTH {FULL, THREEQUARTES, TWOTHIRDS, HALF, THIRD, QUARTER}
    private String value = ""; // BREAK
    private LENGTH length = LENGTH.FULL;
    private String prefix = "";
    private String postfix = "";
    private int pitch = 0;
    private Accent accent = Accent.NORMAL;
    private boolean active = true;

    public TSFNote() {
    }

    public TSFNote(String value, LENGTH length, int pitch, Accent accent) {
        setValue(value);
        this.length = length;
        this.pitch = pitch;
        this.accent = accent;

        if (accent == Accent.LENGTH) {
            prefix = toPrefix(length);
        } else {
            prefix = toPrefix(accent);
        }
    }

    private String toPrefix(Accent accent) {
        switch (accent) {
            case MEASURE:
                return "!";
            case DOUBLEMEASURE:
                return "!!";
            case ACCENTED:
                return ";";
            default:
                return ":";
        }
    }

    private String toPrefix(LENGTH length) {
        switch (length) {
            case THREEQUARTES:
                return "//";
            case TWOTHIRDS:
                return ".,";
            case HALF:
                return ".";
            case THIRD:
                return "/";
            case QUARTER:
                return ",";
            default:
                return "";
        }
    }

    public String getValue() {
        return value;
    }
    public String getValueWithPitch() {
        return value + pitch2Tsf();
    }

    private String pitch2Tsf() {
        StringBuilder tsf = new StringBuilder();
        int p = pitch;
        if (p < 0) {
            while (p++ < 0) {
                tsf.append(",");
            }
        } else if (p > 0) {
            while (p-- > 0) {
                tsf.append("'");
            }
        }
        return tsf.toString();
    }

    public void setValue(String value) {
        this.value = value;

        if (isForwardJump()) {
            setActive(false);
        }

        if (isBackwardJump()) {
            setActive(true);
        }
    }

    public LENGTH getLength() {
        return length;
    }

    public void setLength(LENGTH length) {
        this.length = length;
    }

    public int getPitch() {
        return pitch;
    }

    public void setPitch(int pitch) {
        this.pitch = pitch;
    }

    public Accent getAccent() {
        return accent;
    }

    public void setAccent(Accent accent) {
        this.accent = accent;
    }

    public NoteType getType() {
        if (value.isEmpty()) {
            return NoteType.BREAK;
        } else if (value.equals("-")) {
            return NoteType.CONTINUE;
        } else if ("]".equals(value)) {
            return NoteType.DS;
        } else {
            return NoteType.NORMAL;
        }
    }

    public boolean isThird() {
        return  length == LENGTH.THIRD;
    }

    public boolean isTwoThirds() {
        return length ==  LENGTH.TWOTHIRDS;
    }

    public boolean isFull() {
        return length ==  LENGTH.FULL;
    }

    public boolean isContinue() {
        return "-".equals(value);
    }

    public boolean isBreak() {
        return value.isEmpty();
    }

    public boolean isJump() {
        return value.startsWith("]") || value.startsWith("[");
    }

    public boolean isEndOfPart() {
        return "!!!".equals(value);
    }

    public boolean isMeta() {
        return isEndOfPart() || isJump() || value.startsWith("|");
    }

    public void setBreak() {
        value = "";
    }

    public boolean isDoubleMeasure() {
        return Accent.DOUBLEMEASURE == accent;
    }

    public boolean isMeasure() {
        return Accent.MEASURE == accent;
    }

    public boolean isAccented() {
        return Accent.ACCENTED == accent;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String thePrefix) {
        prefix = thePrefix;
    }

    public String getPostfix() {
        return postfix;
    }

    public void setPostfix(String thePostFix) {
        postfix = thePostFix;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void toggleActive() {
        if (isToggleForwardJump()) {
            return;
        }

        if (isForwardJump()) {
            active = true;
        }

        if (isBackwardJump()) {
            active = false;
        }

        if (isJumpTarget()) {
            active = false;
        }
    }

    public boolean isBackwardJump() {
        return value.startsWith("]");
    }

    public boolean isForwardJump() {
        return value.startsWith("[");
    }

    public boolean isToggleBackwardJump() {
        if (!isBackwardJump()) {
            return false;
        }
        String tag = value.substring(1);

        return !tag.isEmpty() && Character.isUpperCase(tag.charAt(0));
    }

    public boolean isToggleForwardJump() {
        if (!isForwardJump()) {
            return false;
        }
        String tag = value.substring(1);

        return !tag.isEmpty() && Character.isUpperCase(tag.charAt(0));
    }

    public boolean isJumpTarget() {
        return "!!!".equals(value) || isDoubleMeasure() || value.startsWith("|");
    }

    public boolean isCoda() {
        return "|C".equals(value);
    }

    public boolean isJumpFine() {
        return "[F".equals(value);
    }

    public boolean isJumpEnd() {
        return "[f".equals(value);
    }

    public boolean isJumpDaCapo() {
        return "]c".equals(value);
    }

    public boolean isJumpCoda() {
        return "[C".equals(value);
    }

    public boolean isJumpAlFine() {
        return "]F".equals(value);
    }

    public boolean isJumpAlCoda() {
        return "]C".equals(value);
    }

    @Override
    public String toString() {
        return "TSFNote{" +
                "value='" + value + '\'' +
                ", length=" + length +
                ", pitch=" + pitch +
                ", accent=" + accent +
                ", type=" + getType() +
                '}';
    }
}

