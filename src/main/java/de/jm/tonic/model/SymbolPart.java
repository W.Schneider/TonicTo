package de.jm.tonic.model;

import de.jm.tonic.latex.SymbolPart2Latex;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class SymbolPart extends SingleLinePart implements SongPart {
    private boolean first;

    public SymbolPart(String line) {
        super(line);
    }

    public static boolean matches(ListIterator<String> lines) {
        if (lines.hasNext()) {
            String line = SongPart.getLine(lines);

            return line.startsWith("s:");
        } else {
            return false;
        }
    }

    public static SymbolPart parse(ListIterator<String> lines) {
        if (matches(lines)) {
            return new SymbolPart(lines.next());
        } else {
            return null;
        }
    }

    @Override
    public String toLatex() {
        List<SymbolPart> parts = new ArrayList<>();
        parts.add(this);
        return SymbolPart2Latex.toLatex(parts);
    }

    public boolean isFirst() {
        return first;
    }

    public void setFirst(boolean first) {
        this.first = first;
    }
}

