package de.jm.tonic.model;

import de.jm.tonic.latex.SongTextPart2Latex;

import java.util.ListIterator;

public class SongTextPart extends SingleLinePart implements SongPart {

    private SongTextPart(String line) {
        super(line);
    }

    @Override
    public String toLatex() {
        return SongTextPart2Latex.toLatex(this);
    }


    public static boolean matches(ListIterator<String> lines) {
        if (lines.hasNext()) {
            String line = SongPart.getLine(lines);
            if (line.trim().isEmpty()) {
                return false;
            } else {
                return !line.contains(":");
            }
        } else {
            return false;
        }
    }


    public static SongTextPart parse(ListIterator<String> lines) {
        if (matches(lines)) {
            return new SongTextPart(lines.next());
        } else {
            return null;
        }
    }

    public boolean isNumbering() {
        String line = getLine();
        boolean match = line.matches("^[0-9]+\\..*");
        return match;
    }
}
