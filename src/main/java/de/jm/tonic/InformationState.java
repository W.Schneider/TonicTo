package de.jm.tonic;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * Created by Wolfgang Schneider on 01.09.2017.
 */
public class InformationState extends TonicStateBase {
    private static Map<String, String> prefixes = new HashMap<>();

    static {
        prefixes.put("C:", "\\renewcommand\\SongComposer{%s}"); // composer
        prefixes.put("L:", "\\renewcommand\\SongLanguage{%s}"); // language
        prefixes.put("T:", "\\renewcommand\\SongTitle{%s}"); // title
        prefixes.put("X:", "\\renewcommand\\SongNumber{%s}"); // song number
        prefixes.put("Z:", "\\renewcommand\\SongTranscription{%s}"); // transcription
        prefixes.put("c:", "%% %s"); // comment
        prefixes.put("K:", "\\renewcommand\\SongPitch{%s}"); // key
        prefixes.put("latex:", "%s");
        prefixes.put("sql:", "\\setqlength{%spt}");
        prefixes.put("left:", "");
        prefixes.put("right:", "");
        prefixes.put("top:", "");
        prefixes.put("bottom:", "");
        prefixes.put("bpm:", "");
    }
    private String line;

    InformationState(List<TonicState> states) {
        super(states);
    }

    @Override
    public TonicState next(String line) {
        if (matches(line)) {
            this.line = line;
            globalStates.add(this);
            return new DispatcherState(globalStates);
        } else {
            return new DispatcherState(globalStates).next(line);
        }
    }

    static boolean matches(String line) {
        if (line == null) {
            return false;
        }

        for (String prefix : prefixes.keySet()) {
            if (line.startsWith(prefix)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public String toLatex() {
        String value = getValue(line);
        String prefix = getPrefix(line);
        String format = prefixes.get(prefix);
        if (format.isEmpty()) {
            return "";
        }

        return String.format(format, value) + "\n";
    }

    String getValueForKey(String key) {
        if (getPrefix(line).equals(key + ":")) {
            return getValue(line);
        }
        return null;
    }
}
