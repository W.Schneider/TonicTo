package de.jm.tonic;

/**
 * Created by Wolfgang Schneider on 04.09.2017.
 *
 */
public class TextState implements TonicState {
    private String line;
    public TextState(String aLine) {
        line = aLine;
    }

    @Override
    public TonicState next(String line) {
        return null;
    }

    @Override
    public String toLatex() {
        return line + "\n";
    }
}
