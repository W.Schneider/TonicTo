package de.jm.tonic;

/**
 * Created by Wolfgang Schneider on 01.09.2017.
 *
 */
interface TonicState {
    TonicState next(String line);

    String toLatex();
}
