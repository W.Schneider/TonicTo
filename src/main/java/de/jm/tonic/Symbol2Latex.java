package de.jm.tonic;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Wolfgang Schneider on 28.08.2017.
 *
 */
public class Symbol2Latex {

    public static Map<String, String> symbols = new HashMap<>();
    static {
        symbols.put("cresc", "\\cresc");
        symbols.put("decresc", "\\decresc");
        symbols.put("refrain", "\\refrain");
        symbols.put("solo", "\\solo");
        symbols.put("^", "\\fer");
        symbols.put("%", "\\rse");
        symbols.put("$", "\\coda");
        symbols.put("ds", "\\rds");
        symbols.put("dc", "\\rdc");

        symbols.put("DS", "\\DS");
        symbols.put("DC", "\\DC");
        symbols.put("1.", "\\first");
        symbols.put("2.", "\\second");
        symbols.put("tfb", "\\tfb");
        symbols.put("tfub", "\\tfub");

        symbols.put("f", "\\lf");
        symbols.put("mf", "\\lmf");
        symbols.put("ff", "\\lff");
        symbols.put("p", "\\lp");
        symbols.put("pp", "\\lpp");
        symbols.put("ppp", "\\lppp");
        symbols.put("mp", "\\lmp");

        symbols.put(">", "\\hfill");
    }

    private static boolean isSymbolLine(String line) {
        return line.startsWith("s:");
    }

    public static String toLatex(String line) {
        return toLatex(line, true);
    }

    public static String toLatex(String line, boolean firstSymbolLine) {
        StringBuilder latex = new StringBuilder();
        if (isSymbolLine(line)) {
            if (!firstSymbolLine) {
                latex.append("\\\\ \\theadline{");
            }

            String value = getValue(line);
            if (!value.isEmpty()) {
                String l = value.replaceAll(" +", " ").replaceAll("_+", "_");
                String[] parts = l.split(" ");
                for (String part : parts) {
                    latex.append(part2latex(part));
                }
            }

            if (!firstSymbolLine) {
                latex.append("}");
            }
        }

        return latex.toString();
    }

    private static String part2latex(String part) {
        StringBuilder latex = new StringBuilder();
        String partValue = getPartValue(part);
        boolean inBox = false;

        if (isFullBox(part)) {
            String boxType = getBoxType(part);
            partValue = partValue.replaceFirst(boxType, "").trim();
            part= part.replaceFirst("_$", "").trim();
            latex.append(symbols.get(boxType));
            if (!partValue.isEmpty()) {
                latex.append("[");
            }
            inBox = true;
        }

        if (partValue.matches("-?[0-9]+(pt|em)")) {
            latex.append("\\hspace{").append(partValue).append("}");
        } else if (partValue.matches("-?[0-9]+")) {
            latex.append("\\hspace{").append(partValue).append("pt}");
        } else if (!partValue.isEmpty()) {
            String[] subParts = partValue.split(" ");
            boolean firstSubPart = true;
            for (String subPart : subParts) {
                if (firstSubPart) {
                    firstSubPart = false;
                } else {
                    latex.append("\\ ");
                }
                String key = subPart;
                if (inBox && key.length() > 1) {
                    key = key.toLowerCase(); // TODO: wirklich klein?
                }
                if (symbols.containsKey(key)) {
                    latex.append(symbols.get(key));
                } else {
                    // correct escaped chars
                    subPart = subPart.replaceAll("\\\\", "");
                    latex.append("\\sign{").append(subPart).append("}");
                }
            }
        }

        if (isFullBox(part)) {
            String length = getBoxLength(part);
            if (!partValue.isEmpty()) {
                latex.append("]");
            }
            latex.append("{").append(length).append("} ");
        }
        return latex.toString();
    }

    private static String getBoxType(String part) {
        if (part.startsWith("DS")) {
            return "DS";
        } else if (part.startsWith("DC")) {
            return "DC";
        } else if (part.startsWith("1.")) {
            return "1.";
        } else if (part.startsWith("2.")) {
            return "2.";
        } else if (part.endsWith("_")) {
            return "tfub";
        } else {
            return "tfb";
        }
    }

    static String getPartValue(String part) {
        String result = part;
        if (isFullBox(part)) {
            result = part.replaceFirst("\\*[0-9]?\\.?[0-9]*_?$", "");
        }

        result = result.replaceAll(">", " > ");
        result = result.replaceAll("[_ ]+", " ");

        return result;
    }

    static String getBoxLength(String part) {
        if (part.endsWith("*")) {
            return "1";
        } else {
            return part.replaceFirst("[^\\*]*\\*", "");
        }
    }

    static boolean isFullBox(String part) {
        return part.matches(".*\\*[0-9]?\\.?[0-9]*_?");
    }

    public static String getValue(String line) {
        return line.replaceFirst(".*:", "").trim();
    }
}
