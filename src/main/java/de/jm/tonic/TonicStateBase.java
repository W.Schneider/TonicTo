package de.jm.tonic;

import java.util.List;

/**
 * Created by Wolfgang Schneider on 01.09.2017.
 *
 */
public abstract class TonicStateBase implements TonicState {
    final List<TonicState> globalStates;
    TonicStateBase(List<TonicState> states) {
        globalStates = states;
    }

    @Override
    public String toLatex() {
        return "";
    }

    @Override
    public String toString() {
        return toLatex();
    }

    String getValue(String line) {
        int pos = line.indexOf(":");
        return line.substring(pos + 1).trim();
    }

    String getPrefix(String line) {
        int pos = line.indexOf(":");
        return line.substring(0, pos + 1).trim();
    }
}
