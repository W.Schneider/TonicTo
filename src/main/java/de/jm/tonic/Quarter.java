package de.jm.tonic;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wolfgang Schneider on 01.09.2017.
 *
 */
class Quarter {
    static int full = 0b100000;
    static int secondQuarter = 0b010000;
    static int secondThird = 0b001000;
    static int half = 0b000100;
    static int thirdThird = 0b000010;
    static int fourthQuarter = 0b000001;
    static int leftFull = 0b1000000;

    private static Notes2Latex notes2Latex = new Notes2Latex();

    private List<String> notes;

    Quarter(String quarterText) {
        notes = notes2Latex.splitQuarter(quarterText);
    }

    int getWidth() {
        int width = full;
        for (int i = 1; i < notes.size(); i++) {
            String note = notes.get(i);
            if (notes2Latex.isHalf(note)) {
                width |= half;
            } else if (notes2Latex.isQuarter(note)) {
                if (i == 1) {
                    width |= secondQuarter;
                } else {
                    width |= fourthQuarter;
                }
            } else if (notes2Latex.isHalfQuarter(note)) {
                width |= half;
                width |= fourthQuarter;
            } else if (notes2Latex.isThirdThird(note)) {
                width |= secondThird;
                width |= thirdThird;
            } else if (notes2Latex.isThird(note)) {
                if (i == 1) {
                    width |= secondThird;
                } else {
                    width |= thirdThird;
                }
            }
        }

        return width;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (String note : notes) {
            result.append(notes2Latex.lexNote(note));
        }

        return result.toString();
    }
}
