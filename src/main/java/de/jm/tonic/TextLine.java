package de.jm.tonic;

/**
 * Created by Wolfgang Schneider on 01.09.2017.
 *
 */
class TextLine extends SongLine {
    private Text2Latex text2Latex = new Text2Latex();

    TextLine(String l) {
        super(l);
        if (line.trim().endsWith("!!")) {
            isEndRow = true;
            line = line.trim();

            line = line.substring(0, line.length() - 2);
        }
    }

    @Override
    public String toString() {
        return text2Latex.lexText(line) + "\\\\";
    }
}
