package de.jm.tonic;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by johann on 16.07.17.
 *
 */
public class Tonic2Latex {
    private static final String beginDocument =
            "\\documentclass[a4paper, 12pt]{article}\n" +
                    "\\usepackage[%s]{geometry}\n" +
                    "\\usepackage{song}\n" +
                    "\\begin{document}\n\n";

    private static final String endDocument =
            "\\end{document}\n";

    public String newLatex(String tonicText) {
        return toLatex(tonicText, true);
    }

    public static String toLatex(String tonicText) {
        return  toLatex(tonicText, false);
    }

    public static String toLatex(String tonicText, boolean createNewDocument) {
        return toLatex(new StringReader(tonicText), createNewDocument);
    }

    public static String toLatex(Reader reader, boolean createNewDocument) {

        List<TonicState> states = new ArrayList<>();

        BufferedReader br = new BufferedReader(reader);
        String line;
        TonicState currentState = new DispatcherState(states);
        try {
            while((line = br.readLine()) != null) {
                currentState = currentState.next(line.trim());
            }
        } catch (IOException e) {
            // ignore
        }
        if (currentState != null) {
            currentState.next(null);
        }

        StringBuilder latex = new StringBuilder();

        if (createNewDocument) {
            latex.append(String.format(beginDocument, getBorders(states)));
        }

        latex.append(toLatexString(states));

        if (createNewDocument) {
            latex.append(endDocument);
        }

        return latex.toString();
    }

    private static String getBorders(List<TonicState> states) {
        String left = getBorder(states, "left", "1.3cm");
        String right = getBorder(states, "right", "1.3cm");
        String top = getBorder(states, "top", "1.3cm");
        String bottom = getBorder(states, "bottom", "1.3cm");
        return "left=" + left + ", right=" + right + ", top=" + top + ", bottom=" + bottom;
    }

    private static String getBorder(List<TonicState> states, String key, String defaultValue) {
        for (TonicState state : states) {
            if (state instanceof InformationState) {
                InformationState informationState = (InformationState) state;
                String left = informationState.getValueForKey(key);
                if (left != null) {
                    return left;
                }
            }
        }
        return defaultValue;
    }

    private static String toLatexString(List<TonicState> states) {
        StringBuilder latex = new StringBuilder();
        states.forEach(state -> latex.append(state.toLatex()));
        return latex.toString();
    }
}

