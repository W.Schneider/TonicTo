package de.jm.tonic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Wolfgang Schneider on 01.09.2017.
 *
 */
public class SongRowState extends TonicStateBase {
    private List<SongLine> lines = new ArrayList<>();
    private static String SymbolLinePrefix = "s:";
    private boolean isEndRow = false;
    SongRowState(List<TonicState> states) {
        super(states);
    }

    @Override
    public TonicState next(String aLine) {
        if (aLine == null) {
            globalStates.add(this);
            return new DispatcherState(globalStates);
        }

        String line = aLine.trim();
        TonicState nextState;

        if (line.startsWith(SymbolLinePrefix)) {
            lines.add(new SymbolLine(line));
            nextState = this;
        } else if (isNotesLine(line)) {
            lines.add(new NoteLine(line));
            nextState = this;
            isEndRow |= isEndRowLine(line);
        } else if (isSongTextLine(line)) {
            lines.add(new TextLine(line));
            nextState = this;
        } else {
            globalStates.add(this);
            nextState = new DispatcherState(globalStates).next(line);
        }

        return nextState;
    }

    private static boolean isValidLine(String line) {
        return line != null && !line.trim().isEmpty();
    }

    static boolean matches(String line) {
        return isValidLine(line) && (isSymbolLine(line) || isNotesLine(line) || isSongTextLine(line)) ;
    }

    private static boolean isSymbolLine(String line) {
        return line.startsWith(SymbolLinePrefix);
    }

    private static boolean isSongTextLine(String line) {
        return isValidLine(line) && !line.contains(":");
    }

    private static boolean isNotesLine(String line) {
        int colonCount = getCount(':', line);
        int bangCount = getCount('!', line);
        return colonCount > 0 && colonCount + bangCount > 1;
    }

    private static boolean isEndRowLine(String line) {
        return line.trim().endsWith("!!");
    }

    private static int getCount(char ch, String line) {
        int count = 0;
        for (char c : line.toCharArray()) {
            if (c == ch) {
                count++;
            }
        }
        return count;
    }

    @Override
    public String toString() {
        return toLatex();
    }

    @Override
    public String toLatex() {
        StringBuilder builder = new StringBuilder();
        List<String> songLines = new ArrayList<>();
        StringBuilder symbols = new StringBuilder("\\hspace{0pt}");
        String rowtype = isEndRow ? "endrow" : "row";
        boolean firstSymbolLine = true;

        for (SongLine songLine : lines) {
            if (songLine instanceof SymbolLine) {
                SymbolLine symbolLine = (SymbolLine) songLine;
                if (firstSymbolLine) {
                    symbols.append(symbolLine.toLatex(true));
                    firstSymbolLine = false;
                } else {
                    symbols.append("\\\\\n").append(symbolLine.toLatex(false));
                }
            } else {
                songLines.add(songLine.toString());
            }
        }

        if (!songLines.isEmpty()) {
            builder.append("\\begin{tonic-").append(rowtype).append("}{").append(symbols).append("}\n");
            builder.append(getColumns()).append("\n");
            songLines.forEach(s -> builder.append(s).append("\n"));
            builder.append("\\end{tonic-").append(rowtype).append("}\n");
        }

        return builder.toString();
    }

    private String getColumns() {
        List<Integer> widths = new ArrayList<>();
        boolean first = true;
        boolean hasNumber = false;
        for (SongLine songLine : lines) {
            if (songLine instanceof NoteLine) {
                NoteLine noteLine = (NoteLine) songLine;
                List<Integer> lineWidths = noteLine.getWidths();
                if (first) {
                    widths = lineWidths;
                    first = false;
                } else {
                    for (int i = 0; i < lineWidths.size(); i++) {
                        if (i == widths.size()) {
                            widths.add(Quarter.full);
                        }
                        int totalWidth = widths.get(i);
                        int width = lineWidths.get(i);
                        widths.set(i, totalWidth | width);
                    }
                }
            } else if (songLine instanceof TextLine) {
                if (songLine.line.trim().matches("^[1-9]\\..*")) {
                    hasNumber = true;
                }
            }
        }

        if (hasNumber) {
            widths.set(0, Quarter.leftFull);
        }

        StringBuilder columns = new StringBuilder();
        columns.append("{");
        for (Integer width : widths) {
            columns.append(widthToString(width));
        }
        columns.append("L}");
        return columns.toString();
    }

    private static Map<Integer, String> width2String = new HashMap<>();
    static {
        width2String.put(Quarter.leftFull, "L ");
        width2String.put(Quarter.full, "F ");
        width2String.put(Quarter.full | Quarter.half, "HH ");
        width2String.put(Quarter.full | Quarter.half | Quarter.fourthQuarter, "HQQ ");
        width2String.put(Quarter.full | Quarter.secondQuarter | Quarter.half, "QQH ");
        width2String.put(Quarter.full | Quarter.secondQuarter | Quarter.half | Quarter.fourthQuarter, "QQQQ ");
        width2String.put(Quarter.full | Quarter.secondThird | Quarter.thirdThird, "TTT ");
    }

    private static String widthToString(int width) {
        int w = checkThirds(width);
        return width2String.getOrDefault(w, "?");
    }

    private static int checkThirds(int width) {
        int isNoThird = Quarter.secondQuarter | Quarter.half | Quarter.fourthQuarter;
        int isThird = Quarter.secondThird | Quarter.thirdThird;
        int removeThirds = Quarter.full | isNoThird;

        if ((width & isThird) > 0
            && (width & isNoThird) > 0 ) {
            return width & removeThirds;
        }

        return width;
    }
}
