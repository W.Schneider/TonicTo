package de.jm.tonic.midi;

import de.jm.tonic.model.*;

import javax.sound.midi.*;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;

public class MidiPlayer {

    // https://github.com/jline/jline3
    public void playSongModel(SongModel model) throws Exception {
        playSequence(getSequence(model));
    }

    void play(List<MidiNote> notes) throws Exception {
        final int PPQS = 12;
        final int STAKKATO = 0; // verkuerzte Tonlaenge
        Sequence seq = new Sequence(Sequence.PPQ, PPQS);

        addMidiNotesToSequence(seq, notes, PPQS, STAKKATO);

        setBpm(120, 0, seq);

        playSequence(seq);
    }

    void playPartitur(List<List<MidiNote>> partitur) throws Exception {

        final int PPQS = 12;
        final int STAKKATO = 0; // verkuerzte Tonlaenge

        Sequence seq = new Sequence(Sequence.PPQ, PPQS);

        for (List<MidiNote> notes : partitur) {
            addMidiNotesToSequence(seq, notes, PPQS, STAKKATO);
        }

        setBpm(120, 0, seq);

        playSequence(seq);
    }

    // TODO: Events for Title,... https://breno.sarmen.to/midi_documentation/list.html
    private void setBpm(int bpm, int tick, Sequence sequence) throws Exception {

        int mpq = 60000000 / bpm;
        MetaMessage tempoMsg = new MetaMessage();
        tempoMsg.setMessage(0x51 ,new byte[] {
                (byte)(mpq>>16 & 0xff),
                (byte)(mpq>>8 & 0xff),
                (byte)(mpq & 0xff)
        },3);
        MidiEvent tempoEvent = new MidiEvent(tempoMsg, tick);
        sequence.getTracks()[0].add(tempoEvent);
    }

    // https://www.recordingblogs.com/wiki/midi-key-signature-meta-message
    private void setKey(String key, Sequence sequence) throws Exception {
        int major = 0;
        int scale = getScale(key);


        MetaMessage message = new MetaMessage();
        message.setMessage(0x59 ,new byte[] {
                (byte)(scale),
                (byte)(major)
        },2);
        MidiEvent midiEvent = new MidiEvent(message, 0);
        sequence.getTracks()[0].add(midiEvent);
    }

    private static final List<String> SHARPS = Arrays.asList("d", "s", "r", "l", "m", "t", "fe", "de");
    private static final List<String> FLATS = Arrays.asList("d", "f", "ta", "ma", "la", "ra", "sa", "da");
    private int getScale(String key) {
        if (key == null ) {
            return 0;
        }

        int index = 0;

        if (key.contains("=")) {
            String scale = key.split("=")[1].trim();
            index = SHARPS.indexOf(scale);
            if (index < 0) {
                index = FLATS.indexOf(scale);
                if (index < 0) {
                    index = 0;
                } else {
                    index = -index;
                }
            }

        }

        return index;
    }

    private void setText(String text, Sequence sequence) throws Exception {
        if (text == null) {
            return;
        }

        byte[] chars = text.getBytes("ISO-8859-15");

        MetaMessage message = new MetaMessage();
        message.setMessage(0x01, chars, chars.length);
        MidiEvent midiEvent = new MidiEvent(message, 0);
        sequence.getTracks()[0].add(midiEvent);
    }

    private void setTitle(String title, Sequence sequence) throws Exception {

        byte[] chars = title.getBytes("ISO-8859-15");

        MetaMessage message = new MetaMessage();
        message.setMessage(0x03, chars, chars.length);
        MidiEvent midiEvent = new MidiEvent(message, 0);
        sequence.getTracks()[0].add(midiEvent);
    }

    public void writeSongModel(SongModel model, File file) throws Exception {
        Sequence sequence = getSequence(model);
        MidiSystem.write(sequence, 1, file);
    }

    private void addMidiNotesToSequence(Sequence seq, List<MidiNote> notes, int PPQS, int STAKKATO)
            throws InvalidMidiDataException {
        Track track = seq.createTrack();
        long currentTick = 0;
        ShortMessage msg;

        for (MidiNote note : notes) {
            if (note.getVolumne() == 0) {
                // handling of breaks
                currentTick += PPQS/12 * note.getLength();
            } else {
                msg = new ShortMessage();
                msg.setMessage(ShortMessage.NOTE_ON, 0, note.getValue(), note.getVolumne());
                track.add(new MidiEvent(msg, currentTick));

                currentTick += PPQS/12 * note.getLength() - STAKKATO;
                msg = new ShortMessage();
                msg.setMessage(ShortMessage.NOTE_OFF, 0, note.getValue(), 0);
                track.add(new MidiEvent(msg, currentTick));

                currentTick += STAKKATO;
            }
        }
    }

    private Sequence getSequence(SongModel model) throws Exception {
        final int PPQS = 12;
        final int STAKKATO = 0; // verkuerzte Tonlaenge

        Sequence sequence = new Sequence(Sequence.PPQ, PPQS);

        int transpose = getTranspose(model.getSongInfo().getKey());
        SongRow oneRow = model.getSongRow();
        int maxRows = oneRow.getNoteLines().size();
        int rowNumber = 0;

        for (List<TSFNote> voice : oneRow.getNoteLines()) {
            List<MidiNote> midiNotes = getMidiNotes(voice,maxRows - rowNumber <= 2, transpose);
            rowNumber++;
            addMidiNotesToSequence(sequence, midiNotes, PPQS, STAKKATO);
        }

        setBpm(model.getBPM(), 0, sequence);
        SongInfo info = model.getSongInfo();
        setTitle(info.getTitle(), sequence);
        setKey(info.getKey(), sequence);
        setText(info.getText(), sequence);


        return sequence;
    }

    private List<Sequence> getSequences(SongModel model) throws InvalidMidiDataException {
        final int PPQS = 96;
        final int STAKKATO = 0; // verkuerzte Tonlaenge

        List<Sequence> sequences = new ArrayList<>();

        int transpose = getTranspose(model.getSongInfo().getKey());

        for (SongRow row : model.getSongRows()) {
            int rowNumber = 0;
            Sequence seq = new Sequence(Sequence.PPQ, PPQS);
            sequences.add(seq);
            for (List<TSFNote> notes : row.getNoteLines()) {
                List<MidiNote> midiNotes = getMidiNotes(notes,rowNumber++ > 1, transpose);
                addMidiNotesToSequence(seq, midiNotes, PPQS, STAKKATO);
            }
        }

        return sequences;
    }


    private int getTranspose(String key) {
        if (key == null) {
            return 0;
        }
        Integer transpose = TSFNote2MidiNote.diffs.get(key);
        if (transpose == null) {
            // split key (do = m)
            key = key.replaceAll(".*=", "").trim();
            transpose = TSFNote2MidiNote.diffs.get(key);

            if (transpose == null) {
                transpose = 0;
            }
        }

        return transpose;
    }


    private void playSequence(Sequence seq) throws Exception {
        //Sequencer und Synthesizer initialisieren
        Sequencer sequencer = MidiSystem.getSequencer();

        Transmitter transmitter = sequencer.getTransmitter();
        Synthesizer synthesizer = MidiSystem.getSynthesizer();
        Receiver receiver = synthesizer.getReceiver();
        //Beide oeffnen und verbinden
        sequencer.open();
        synthesizer.open();
        transmitter.setReceiver(receiver);
        //Sequence abspielen
        sequencer.setSequence(seq);
        sequencer.start();

        while (true) {
            try {
                Thread.sleep(100);
            } catch (Exception e) {
                //nothing
            }
            if (!sequencer.isRunning()) {
                break;
            }
        }
        //Sequencer anhalten und Geraete schliessen
        sequencer.stop();
        sequencer.close();
        synthesizer.close();
    }

    static List<MidiNote> getMidiNotes(List<TSFNote> notes, boolean male, int transpose) {
        List<MidiNote> midiNotes = new ArrayList<>();
        MidiNote lastNote = null;
        ListIterator<TSFNote> iter = notes.listIterator();
        Character tagActive = null;

        while (iter.hasNext()) {
            TSFNote note = iter.next();
            if (note.isToggleBackwardJump() && note.isActive()) {
                tagActive = note.getValue().charAt(1);
            }

            if (note.isToggleForwardJump() && isTagActive(note, tagActive)) {
                note.setActive(true);
            }

            if (note.isJumpEnd()) {
                break;
            }
            if (note.isJumpFine()) {
                if (note.isActive()) {
                    break;
                }
                note.toggleActive();
            } else if (note.isJump() && note.isActive()) {
                if (note.isToggleForwardJump()) {
                    if (isTagActive(note, tagActive)) {
                        jump(note, iter);
                    }
                } else {
                    jump(note, iter);
                    note.toggleActive();
                }
            } else if (note.isForwardJump()) {
                note.toggleActive();
            }

            if (note.isMeta()) {
                //System.out.println(note.getValue());
            } else {
                MidiNote midiNote = TSFNote2MidiNote.toMidiNote(note, lastNote, male, transpose);
                if (midiNote != null) {
                    lastNote = midiNote;
                    midiNotes.add(midiNote);
                }
            }
        }
        return midiNotes;
    }

    private static boolean isTagActive(TSFNote note, Character tagActive) {
        return tagActive != null && tagActive == note.getValue().charAt(1);
    }

    private static void jump(TSFNote note, ListIterator<TSFNote> iter) {
        if (!note.isJump() || !note.isActive()) {
            return;
        }

        if (note.isBackwardJump()) {
            while (iter.hasPrevious()) {
                TSFNote previous = iter.previous();
                if (note.isJumpDaCapo()) {
                    continue;
                }
                if (isCorrectJumpTarget(note, previous)) {
                    return;
                }
            }
        } else {
            while (iter.hasNext()) {
                TSFNote next = iter.next();
                if (note.isJumpCoda() && !next.isCoda()) {
                    continue;
                }
                if (isCorrectJumpTarget(note, next)) {
                    if (next.isDoubleMeasure()) {
                        iter.previous();
                    }
                    return;
                }
            }
        }
    }

    private static boolean isCorrectJumpTarget(TSFNote note, TSFNote target) {
        if (!target.isJumpTarget()) {
            return false;
        }

        String sourceTag = note.getValue().substring(1);
        String value = target.getValue();
        String targetTag;
        if (value.isEmpty()) {
            targetTag = "";
        } else {
            targetTag = target.getValue().substring(1);
        }

        if (sourceTag.isEmpty()) {
            return targetTag.isEmpty() || target.isEndOfPart() || target.isDoubleMeasure();
        }

        return sourceTag.equals(targetTag);
    }
}

