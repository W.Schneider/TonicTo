package de.jm.tonic.midi;

public class TSFScanner implements Scanner {
    private String line;
    private int pos;

    public TSFScanner(String line) {
        this.line = line;
    }

    @Override
    public Character peek() {
        if (!end()) {
            return line.charAt(pos);
        } else {
            return null;
        }
    }

    @Override
    public Character consume() {
        if (pos >= line.length()) {
            throw new RuntimeException("Already at end of line");
        } else {
            Character c = peek();
            pos++;
            return c;
        }
    }

    @Override
    public boolean end() {
        return pos >= line.length();
    }

    @Override
    public String toString() {
        return line.substring(pos);
    }

    @Override
    public Character get(int relativePos) {
        int p = pos + relativePos;
        if (p < line.length()) {
            return line.charAt(p);
        } else {
            return null;
        }
    }

    @Override
    public int getCurrentPos() {
        return pos;
    }
}

