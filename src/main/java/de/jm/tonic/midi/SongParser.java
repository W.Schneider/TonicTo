package de.jm.tonic.midi;

import de.jm.tonic.model.*;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;


public class SongParser implements SongParserInfo {

    private int fileRow = 0;
    private int row = 0;
    private int voice = 0;

    public List<SongPart> parseParts(ListIterator<String> lines) { // all lines of file
        List<SongPart> parts = new ArrayList<>();
        while (lines.hasNext()) {

            if (InfoPart.matches(lines)) {
                parts.add(InfoPart.parse(lines));
            } else if (VersePart.matches(lines)) {
                VersePart part = VersePart.parse(lines);
                SongPart previousPart = getPreviousPart(parts);
                if (previousPart instanceof VersePart) {
                    VersePart previous = (VersePart) previousPart;
                    if (!previous.isEndColumns()) {
                        previous.setStartColumns(true);
                        part.setEndColumns(true);
                    }
                }
                parts.add(part);
            } else if (SongRowPart.matches(lines)) {
                parts.add(SongRowPart.parse(lines));
            } else if (VerticalSpacePart.matches(lines)) {
                parts.add(VerticalSpacePart.parse(lines));
            } else {
                String line = lines.next();
                if (line.trim().isEmpty()) {
                    parts.add(new EmptyPart());
                } else {
                    parts.add(new LatexPart(line));
                }
            }
        }
        return parts;
    }

    public SongModel parse(ListIterator<String> lines) { // all lines of file

        List<SongPart> parts = parseParts(lines);
        //debug(parts);
        return SongModelParser.parse(parts);
    }

    private SongPart getPreviousPart(List<SongPart> parts) {
        if (parts.isEmpty()) {
            return null;
        } else {
            return parts.get(parts.size() - 1);
        }
    }

    private void debug(List<SongPart> parts) {
        parts.forEach(p -> System.out.println(p.getClass().getName()));
    }

    @Override
    public int getCurrentFileRow() {
        return fileRow;
    }

    @Override
    public int getCurrentSongRow() {
        return row;
    }

    @Override
    public int getCurrentVoice() {
        return voice;
    }

    @Override
    public String toString() {
        return  "voice=" + voice +
                ", row=" + row +
                ", fileRow=" + fileRow;
    }
}

