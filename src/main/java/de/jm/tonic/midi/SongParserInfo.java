package de.jm.tonic.midi;

public interface SongParserInfo {

    int getCurrentFileRow();
    int getCurrentSongRow();
    int getCurrentVoice();
}
