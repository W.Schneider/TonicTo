package de.jm.tonic.midi;

public interface Scanner {
    Character peek();
    Character consume();
    Character get(int relativePos);
    boolean end();
    int getCurrentPos();
}
