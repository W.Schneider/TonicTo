package de.jm.tonic.midi;

import de.jm.tonic.model.*;

import java.util.Arrays;
import java.util.List;

public class SongModelParser {

    public static SongModel parse(List<SongPart> parts) {
        SongModel model = new SongModel();

        for (SongPart part : parts) {
            if (part instanceof InfoPart) {
                parseInfoPart(model, (InfoPart) part);
            } else if (part instanceof SongRowPart) {
                parseSongRowPart(model, (SongRowPart) part);
            } else {
                // do nothing
            }
        }

        return model;
    }

    private static void parseSongRowPart(SongModel model, SongRowPart rowPart) {
        SongRow songRow = new SongRow();
        List<NotesPart> notesParts = rowPart.getNotesParts();
        List<String> voiceKeys = getVoiceKeys(rowPart, notesParts);

        for (int i = 0; i < notesParts.size(); i++) {
            NotesPart part = notesParts.get(i);
            String voiceKey = voiceKeys.get(i);
            TSFNoteParser parser = new TSFNoteParser(part.getLine().replaceAll("\\?", ""));
            songRow.addNoteLine(parser.parse(), voiceKey);
        }
        model.addSongRow(songRow);
    }

    private static List<String> getVoiceKeys(SongRowPart rowPart, List<NotesPart> notesParts) {
        List<String> voiceKeys = rowPart.getVoiceKeys();
        if (voiceKeys.isEmpty()) {
            if (notesParts.size() <= 4) {
                voiceKeys = Arrays.asList("s", "a", "t", "b");
            } else if (notesParts.size() == 5)  {
                voiceKeys = Arrays.asList("S", "s", "a", "t", "b");
            } else if (notesParts.size() == 6)  {
                voiceKeys = Arrays.asList("S", "S", "s", "a", "t", "b");
            } else {
                System.out.println("More than 6 voices, don't know what to do! Use 'v:'");
                System.exit(1);
            }
            System.out.print("guessing ");
        }
        System.out.println("voices: " + voiceKeys);
        return voiceKeys;
    }

    private static void parseInfoPart(SongModel model, InfoPart part) {
        String key = part.getKey();
        String value = part.getValue();
        value = value.replaceAll("^ *\\\\\\\\", "");
        value = value.replaceAll("\\\\\\\\", ", ");

        switch (key) {
            case "bpm":
                model.setBPM(Integer.parseInt(value));
                break;
            case "K":
                model.getSongInfo().setKey(value);
                break;
            case "T":
                model.getSongInfo().setTitle(value);
                break;
            case "C":
                model.getSongInfo().setText(value);
                break;

        }
    }

}
