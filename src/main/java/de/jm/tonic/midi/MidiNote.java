package de.jm.tonic.midi;


/*
Frauenstimme:
d = c
d' = c1

Maennerstimme:
d = C
d, = C1

 */

import java.util.HashMap;
import java.util.Map;

public class MidiNote {
    private static final int NORMAL_VOLUME = 65;
    private static final int ACCENTED_VOLUME = NORMAL_VOLUME;
    private static final int MEASURE_VOLUME = NORMAL_VOLUME;
    private int value;
    private int volumne = NORMAL_VOLUME;
    private int length;

    private static Map<Integer, String> toSolfa = new HashMap<>();
    static {
        toSolfa.put(0, "d");
        toSolfa.put(1, "di");
        toSolfa.put(2, "r");
        toSolfa.put(3, "ri");
        toSolfa.put(4, "m");
        toSolfa.put(5, "f");
        toSolfa.put(6, "fi");
        toSolfa.put(7, "s");
        toSolfa.put(8, "ta");
        toSolfa.put(9, "t");
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getVolumne() {
        return volumne;
    }

    public void setVolumne(int volumne) {
        this.volumne = volumne;
    }

    public void setAccentedVolume() {
        this.volumne = ACCENTED_VOLUME;
    }

    public void setMeasureVolume() {
        this.volumne = MEASURE_VOLUME;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public void addLength(int length) {
        this.length += length;
    }

    public boolean isBreak() {
        return volumne == 0;
    }

    @Override
    public String toString() {
        return "MidiNote{" +
                "value=" + toSolfa.getOrDefault(value % 48, Integer.toString(value)) +
                ", volumne=" + volumne +
                ", length=" + length +
                '}';
    }
}

