package de.jm.tonic.midi;

import de.jm.tonic.model.TSFNote;
import de.jm.tonic.parser.Voice;

import java.util.*;

public class TSFNoteParser {
    private final SongParserInfo songParserInfo;
    private static List<Character> startNote = Arrays.asList('!', ';', ':', '.', ',', '/', ']', '[', '|');
    private static List<Character> secondNote = Arrays.asList('!', 'd', 'r', 'm', 'f', 's', 'l', 't', 'b' ,' ', '-', ','
            ,'?', '1', '2', '3', 'c', 'C', 'F');
    private static final Character HIGH = '\'';
    private static final Character LOW = ',';
    private static final Character HALF = '.';
    private static final Character QUARTER = ',';
    private static final Character THIRD = '/';
    private static final Character MEASURE = '!';
    private static final Character ACCENTED = ';';
    private static final Character NORMAL = ':';

    private static Map<Character, List<Character>> validValues = new HashMap<>();
    static {
        validValues.put('d', Arrays.asList('e', 'i', 'a'));
        validValues.put('r', Arrays.asList('e', 'i', 'a'));
        validValues.put('m', Collections.singletonList('a'));
        validValues.put('f', Arrays.asList('e', 'i'));
        validValues.put('s', Arrays.asList('e', 'i', 'a'));
        validValues.put('l', Arrays.asList('e', 'i', 'a'));
        validValues.put('t', Collections.singletonList('a'));
        validValues.put('b', Collections.singletonList('a'));
        validValues.put('-', Collections.emptyList());
        validValues.put('[', Arrays.asList('1', '2', '3', 'C', 'F', 'f')); // coda, Fine, jump to the end
        validValues.put(']', Arrays.asList('1', '2', '3', 'c', 'C', 'F')); // da Capo, dal Coda, dal Fine
        validValues.put('|', Arrays.asList('1', '2', '3', 'C', 'F')); // jump target; coda, fine
        validValues.put('?', Collections.emptyList());
    }

    private Scanner scanner;
    private Voice notes = new Voice();

    public TSFNoteParser(String line) {
        songParserInfo = null;
        scanner = new TSFScanner(line);
    }

    public TSFNoteParser(String line, SongParserInfo info) {
        songParserInfo = info;
        scanner = new TSFScanner(line);
    }

    public Voice parse() {
        skipToNextNote(); // ignore everything up to the first note
        TSFNote note = parseNote();
        while(note != null) {
            notes.add(note);
            skipToNextNote();
            note = parseNote();
        }

        return notes;
    }

    String skipNextPlainNote() {
        StringBuilder note = new StringBuilder();
        Character c = scanner.peek();

        if ("drmfsltb".indexOf(c) >= 0) {
            note.append(c);
            scanner.consume();
            c = scanner.peek();
            if ("aei".indexOf(c) >= 0) {
                note.append(c);
                scanner.consume();
                c = scanner.peek();
            }
        }
        if (",'".indexOf(c) >= 0) {
            note.append(c);
            scanner.consume();
        }
        return note.toString();
    }

    String skipToNextNote() {
        StringBuilder builder = new StringBuilder();
        if (scanner.end()) {
            return "";
        }

        // "=1.3 !d,"
        while (!scanner.end() && !startNote.contains(scanner.peek())) {
            builder.append(scanner.peek());
            scanner.consume();
        }

        // ".3 !d,"
        Character current = scanner.peek(); // .
        Character next = scanner.get(1); // 3
        if (next != null) {
            if (current == '.' && next == ',' || current == '/' && next == '/') {  // todo was ist mit ,,d
                return builder.toString();
            } else if (current == '.' && Character.isDigit(next)) {
                builder.append(current);
                scanner.consume();
                builder.append(skipToNextNote());
            } else if(!secondNote.contains(next)) {
                builder.append(scanner.peek());
                scanner.consume();
                if (songParserInfo == null) {
                    System.out.println("skipped possible note '" + builder + "'");
                } else {
                    System.out.println("skipped possible note '" + builder + "' on line "
                            + (songParserInfo.getCurrentFileRow() + 1) + ", col " + (scanner.getCurrentPos()));
                }
                builder.append(skipToNextNote());
            }
        }

        return builder.toString();
    }

    boolean isMeta(Character c) {
        return c != null && (']' == c || '[' == c || '|' == c);
    }

    TSFNote parseMeta() {
        TSFNote note = new TSFNote();
        note.setValue(parseValue());
        String postfix = skipToNextNote();
        note.setPostfix(postfix);
        return note;
    }

    TSFNote parseNote() {
        TSFNote note = new TSFNote();
        Character c = scanner.peek();

        if (isMeta(c)) {
            note = parseMeta();
        } else {

            // first accent or length
            if (isAccent(c)) {
                String accent = getAccentPrefix();
                note.setPrefix(accent);
                note.setAccent(parseAccent(accent));
            } else if (isLength(c)) {
                String length = getLengthPrefix();
                note.setPrefix(length);
                setLength(note, parseLength(length));
                note.setAccent(TSFNote.Accent.LENGTH);
            } else {
                // TODO: not a note
                return null;
            }

            if (scanner.end() && note.isDoubleMeasure()) {
                note.setValue("!!!");
                return note;
            }

            c = scanner.peek();

            // second value
            if (isValue(c)) {
                note.setValue(parseValue());
            } else {
                String postfix = skipToNextNote();
                note.setPostfix(postfix);
                return note;
            }

            c = scanner.peek();

            // third pitch
            if (isPitch(c)) {
                note.setPitch(parsePitch());
            }

            c = scanner.peek();
            if (c != null && (c == '+' || c == '&')) {
                note.setPostfix(String.valueOf(c));
                scanner.consume();
            } else if (c != null && c == '-') {
                scanner.consume();
                String plainNote = skipNextPlainNote();
                String postfix = skipToNextNote();
                note.setPostfix(c + plainNote + (postfix == null ? "" : postfix));
            } else {
                String postfix = skipToNextNote();
                note.setPostfix(postfix);
            }
        }

        return note;
    }

    private void setLength(TSFNote note, TSFNote.LENGTH length) {
        TSFNote previous;
        if (notes.isEmpty())  {
            previous = new TSFNote();
        } else {
            previous = notes.get(notes.size() - 1);
        }

        if (previous.getLength() == TSFNote.LENGTH.QUARTER && length == TSFNote.LENGTH.THREEQUARTES) {
            previous.setLength(TSFNote.LENGTH.HALF);
        } else if (previous.getLength() != TSFNote.LENGTH.QUARTER) {
            previous.setLength(length);
        }

        switch (length) {
            case QUARTER:
            case THIRD:
            case HALF:
                note.setLength(length);
                break;

            case TWOTHIRDS:
                note.setLength(TSFNote.LENGTH.THIRD);
                break;

            case THREEQUARTES:
                note.setLength(TSFNote.LENGTH.QUARTER);
                break;
        }

    }

    boolean isAccent(Character c) {
        return c == MEASURE || c == ACCENTED || c == NORMAL;
    }


    String getAccentPrefix() {
        StringBuilder prefix = new StringBuilder();

        Character c = scanner.peek();
        if (c == MEASURE) {
            prefix.append(c);
            scanner.consume();
            c = scanner.peek();
            if (c == MEASURE) {
                prefix.append(c);
                scanner.consume();
            }
        } else if (c == ACCENTED) {
            prefix.append(c);
            scanner.consume();
        } else if (c == NORMAL) {
            scanner.consume();
            prefix.append(c);
        } else {
            // todo: illegal accent
            return null;
        }
        return prefix.toString();
    }

    static TSFNote.Accent parseAccent(String accent) {
        TSFScanner scanner = new TSFScanner(accent);
        Character c = scanner.peek();
        if (c == MEASURE) {
            scanner.consume();
            c = scanner.peek();
            if (c == MEASURE) {
                scanner.consume();
                return TSFNote.Accent.DOUBLEMEASURE;
            } else {
                return TSFNote.Accent.MEASURE;
            }

        } else if (c == ACCENTED) {
            scanner.consume();
            return TSFNote.Accent.ACCENTED;
        } else if (c == NORMAL) {
            scanner.consume();
            return TSFNote.Accent.NORMAL;
        } else {
            // todo: illegal accent
            return null;
        }
    }

    boolean isLength(Character c) {
        return c == HALF || c == QUARTER || c == THIRD;
    }

    String getLengthPrefix() {
        StringBuilder prefix = new StringBuilder();
        Character c = scanner.peek();

        if (c == HALF) {
            prefix.append(c);
            scanner.consume();

            c = scanner.peek();
            if (c != null && c == ',') {
                prefix.append(c);
                scanner.consume();
            }
        } else if (c == THIRD) {
            prefix.append(c);
            scanner.consume();

            c = scanner.peek();
            if (c != null && c == THIRD)  {
                prefix.append(c);
                scanner.consume();
            }
        } else if (c == QUARTER) {
            prefix.append(c);
            scanner.consume();

            c = scanner.peek();
            if (c != null && c == QUARTER)  {
                prefix.append(c);
                scanner.consume();
            }
        }

        return prefix.toString();
    }

    static TSFNote.LENGTH parseLength(String length) {
        TSFScanner scanner = new TSFScanner(length);
        Character c = scanner.peek();

        if (c == HALF) {
            scanner.consume();
            c = scanner.peek();
            if (c != null && c == ',') {
                scanner.consume();
                return TSFNote.LENGTH.THREEQUARTES;
            } else {
                return TSFNote.LENGTH.HALF;
            }
        } else if (c == THIRD) {
            scanner.consume();
            c = scanner.peek();
            if (c != null && c == THIRD)  {
                scanner.consume();
                return TSFNote.LENGTH.TWOTHIRDS;
            } else {
                return TSFNote.LENGTH.THIRD;
            }
        } else if (c == QUARTER) {
            scanner.consume();
            c = scanner.peek();
            if (c != null && c == QUARTER)  {
                scanner.consume();
                return TSFNote.LENGTH.THREEQUARTES;
            } else {
                return TSFNote.LENGTH.QUARTER;
            }

        } else {
            // todo no length
            return TSFNote.LENGTH.FULL;
        }
    }


    boolean isValue(Character c) {
        return validValues.containsKey(c);
    }

    String parseValue() {
        Character c = scanner.peek();
        List<Character> second = validValues.get(c);
        if (second == null) {
            return "";
        }

        scanner.consume();
        Character sec = scanner.peek();
        if (second.contains(sec)) {
            scanner.consume();
            return String.valueOf(c) + sec;
        }

        return String.valueOf(c);
    }

    boolean isPitch(Character c) {
        return HIGH == c || LOW == c;
    }

    int parsePitch() {
        Character c = scanner.peek();
        int  pitch = 0;
        if (HIGH == c) {
            while (HIGH == c) {
                pitch++;
                scanner.consume();
                c = scanner.peek();
            }
        } else if (LOW == c) {
            while (LOW == c) {
                pitch--;
                scanner.consume();
                c = scanner.peek();
            }
        }

        return pitch;
    }

}

