package de.jm.tonic;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wolfgang Schneider on 01.09.2017.
 *
 */
class NoteLine extends SongLine {
    private List<Quarter> quarters = new ArrayList<>();

    NoteLine(String l) {
        super(l);
        if (line.trim().endsWith("!!")) {
            isEndRow = true;
            line = line.substring(0, line.length() - 2);
        }
        line = line.trim();
        Notes2Latex notes2Latex = new Notes2Latex();
        for (String qt : notes2Latex.splitIntoQuarters(line)) {
            quarters.add(new Quarter(qt));
        }
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (Quarter quarter : quarters) {
            result.append(quarter);
        }
        return result + "\\\\";
    }

    List<Integer> getWidths() {
        List<Integer> widths = new ArrayList<>();
        for (Quarter quarter : quarters) {
            widths.add(quarter.getWidth());
        }
        return widths;
    }
}
