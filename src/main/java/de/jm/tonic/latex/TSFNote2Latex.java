package de.jm.tonic.latex;

import de.jm.tonic.Notes2Latex;
import de.jm.tonic.midi.TSFScanner;
import de.jm.tonic.model.TSFNote;
import de.jm.tonic.model.TSFNote2MidiNote;
import de.jm.tonic.parser.QuarterIterator;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



public class TSFNote2Latex {

    public static String toLatex(String note) {
        return Notes2Latex.lexNote(note);
    }

    public static String toLatex(TSFNote note) {
        String noteString = getNoteString(note);
        return toLatex(noteString);
    }

    static String toLatexNew(TSFNote note) {
        StringBuilder latex = new StringBuilder();

        TSFScanner postfix = new TSFScanner(note.getPostfix());
        latex.append("\\");
        if (note.getPostfix().startsWith("-")) {
            latex.append(parseKeyChangeNote(note));
        } else {
            latex.append(parseMulti(postfix));
            latex.append(parseAccent(note.getAccent()));
            latex.append(parseLength(note.getPrefix()));
            latex.append(parsePitch(note.getPitch()));
            latex.append(parseUnderPointOrLine(postfix));
            if (note.isContinue()) {
                latex.append("{").append("\\cs").append("}");
            } else {
                latex.append("{").append(note.getValue()).append("}");
            }


            latex.append(parsePostfix(postfix.toString().trim()));
        }

        return latex.toString();
    }

    private static String parseKeyChangeNote(TSFNote note) {
        //Todo: works only if pitches are 0
        StringBuilder latex = new StringBuilder();

        latex.append(parseAccent(note.getAccent())).append("s");
        latex.append("\\hit{");
        latex.append(note.getValue());
        latex.append("}");
        latex.append(note.getPostfix().substring(1));
        return latex.toString();
    }

    private static String parsePostfix(String postfix) {

        postfix = postfix.replace("*", "&");

        if (postfix.contains(">")) {
            return "\\hfill{" + postfix.replaceFirst(".*>", "") + "}";
        } else {
            return postfix;
        }
    }

    private static String parseMulti(TSFScanner postfixScanner) {
        if (postfixScanner.end()) {
            return "";
        }

        Character c = postfixScanner.peek();
        if (c == '*') {
            postfixScanner.consume();
            return "d";
        } else {
            return "";
        }
    }

    static String parseUnderPointOrLine(TSFScanner postfixScanner) {
        if (postfixScanner.end()) {
            return "";
        }

        Character c = postfixScanner.peek();

        StringBuilder latex = new StringBuilder();

        if (c == '_') {
            latex.append("u");
            postfixScanner.consume();
        } else if (c == '=') {
            latex.append("p");
            postfixScanner.consume();
        } else {
            return "";
        }

        String number = parseNumber(postfixScanner);
        if (number.isEmpty()) {
            number = "1";
        }

        if (!"1".equals(number)) {
            latex.append("[").append(number).append("]");
        }

        return latex.toString();
    }

    static String parseNumber(TSFScanner postfixScanner) {
        StringBuilder number = new StringBuilder();
        Character c = postfixScanner.peek();
        while (c != null && Character.isDigit(c)) {
            number.append(c);
            postfixScanner.consume();
            c = postfixScanner.peek();
        }

        if (number.length() > 0 && c != null && c == '.') {
            number.append(c);
            postfixScanner.consume();
            c = postfixScanner.peek();
            while (c != null && Character.isDigit(c)) {
                number.append(c);
                postfixScanner.consume();
                c = postfixScanner.peek();
            }
        }

        return number.toString();
    }


    public static String toLatexNewQuarter(List<TSFNote> notes, List<String> widths) {
        StringBuilder latex = new StringBuilder();
        TSFNote previous = null;
        QuarterIterator iter = new QuarterIterator(notes);
        int n = 0;
        while (iter.hasNext()) {
            int widthPos = 0;
            int length = TSFNote2MidiNote.toLength(TSFNote.LENGTH.FULL);
            String width = widths.get(n++); // QQH
            List<TSFNote> quarter = iter.next();
            for (TSFNote note : quarter) {
                String postfix = note.getPostfix();

                if (previous != null) {
                    if (latex.length() > 0) {
                        latex.append("& ");
                    }
                    latex.append(toLatexNew(previous, note));
                    previous = null;
                } else if (postfix.matches("[+&]")) {
                    previous = note;
                } else {
                    if (latex.length() > 0) {
                        latex.append("& ");
                    }

                    latex.append(toLatexNew(note));

                    int columns = getCols(note);
                    int remaining = TSFNote2MidiNote.toLength(note.getLength());
                    length -= remaining;
                    if (length > 0) {
                        if (",".equals(note.getPrefix())) {
                            remaining = TSFNote2MidiNote.toLength(TSFNote.LENGTH.QUARTER);
                        } else if (note.getLength() == TSFNote.LENGTH.TWOTHIRDS) {
                            remaining = TSFNote2MidiNote.toLength(TSFNote.LENGTH.THIRD);
                        } else if (note.getLength() == TSFNote.LENGTH.THREEQUARTES) {
                            remaining = TSFNote2MidiNote.toLength(TSFNote.LENGTH.HALF);
                        }
                    }
                    remaining -= getLength(width.charAt(widthPos++));
                    columns--;

                    while (remaining > 0) {
                        if (columns <= 0) {
                            latex.append("& ");
                        }
                        columns--;
                        remaining -= getLength(width.charAt(widthPos++));
                    }
                }
            }
        }

        latex.append(" \\\\");
        return latex.toString();
    }

    private static int getLength(char columnSign) {
        switch (columnSign) {
            case 'F': return 12;
            case 'H': return 6;
            case 'T': return 4;
            case 'Q': return 3;
            default: return 0;
        }
    }

    private int add(int pos, TSFNote note) {
        //int remainingLength = TSFNote2MidiNote.toLength(TSFNote.LENGTH.FULL);
        return pos + TSFNote2MidiNote.toLength(TSFNote.LENGTH.FULL);
    }

    private static int getCols(TSFNote note) {
        int cols = 1;
        String prefix = note.getPrefix();

        if (".,".equals(prefix) || /* ",,".equals(prefix) ||*/ "//".equals(prefix)) {
            cols = 2;
        }

        TSFScanner scanner = new TSFScanner(note.getPostfix());
        if (!scanner.end()) {
            Character c = scanner.peek();
            while (c != null && c == '*') {
                scanner.consume();
                c = scanner.peek();
                cols++;
            }
        }

        return cols;
    }

    public static String toLatex(List<TSFNote> notes) {
        StringBuilder latex = new StringBuilder();
        TSFNote previous = null;

        for (TSFNote note : notes) {
            if (note.isMeta()) {
                continue;
            }

            String postfix = note.getPostfix();
            if (postfix == null) {
                postfix = "";
            } else {
                postfix = postfix.trim();
            }

            if (previous != null) {
                latex.append(toLatex(previous, note));
                previous = null;
            } else if (postfix.equals("+")) {
                previous = note;
            } else if (postfix.equals("&")) {
                previous = note;
            } else if (postfix.equals("-")) {
                previous = note;
            } else if (postfix.isEmpty()) {
                latex.append(toLatex(note));
                previous = null;
            } else {
                if (postfix.matches("[ *]+")) {
                    latex.append(toLatex(note.getPrefix() + note.getValueWithPitch()));
                    postfix = postfix.replace(" ", "");
                    postfix = postfix.replace("*", "&");
                    latex.append(postfix);
                } else {
                    System.out.println("ignored postfix >" + postfix + "<");
                    postfix = postfix.split(" ")[0];
                    note.setPostfix(postfix);
                    latex.append(toLatex(note));
                }
            }
        }
        return latex.toString() + "\\\\";
    }

    private static String parsePitch(int pitch) {
        if (pitch == -1) {
            return "l";
        } else if (pitch == 1) {
            return "h";
        } else if (pitch > 1) {
            return "h[" + pitch + "]";
        } else if (pitch < -1) {
            return "l[" + (pitch * -1) + "]";
        }
        return "n";
    }

    private static String parseLength(String prefix) {
        switch (prefix) {
            case ".":
                return "h";
            case ".,":
                return "hq";
            case ",,":
                return "hhq";
            case ",":
                return "q";
            case "/":
                return "t";
            case "//":
                return "tt";
            default:
                return "";
        }
    }

    private static String parseAccent(TSFNote.Accent accent) {
        switch (accent) {
            case DOUBLEMEASURE: return "d";
            case MEASURE: return "m";
            case ACCENTED: return "a";
            case NORMAL: return "n";
            default: return "";
        }
    }

    static String toLatexNew(TSFNote previous, TSFNote note) {
        StringBuilder latex = new StringBuilder();

        String previousPostfix = previous.getPostfix().trim();

        if (previousPostfix.equals("-")) {
            // todo key change not correct yet if underline or underpoint
            latex.append("\\hit{");
            latex.append(toLatexNew(previous));
            latex.deleteCharAt(latex.length() - 1);
            latex.append("}");
            latex.append(toLatexNew(note));
        } else if (previousPostfix.equals("+")) {
            previous.setPostfix("");
            latex.append(toLatexNew(previous));
            latex.append(toLatexNew(note));
        } else if (previousPostfix.equals("&")) {
            latex.append("\\multi{");
            latex.append(toLatexNew(previous));
            latex.deleteCharAt(latex.length() - 1);
            latex.append(toLatexNew(note));
            latex.append("}");
        } else {
            System.out.println("Skipped previous and current note: " + previous + ", " + note);
        }

        return latex.toString();
    }

    static String toLatex(TSFNote previous, TSFNote note) {
        StringBuilder latex = new StringBuilder();
        String previousPostfix = previous.getPostfix().trim();

        if (previousPostfix.equals("-")) {
            // todo key change not correct yet if underline or underpoint
            String previousString = previous.getValueWithPitch() + previous.getPostfix();
            String currentString = note.getValueWithPitch() + note.getPostfix();
            return getAccentLatex(previous) + Notes2Latex.lexNote(previousString + currentString);
        } else if (previousPostfix.equals("+")) {
            latex.append(getAccentLatex(previous));
            latex.append("\\pn{");
            latex.append(Notes2Latex.getPlainNote(previous.getValueWithPitch()));
            latex.append(getLengthLatex(note)).append("{");
            latex.append(Notes2Latex.getPlainNote(note.getValueWithPitch()));
            latex.append("}}");
        } else if (previousPostfix.equals("&")) {
            latex.append(getAccentLatex(previous));
            latex.append("\\dpn{");
            latex.append(Notes2Latex.getPlainNote(previous.getValueWithPitch()));
            latex.append(getLengthLatex(note));
            if (note.getPitch() == 0) {
                latex.append("{");
            }
            latex.append(Notes2Latex.getPlainNote(note.getValueWithPitch()));
            if (note.getPitch() == 0) {
                latex.append("}");
            }
            latex.append("}");
        } else {
            System.out.println("Skipped previous and current note: " + previous + ", " + note);
        }
        return latex.toString();
    }


    public static String getNoteString(TSFNote note) {
        return note.getPrefix() + note.getValueWithPitch() + note.getPostfix();
    }

    static String getNoteStringWithoutPostfix(TSFNote note) {
        return note.getPrefix() + note.getValueWithPitch();
    }

    static String getAccentLatex(TSFNote note) {
        switch (note.getAccent()) {
            case MEASURE:
                return "\\ms";
            case DOUBLEMEASURE:
                return "\\ds";
            case ACCENTED:
                return "\\as";
            default:
                return "\\ns";
        }
    }

    static String getLengthLatex(TSFNote note) {
        switch (note.getPrefix()) {
            case ".":
                return "\\hs";
            case ",":
                return "\\qs";
            case ",,":
            case ".,":
                return "\\hqs";
            case "/":
                return "\\ts";
            case "//":
                return "\\tts";
            default:
                return "\\ns";
        }
    }

    public static String getFloat(String s) {
        Pattern p = Pattern.compile("[0-9]*\\.?[0-9]+");

        Matcher m = p.matcher(s);

        if (m.find()) {
            return m.group();
        }

        return "";
    }
}
