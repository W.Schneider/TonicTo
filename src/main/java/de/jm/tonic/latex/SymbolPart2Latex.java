package de.jm.tonic.latex;

import de.jm.tonic.Symbol2Latex;
import de.jm.tonic.model.SymbolPart;

import java.util.List;

public class SymbolPart2Latex {

    public static String toLatex(List<SymbolPart> symbolParts) {
        StringBuilder latex = new StringBuilder("{");
        for (int i = 0; i < symbolParts.size(); i++) {
            String line = symbolParts.get(i).getLine();
            latex.append(Symbol2Latex.toLatex(line, i == 0));
        }
        latex.append("}\n");
        return latex.toString();
    }
}
