package de.jm.tonic.latex;

import de.jm.tonic.Text2Latex;
import de.jm.tonic.model.SymbolNotesPart;

public class SymbolNotesPart2Latex {
    public static String toLatex(SymbolNotesPart part) {
        Text2Latex text2Latex = new Text2Latex();
        return text2Latex.lexText(part.getLine()) + "\\\\";
    }
}