package de.jm.tonic.latex;

import de.jm.tonic.model.NotesPart;

public class NotesPart2Latex {
    public static String toLatex(NotesPart notesPart) {
        return TSFNote2Latex.toLatexNewQuarter(notesPart.getNotes(), notesPart.getWidths());
    }
}
