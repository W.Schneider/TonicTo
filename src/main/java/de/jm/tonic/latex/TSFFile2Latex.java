package de.jm.tonic.latex;

import de.jm.tonic.midi.SongParser;
import de.jm.tonic.model.InfoPart;
import de.jm.tonic.model.SongPart;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class TSFFile2Latex {

    private static final String beginDocument =
            "\\documentclass[a4paper, 12pt]{article}\n" +
                    "\\usepackage[%s]{geometry}\n" +
                    "\\usepackage{song}\n" +
                    "\\begin{document}\n\n";

    private static final String endDocument = "\\end{document}\n";

    public static String toLatex(List<SongPart> parts) {
        StringBuilder latex = new StringBuilder();
        for (SongPart part : parts) {
            latex.append(part.toLatex());
        }
        return latex.toString();
    }

    public static String toLatex(String tsfFile, boolean completeDocument) throws IOException {
        StringBuilder latex = new StringBuilder();
        List<String> rows = Files.readAllLines(Paths.get(tsfFile), StandardCharsets.UTF_8);
        SongParser parser = new SongParser();
        List<SongPart> parts = parser.parseParts(rows.listIterator());
        latex.append(toLatex(parts));

        if (completeDocument) {
            latex.insert(0, String.format(beginDocument, getBorders(parts)));
            latex.append(endDocument);
        } else {
            latex.insert(0, String.format("\\SongData{%s}{%s}{%s}\n", getTitle(parts), getComposer(parts), getKey(parts)));
        }
        return latex.toString();
    }

    private static String getTitle(List<SongPart> parts) {
        return getValue(parts, "T");
    }

    private static String getComposer(List<SongPart> parts) {
        return getValue(parts, "C");
    }

    private static String getKey(List<SongPart> parts) {
        return getValue(parts, "K");
    }

    private static String getValue(List<SongPart> parts, String key) {
        for (SongPart part : parts) {
            if (part instanceof InfoPart) {
                InfoPart infoPart = (InfoPart) part;
                if (key.equals(infoPart.getKey())) {
                    return infoPart.getValue();
                }
            }
        }
        return "";
    }

    private static String getBorders(List<SongPart> parts) {
        String top = "1.3cm";
        String bottom = "1.3cm";
        String left = "1.3cm";
        String right = "1.3cm";
        boolean landscape = false;

        for (SongPart part : parts) {
            if (part instanceof InfoPart) {
                InfoPart infoPart = (InfoPart) part;
                if ("top".equals(infoPart.getKey())) {
                    top = infoPart.getValue();
                } else if ("bottom".equals(infoPart.getKey())) {
                    bottom = infoPart.getValue();
                } else if ("left".equals(infoPart.getKey())) {
                    left = infoPart.getValue();
                } else if ("right".equals(infoPart.getKey())) {
                    right = infoPart.getValue();
                } else if ("landscape".equals(infoPart.getKey())) {
                    landscape = true;
                }
            }
        }

        String borders = "left=" + left + ", right=" + right + ", top=" + top + ", bottom=" + bottom;
        return borders + (landscape ? (",landscape") : "");
    }
}
