package de.jm.tonic.latex;

import de.jm.tonic.model.InfoPart;

import java.util.HashMap;
import java.util.Map;

public class InfoPart2Latex {
    private static Map<String, String> key2Latex = new HashMap<>();

    static {
        key2Latex.put("C", "\\renewcommand\\SongComposer{%s}"); // composer
        key2Latex.put("L", "\\renewcommand\\SongLanguage{%s}"); // language
        key2Latex.put("T", "\\renewcommand\\SongTitle{%s}"); // title
        key2Latex.put("X", "\\renewcommand\\SongNumber{%s}"); // song number
        key2Latex.put("Z", "\\renewcommand\\SongTranscription{%s}"); // transcription
        key2Latex.put("c", "%% %s"); // comment
        key2Latex.put("K", "\\renewcommand\\SongPitch{%s}"); // key
        key2Latex.put("latex", "%s");
        key2Latex.put("sql", "\\setqlength{%spt}");
        key2Latex.put("left", "");
        key2Latex.put("right", "");
        key2Latex.put("top", "");
        key2Latex.put("bottom", "");
        key2Latex.put("bpm", "");
        key2Latex.put("small", "\\small");
        key2Latex.put("smaller", "\\footnotesize");
        key2Latex.put("smallest", "\\scriptsize");
        key2Latex.put("tiny", "\\tiny");
        key2Latex.put("newpage", "\\newpage");
        key2Latex.put("landscape", "");
    }

    public static String toLatex(InfoPart part) {
        String key = part.getKey();
        String value = part.getValue();
        if (key2Latex.containsKey(key)) {
            String format = key2Latex.get(key);
            return String.format(format, value) + "\n";
        } else {
            System.out.println("skipped key: " + key);
            return "\n";
        }
    }
}
