package de.jm.tonic.latex;

import de.jm.tonic.model.*;
import de.jm.tonic.parser.VerticalQuarter;
import de.jm.tonic.parser.Voice;

import java.util.*;

public class SongRowPart2Latex {
    static class Meta {
        boolean endRow = false;
        boolean numbering = false;
    }

    public static String toLatex(SongRowPart songRowPart) {
        List<SongPart> parts = songRowPart.getParts();
        Meta meta = analyse(parts);

        StringBuilder noteLines = new StringBuilder();
        List<Voice> voices = getVoices(songRowPart);
        List<SymbolPart> symbolParts = new ArrayList<>();
        List<String> columns = Arrays.asList(getColumns(voices).split(" "));
        for (SongPart part : parts) {
            if (part instanceof SymbolPart) {
                symbolParts.add((SymbolPart) part);
            } else if (part instanceof NotesPart) {
                NotesPart notesPart = (NotesPart) part;
                if (meta.numbering) {
                    noteLines.append("& ");
                }
                notesPart.setWidths(columns);
                noteLines.append(notesPart.toLatex()).append("\n");
            } else {
                noteLines.append(part.toLatex()).append("\n");
            }
        }

        StringBuilder latex = new StringBuilder();

        latex.append("\\begin{tonic-").append(meta.endRow ? "endrow" : "row").append("}");
        latex.append(SymbolPart2Latex.toLatex(symbolParts));

        latex.append("{").append(meta.numbering ? "L " : "").append(getColumns(voices)).append("}\n");
        latex.append(noteLines);
        latex.append("\\end{tonic-").append(meta.endRow ? "endrow" : "row").append("}\n\n");

        return latex.toString();
    }

    private static List<Voice> getVoices(SongRowPart songRowPart) {
        List<Voice> voices = new ArrayList<>();
        for (SongPart part : songRowPart.getParts()) {
            if (part instanceof NotesPart) {
                voices.add(((NotesPart) part).getNotes());
            }
        }
        return voices;
    }

    private static Meta analyse(List<SongPart> parts) {
        Meta meta = new Meta();
        for (SongPart part : parts) {
            if (part instanceof NotesPart) {
                meta.endRow = ((NotesPart) part).isEndOfPart();
            } else if (part instanceof SongTextPart) {

                meta.numbering |= ((SongTextPart) part).isNumbering();
            }
        }

        return meta;
    }

    private static String getColumns(List<Voice> voices) {
        StringBuilder builder = new StringBuilder();
        List<VerticalQuarter> verticalQuarters = VerticalQuarter.asQuarterList(voices) ;
        List<Integer> lengths = new ArrayList<>();
        for (VerticalQuarter quarter : verticalQuarters) {
            lengths.add(quarter.getWidth());
        }

        builder.append(getLengthString(lengths)).append("L ");
        return builder.toString();
    }

    private static Map<Integer, String> Width2String = new HashMap<>();
    static {
        Width2String.put(0, "");
        Width2String.put(VerticalQuarter.LEFTFULL, "L ");
        Width2String.put(VerticalQuarter.FULL, "F ");
        Width2String.put(VerticalQuarter.FULL | VerticalQuarter.HALF, "HH ");
        Width2String.put(VerticalQuarter.FULL | VerticalQuarter.HALF | VerticalQuarter.FOURTHQUARTER, "HQQ ");
        Width2String.put(VerticalQuarter.FULL | VerticalQuarter.SECONDQUARTER | VerticalQuarter.HALF, "QQH ");
        Width2String.put(VerticalQuarter.FULL | VerticalQuarter.SECONDQUARTER | VerticalQuarter.HALF | VerticalQuarter.FOURTHQUARTER, "QQQQ ");
        Width2String.put(VerticalQuarter.FULL | VerticalQuarter.SECONDTHIRD | VerticalQuarter.THIRDTHIRD, "TTT ");
    }

    static String getLengthString(List<Integer> lengths) {
        StringBuilder builder = new StringBuilder();
        for (int length : lengths) {
            builder.append(widthToString(length));
        }
        return builder.toString();
    }

    static String widthToString(int width) {
        int w = checkThirds(width);
        return Width2String.getOrDefault(w, "?");
    }

    static int checkThirds(int width) {
        int isNoThird = VerticalQuarter.SECONDQUARTER | VerticalQuarter.HALF | VerticalQuarter.FOURTHQUARTER;
        int isThird = VerticalQuarter.SECONDTHIRD | VerticalQuarter.THIRDTHIRD;
        int removeThirds = VerticalQuarter.FULL | isNoThird;

        if ((width & isThird) > 0 && (width & isNoThird) > 0 ) {
            return width & removeThirds;
        }

        return width;
    }
}
