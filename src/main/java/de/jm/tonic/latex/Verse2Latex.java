package de.jm.tonic.latex;

import de.jm.tonic.model.VersePart;

import java.util.List;

public class Verse2Latex {

    public static String toLatex(VersePart part) {
        StringBuilder latex = new StringBuilder();
        List<String>  lines = part.getPartLines();

        for (String line : lines) {
            latex.append(line).append("\\\\\n");
        }

        if (part.isStartColumns()) {
            latex.insert(0, "\\parbox{\\textwidth/2-0.25cm}{");
            latex.append("}");
            latex.append("\\hspace{0.25cm}");
            latex.append("\\parbox{\\textwidth/2-0.25cm}{");
        } else if (part.isEndColumns()) {
            latex.append("}");
            latex.append("\n");
        }

        return latex.toString();
    }
}
