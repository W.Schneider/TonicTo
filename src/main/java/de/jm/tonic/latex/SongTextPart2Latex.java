package de.jm.tonic.latex;

import de.jm.tonic.Text2Latex;
import de.jm.tonic.model.SongTextPart;

public class SongTextPart2Latex {
    public static String toLatex(SongTextPart part) {
        Text2Latex text2Latex = new Text2Latex();
        return text2Latex.lexText(part.getLine()) + "\\\\";
    }
}