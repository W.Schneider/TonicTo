package de.jm.tonic;

import java.util.List;

/**
 * Created by Wolfgang Schneider on 01.09.2017.
 *
 */
public class DispatcherState extends TonicStateBase {
    DispatcherState(List<TonicState> states) {
        super(states);
    }

    @Override
    public TonicState next(String line) {
        if (line == null) {
            // end of file
            return null;
        }

        if (InformationState.matches(line)) {
            return new InformationState(globalStates).next(line);
        } else if (VerseState.matches(line)) {
            return new VerseState(globalStates).next(line);
        } else if (SongRowState.matches(line)) {
            return new SongRowState(globalStates).next(line);
        } else if (line.isEmpty()) {
            globalStates.add(new EmptyState(globalStates));
            return this;
        } else {
            globalStates.add(new TextState(line));
            return this;
        }
    }

}

