package de.jm.tonic;

import java.util.List;

/**
 * Created by Wolfgang Schneider on 01.09.2017.
 *
 */
public class EmptyState extends TonicStateBase {
    EmptyState(List<TonicState> states) {
        super(states);
    }

    @Override
    public TonicState next(String line) {
        globalStates.add(this);

        return new DispatcherState(globalStates);
    }

    @Override
    public String toLatex() {
        return "\n";
    }
}
