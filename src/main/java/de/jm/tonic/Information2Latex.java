package de.jm.tonic;

/**
 * Created by johann on 16.07.17.
 *
 */
class Information2Latex {

    private static final String noteLineSign = "N:";
    private static final String songtextLineSign = "w:";
    private static final String keyLineSign = "K:";
    private static final String composerLineSign = "C:";
    private static final String languageLineSign = "L:";
    private static final String titleLineSign = "T:";
    private static final String songNumberLineSign = "X:";
    private static final String transcriptionLineSign = "Z:";
    private static final String commentLineSign = "c:";
    private static final String setQLengthLineSign = "sql:";
    private static final String latexLineSign = "latex:";

    static boolean isInformationLine(String line) {
        return isKeyLine(line) || isComposerLine(line) || isLanguageLine(line)
                || isSongNumberLine(line) || isTitleLine(line) || isTranscriptionLine(line)
                || isCommentLine(line) || isSetQLengthLine(line) || isLatexLine(line);
    }

    private static boolean isKeyLine(String line) {
        return startsWith(line, keyLineSign);
    }

    private static boolean isComposerLine(String line) {
        return startsWith(line, composerLineSign);
    }

    private static boolean isLanguageLine(String line) {
        return startsWith(line, languageLineSign);
    }

    private static boolean isTitleLine(String line) {
        return startsWith(line, titleLineSign);
    }

    private static boolean isSongNumberLine(String line) {
        return startsWith(line, songNumberLineSign);
    }

    private static boolean isTranscriptionLine(String line) {
        return startsWith(line, transcriptionLineSign);
    }

    private static boolean isSetQLengthLine(String line) {
        return startsWith(line, setQLengthLineSign);
    }

    private static boolean isCommentLine(String line) {
        return startsWith(line, commentLineSign);
    }

    private static boolean isLatexLine(String line) {
        return startsWith(line, latexLineSign);
    }

    private static boolean startsWith(String line, String text) {
        return line != null && line.startsWith(text);
    }

    String toLatex(String line) {
        String latex = line;

        if (isInformationLine(line)) {
            String value = getValue(line);
            if (!value.isEmpty()) {
                if (isComposerLine(line)) {
                    latex = "\\renewcommand\\SongComposer{" + value + "}";
                } else if (isKeyLine(line)) {
                    latex = "\\renewcommand\\SongPitch{" + value + "}";
                } else if (isLanguageLine(line)) {
                    latex = "\\renewcommand\\SongLanguage{" + value + "}";
                } else if (isSongNumberLine(line)) {
                    latex = "\\renewcommand\\SongNumber{" + value + "}";
                } else if (isTitleLine(line)) {
                    latex = "\\renewcommand\\SongTitle{" + value + "}";
                } else if (isTranscriptionLine(line)) {
                    //latex = "\\renewcommand\\SongTransscription{" + value + "}";
                    latex = "";
                } else if (isSetQLengthLine(line)) {
                    latex = "\\setqlength{" + value + (value.matches(".*[0-9]$") ? "pt}" : "}");
                } else if (isCommentLine(line)) {
                    latex = "% " + value;
                } else if (isLatexLine(line)) {
                    latex = value;
                }
            }
        }

        return latex;
    }

    private String getValue(String line) {
        int pos = line.indexOf(":");
        return line.substring(pos + 1).trim();
    }
}

