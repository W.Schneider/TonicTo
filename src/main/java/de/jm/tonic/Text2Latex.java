package de.jm.tonic;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by johann on 16.07.17.
 *
 */
public class Text2Latex {

    private boolean symbolLine = false;

    public String lexText(String line) {
        if (line.contains("~")) {
            symbolLine = true;
            line = line.replaceAll("~", "");
        } else {
            symbolLine = false;
        }

        StringBuilder latex = new StringBuilder();
        String l = line.replaceAll(" +", " ");
        l = l.replaceAll("^ ", "");
        String[] parts = l.split(" ");
        for (String part : parts) {
            latex.append(lexTextPart(part));
        }

        return latex.toString();
    }

    private static String[] normalTextLatex = { "\\nt{", "\\dnt{", "\\tnt{", "\\fnt{", "\\mcnt{"};
    private static String[] normalTextRightLatex = { "\\nt{", "\\dntr{", "\\tntr{", "\\fntr{", "\\mcntr{"};
    private static String[] measureTextLatex = { "\\mt{", "\\dmt{", "\\tmt{", "\\fmt{", "\\mcmt{"};
    private static String[] measureTextRightLatex = { "\\mtr{", "\\dmtr{", "\\tmtr{", "\\fmtr{", "\\mcmtr{"};
    private static String[] doubleMeasureTextLatex= { "\\dt{", "\\ddt{", "\\tdt{", "\\fdt{", "\\mcdt{"};
    private static String[] doubleMeasureTextRightLatex= { "\\dtr{", "\\ddtr{", "\\tdtr{", "\\fdtr{", "\\mcdtr{"};
    private static Map<String, String[]> text2latex;
    private static Map<Character, String> symbols;

    static {
        text2latex = new HashMap<>();
        text2latex.put("M", measureTextLatex);
        text2latex.put("MR", measureTextRightLatex);
        text2latex.put("N", normalTextLatex);
        text2latex.put("NR", normalTextRightLatex);
        text2latex.put("D", doubleMeasureTextLatex);
        text2latex.put("DR", doubleMeasureTextRightLatex);

        symbols = new HashMap<>();
        symbols.put('M', "\\ms");
        symbols.put('N', "");
        symbols.put('D', "\\ds");
    }

    String lexTextPart(String part) {
        StringBuilder latex = new StringBuilder();
        String key;
        String p = part.trim();

        if (p.equals("!!")) {
            return " ";
        } else if (p.equals("!")) {
            return measureTextLatex[0] + "} ";
        } else if (p.startsWith("!!")) {
            key = "D";
            p = p.substring(2);
        } else if (p.startsWith("!")) {
            key = "M";
            p = p.substring(1);
        } else {
            key = "N";
        }

        // numbering of verses
        if (p.matches("[1-9]\\.")) {
            return p + "& ";
        }

        int columnCount = getColumnCount(p);
        if (columnCount == p.length() && "N".equals(key) && p.endsWith("*")) {
            latex.append(p.replaceAll("\\*", "& "));
        } else {
            if (p.startsWith("*") /* && !"M".equals(key) */ && !"D".equals(key)) {
                key += "R";
            }

            p = p.replaceAll("^\\**", "");
            p = p.replaceAll("\\**$", "");
            p = p.replaceAll("\\*\\*", " ");
            p = p.replaceAll("_", " ");
            p = p.replaceAll(">", "\\\\hfill ");

            if (symbolLine) {
                latex.append(getSymbolLatex(key, columnCount, p));
            } else {
                if (columnCount > 4) {
                    latex.append(text2latex.get(key)[4]).append(columnCount).append("}{");
                } else {
                    latex.append(text2latex.get(key)[columnCount - 1]);
                }

                latex.append(p.replaceAll("\\*", " "));
                if (key.endsWith("R") || latex.toString().contains("\\hfill")) {
                    latex.append("\\,");
                }
                latex.append("} ");
            }
        }

        return latex.toString();
    }

    static String getSymbolLatex(String key, int columnCount, String text) {
        StringBuilder latex = new StringBuilder();

        char align = 'L';
        if (key.length() == 2) {
            align = key.charAt(1);
        }

        if (columnCount > 1) {
            latex.append("\\multicolumn{").append(columnCount).append("}{L}");
            latex.append("{").append(symbols.get(key.charAt(0)));
            if (align == 'R') {
                latex.append("\\hfill");
            }
            latex.append(getSymbol(text)).append("}");
        } else {
            latex.append(symbols.get(key.charAt(0)));
            if (align == 'R') {
                latex.append("\\hfill");
            }
            latex.append(getSymbol(text));
        }

        return latex.toString();
    }

    static String getSymbol(String key) {
        if (Symbol2Latex.symbols.containsKey(key)) {
            return Symbol2Latex.symbols.get(key);
        } else {
            return "\\sign{" + key + "}";
        }
    }

    static int getColumnCount(String part) {
        int count = 0;
        boolean word = false;

        for (char c : part.trim().toCharArray()) {
            if (c == '*') {
                count++;
                word = false;
            } else if (!word) {
                count++;
                word = true;
            }
        }
        return count;
    }

    public void setSymbolLine(boolean symbolLine) {
        this.symbolLine = symbolLine;
    }
}
