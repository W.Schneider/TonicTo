package de.jm.tonic;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static de.jm.tonic.Text2Latex.getColumnCount;
import static java.lang.Math.abs;

/**
 * Created by johann on 16.07.17.
 *
 */

public class Notes2Latex {

    private Pattern quarterPattern = Pattern.compile("(!!|!|;|:|\\*)[^!;:$]*");

    String lexNotes(String line) {
        StringBuilder latex = new StringBuilder();
        List<String> quarters = splitIntoQuarters(line);

        for (String quarter : quarters) {
            List<String> notes = lexQuarter(quarter);
            for (String latexNote : notes) {
                latex.append(latexNote);
            }
        }

        return latex.toString();
    }


    private static Pattern typePattern = Pattern.compile("^[!;:./,]+");
    static String noteType(String note) {
        Matcher matcher = typePattern.matcher(note);
        String type = "";
        if (matcher.find()) {
            type = matcher.group();
        }
        return type;
    }

    private static Map<Integer, String> columnCountMap = new HashMap<>();
    static {
        columnCountMap.put(0, "");
        columnCountMap.put(1, "");
        columnCountMap.put(2, "d");
        columnCountMap.put(3, "t");
        columnCountMap.put(4, "f");
    }

    public static String lexNote(String note) {
        StringBuilder latexNote = new StringBuilder("\\");

        String n = note.trim();
        if (n.matches("\\**")) {
            return n.replaceAll("\\*", "&") + " ";
        }

        String type = noteType(n);
        n = n.replaceFirst(type, "");


        latexNote.append(columnCountMap.getOrDefault(getColumnCount(n), "mc"));
        latexNote.append(typeToLatex.get(type));

        n = n.replaceAll("\\*", "");

        if (n.contains("+")) {
            // 2 notes in one
            latexNote.append("s");
            return subNotes2latex(n, latexNote);
        }

        if (!n.startsWith("-") && !n.endsWith("-") && n.contains("-") && n.length() > 2) {
            String keyChange = keyChange2latex(n, type);
            if (!keyChange.isEmpty()) {
                return keyChange;
            }
        }

        int pitch = getPitch(n);
        if ("-".equals(n)) {
            return latexNote.append("c ").toString();
        } else if (isHigh(n)) {
            n = n.replace("'", "");
            latexNote.append("h");
        } else if(isLow(n)) {
            n = n.replace(",", "");
            latexNote.append("l");
        } else {
            latexNote.append("n");
        }

        if (pitch > 1 || pitch < -1) {
            if (isUnderline(n) || isUnderpoint(n)) {
                latexNote.append("p");
            }
            latexNote.append("[").append(abs(pitch)).append("]");
        }

        if (isUnderline(n)) {
            latexNote.append("u[");
            latexNote.append(getUnderLength(n));
            latexNote.append("]");
            n = n.replaceAll("_[0-9.]*", "");
        } else if (isUnderpoint(n)) {
            latexNote.append("p[");
            latexNote.append(getUnderLength(n));
            latexNote.append("]");
            n = n.replaceAll("=[0-9.]*", "");
        }

        latexNote.append("{");
        latexNote.append(n);
        latexNote.append("} ");

        return latexNote.toString();
    }


    private static Map<String, String> accentToLatex = new HashMap<>();
    static {
        accentToLatex.put("!!", "\\ds");
        accentToLatex.put("!", "\\ms");
        accentToLatex.put(";", "\\as");
        accentToLatex.put(":", "\\ns");
    }

    static String keyChange2latex(String note, String type) {
        StringBuilder result = new StringBuilder();
        result.append(accentToLatex.getOrDefault(type, ""));

        String[] notes = note.split("-");
        if (notes.length == 2) {
            result.append("\\hit{").append(getPlainNote(notes[0]));
            result.append("}");
            result.append(getPlainNote(notes[1]));
            result.append("&");
            return result.toString();
        } else {
            return "";
        }
    }

    public static String getPlainNote(String note) {
        StringBuilder latex= new StringBuilder();
        String replace = "";

        if (isHigh(note)) {
            latex.append("\\hi");
            replace = "'";
        } else if (isLow(note)) {
            latex.append("\\lo");
            replace = ",";
        } else {
            latex.append(note);
        }

        int pitch = abs(getPitch(note));

        if (pitch > 1) {
            latex.append("[").append(pitch).append("]");
        }

        if (pitch > 0) {
            latex.append("{").append(note.replaceAll(replace, "")).append("}");
        }


        return latex.toString();
    }

    static String subNotes2latex(String note, StringBuilder latexNote) {
        latexNote.append("\\pn{");
        String[] subNotes = note.split("\\+");
        for (String subNote : subNotes) {
            String type = noteType(subNote);
            if (!type.equals("")) {
                latexNote.append(typeToLatexSymbol.get(type)).append("{");
                subNote = subNote.replaceFirst(type, "");
            }

            latexNote.append(getPlainNote(subNote));

            if (!type.equals("")) {
                latexNote.append("}");
            }
        }
        latexNote.append("}");
        return latexNote.toString().replaceAll(" ", "") + " ";
    }

    // s/.*_\([0-9.]\+\)/\1
    // d_2 d_2.5
    private static Pattern underLengthPattern = Pattern.compile("[_=]([0-9.]+)");
    private static String getUnderLength(String note) {

        Matcher matcher = underLengthPattern.matcher(note);

        String length = "1";
        while (matcher.find()) {
            length = matcher.group(1);
        }
        return length;
    }


    private static Map<String, String> typeToLatex = new HashMap<>();
    static {
        typeToLatex.put("!!", "d");
        typeToLatex.put("!", "m");
        typeToLatex.put(";", "a");
        typeToLatex.put(":", "n");
        typeToLatex.put(".,", "hq");
        typeToLatex.put(",,", "hhq");
        typeToLatex.put("//", "tt");
        typeToLatex.put(".", "h");
        typeToLatex.put("/", "t");
        typeToLatex.put(",", "q");
        typeToLatex.put("", "p");
    }

    private static Map<String, String> typeToLatexSymbol = new HashMap<>();
    static {
        typeToLatexSymbol.put("!!", "\\ds");
        typeToLatexSymbol.put("!", "\\ms");
        typeToLatexSymbol.put(";", "\\as");
        typeToLatexSymbol.put(":", "\\ns");
        typeToLatexSymbol.put(".,", "\\hs\\qs");
        typeToLatexSymbol.put(",,", "\\hs\\qs");
        typeToLatexSymbol.put("//", "\\ts\\ts");
        typeToLatexSymbol.put(".", "\\hs");
        typeToLatexSymbol.put("/", "\\ts");
        typeToLatexSymbol.put(",", "\\qs");
    }

    private static boolean isHigh(String n) {
        return n.contains("'");
    }

    private static int getPitch(String note) {
        int pitch = 0;
        for (char c : note.toCharArray()) {
            if (c == '\'')  {
                pitch++;
            } else if (c == ',') {
                pitch--;
            }
        }
        return pitch;
    }

    private static boolean isLow(String n) {
        return n.contains(",");
    }

    private static boolean isUnderline(String n) {
        return n.contains("_");
    }
    private static boolean isUnderpoint(String n) {
        return n.contains("=");
    }

    List<String> splitIntoQuarters(String line) {
        List<String> quarters = new ArrayList<>();
        String l = line.replaceAll(" +", " ");

        Matcher matcher = quarterPattern.matcher(l);

        while (matcher.find()) {
            String group = matcher.group();
            quarters.add(group);
        }

        return quarters;
    }

    private Pattern notePattern = Pattern.compile("(!!|!|;|:|\\.,|\\.|/|,|\\*)([^ $]*)");
    List<String> lexQuarter(String quarter) {
        List<String> latexNotes = new ArrayList<>();

        Matcher matcher = notePattern.matcher(quarter);

        while (matcher.find()) {
            String note = matcher.group();
            latexNotes.add(lexNote(note));
        }

        return latexNotes;
    }

    List<String> splitQuarter(String quarter) {
        List<String> notes = new ArrayList<>();

        Matcher matcher = notePattern.matcher(quarter);

        while (matcher.find()) {
            String note = matcher.group();
            notes.add(note);
        }

        return notes;
    }

    boolean isQuarter(String note) {
        return note.startsWith(",");
    }

    boolean isThird(String note) {
        return note.startsWith("/");
    }

    boolean isThirdThird(String note) {
        return note.startsWith("//");
    }

    boolean isHalfQuarter(String note) {
        return note.startsWith(".,");
    }

    boolean isHalf(String note) {
        return !isHalfQuarter(note) && (note.startsWith(".") || note.startsWith(",,"));
    }
}

