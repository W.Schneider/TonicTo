package de.jm.tonic;

/**
 * Created by Wolfgang Schneider on 01.09.2017.
 *
 */
class SymbolLine extends SongLine {
    SymbolLine(String l) {
        super(l);
    }

    public String toString() {
        return Symbol2Latex.toLatex(line, true);
    }

    public String toLatex(boolean firstSymbolLine) {
        return Symbol2Latex.toLatex(line, firstSymbolLine);
    }
}
