package de.jm.tonic.parser;

import de.jm.tonic.model.TSFNote;
import de.jm.tonic.model.TSFNote2MidiNote;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class QuarterIterator  {
    private ListIterator<TSFNote> iter;
    public QuarterIterator(List<TSFNote> tsfNotes) {
        iter = tsfNotes.listIterator();
    }

    public List<TSFNote> next() {
        List<TSFNote> tsfNotes = new ArrayList<>();
        int remainingLength = TSFNote2MidiNote.toLength(TSFNote.LENGTH.FULL);

        while (iter.hasNext() && remainingLength > 0) {
            TSFNote note = iter.next();
            if (!note.isMeta()) {
                int length = TSFNote2MidiNote.toLength(note.getLength());
                tsfNotes.add(note);
                remainingLength -= length;
            }
        }

        return tsfNotes;
    }

    public boolean hasNext()  {
        return iter.hasNext();
    }

}