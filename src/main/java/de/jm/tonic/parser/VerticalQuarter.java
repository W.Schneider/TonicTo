package de.jm.tonic.parser;

import de.jm.tonic.model.TSFNote;

import java.util.ArrayList;
import java.util.List;

public class VerticalQuarter {

    public static int FULL = 0b100000;
    public static int SECONDQUARTER = 0b010000;
    public static int SECONDTHIRD = 0b001000;
    public static int HALF = 0b000100;
    public static int THIRDTHIRD = 0b000010;
    public static int FOURTHQUARTER = 0b000001;
    public static int LEFTFULL = 0b1000000;

    private List<List<TSFNote>> notes = new ArrayList<>();

    public VerticalQuarter() {
    }

    public List<List<TSFNote>> getNotes() {
        return notes;
    }

    public void add(List<TSFNote> tsfNotes) {
        notes.add(tsfNotes);
    }

    public int getWidth() {
        StringBuilder widthString = new StringBuilder();
        int width = 0;

        for (List<TSFNote> quarterNotes : notes) {
            int size = quarterNotes.size();
            if (size == 0) {
                continue;
            }
            TSFNote first = quarterNotes.get(0);
            TSFNote last = quarterNotes.get(size - 1);
            TSFNote.LENGTH firstLength = first.getLength();
            TSFNote.LENGTH lastLength = last.getLength();

            if (firstLength == TSFNote.LENGTH.QUARTER) {
                widthString.append("QQ");
                width |= (FULL | SECONDQUARTER);
            } else if (firstLength == TSFNote.LENGTH.HALF || firstLength == TSFNote.LENGTH.THREEQUARTES) {
                widthString.append("H");
                width |= (FULL | HALF);
            } else if (firstLength == TSFNote.LENGTH.THIRD || firstLength == TSFNote.LENGTH.TWOTHIRDS) {
                widthString.append("TTT");
                width |= (FULL | SECONDTHIRD | THIRDTHIRD);
            } else {
                width |= FULL;
            }

            if (",,".equals(last.getPrefix())) {
                widthString.append("H");
                width |= HALF;
            } else if (quarterNotes.size() == 2 && first.getPostfix().equals("+")) {
                return FULL;
            } else if (lastLength == TSFNote.LENGTH.QUARTER) {
                widthString.append("QQ");
                width |= (HALF | FOURTHQUARTER);
            } else if (lastLength == TSFNote.LENGTH.HALF) {
                widthString.append("H");
                width |= HALF;
            }
        }
        return width;
    }

    public static List<VerticalQuarter> asQuarterList(List<Voice> tsfNotes) {
        List<VerticalQuarter> quarterList = new ArrayList<>();
        List<QuarterIterator> iterators = new ArrayList<>();

        for (List<TSFNote> voice : tsfNotes) {
            iterators.add(new QuarterIterator(voice));
        }

        if (iterators.isEmpty()) {
            return quarterList;
        }

        while (iterators.get(0).hasNext()) {
            VerticalQuarter quarter = new VerticalQuarter();
            for (QuarterIterator iter : iterators) {
                quarter.add(iter.next());
            }
            quarterList.add(quarter);
        }

        return quarterList;
    }

    @Override
    public String toString() {
        return "VerticalQuarter{" +
                "notes=" + notes +
                '}';
    }
}
