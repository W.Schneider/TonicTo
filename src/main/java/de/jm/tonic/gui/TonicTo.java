package de.jm.tonic.gui;

import de.jm.tonic.latex.TSFFile2Latex;
import de.jm.tonic.midi.MidiPlayer;
import de.jm.tonic.model.SongModel;
import de.jm.tonic.midi.SongParser;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiUnavailableException;
import javax.swing.*;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * Created by Wolfgang Schneider on 22.06.2017.
 *
 */
public class TonicTo {
    public static void main(String[] args) throws Exception {

        if (args.length >= 2) {
            if ("-p".equals(args[0])) {
                for (int i = 1; i < args.length; i++) {
                    String tsfFile = args[i];
                    System.out.println("Playing " + tsfFile);
                    List<String> rows = Files.readAllLines(Paths.get(tsfFile), StandardCharsets.UTF_8);
                    SongParser parser = new SongParser();
                    SongModel model = parser.parse(rows.listIterator());
                    MidiPlayer player = new MidiPlayer();
                    player.playSongModel(model);
                }
            } else if ("-w".equals(args[0])) {
                String targetDir = getTargetDir(args);
                for (int i = 1; i < args.length; i++) {
                    String tsfFile = args[i];
                    if (!isTsfFile(tsfFile)) {
                        continue;
                    }
                    System.out.println("Writing " + tsfFile);
                    List<String> rows = Files.readAllLines(Paths.get(tsfFile), StandardCharsets.UTF_8);
                    SongParser parser = new SongParser();
                    SongModel model = parser.parse(rows.listIterator());
                    MidiPlayer player = new MidiPlayer();
                    player.writeSongModel(model, getMidiFile(tsfFile, targetDir));
                }
            } else {
                String infile = args[0];
                String outfile = args[1];
                String latex;
                boolean completeDocument = true;
                if (args.length > 2) {
                    completeDocument = false;
                }

                latex = TSFFile2Latex.toLatex(infile, completeDocument);

                try (Writer writer = new BufferedWriter(new OutputStreamWriter( new FileOutputStream(outfile), StandardCharsets.UTF_8))) {
                    writer.write(latex);
                }

            }
        } else {
            //Schedule a job for the event-dispatching thread:
            //creating and showing this application's GUI.
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    //Display the window.
                    JFrame frame = new TonicFrame();
                    frame.setVisible(true);
                }
            });
        }
    }

    private static String getTargetDir(String[] args) {
        for (String arg : args) {
            if (!isTsfFile(arg) && arg.charAt(0) != '-') {
                return arg;
            }
        }
        return null;
    }

    private static boolean isTsfFile(String tsfFile) {
        return tsfFile != null && tsfFile.endsWith(".tsf");
    }
    static File getMidiFile(String tsfFile) {
        return getMidiFile(tsfFile, null);
    }

    static File getMidiFile(String tsfFile, String targetDir) {

        File source = new File(tsfFile);
        String sourceName = source.getName();

        String filename;

        int posOfPoint = sourceName.lastIndexOf(".");
        if (posOfPoint > 1) {
            filename = sourceName.substring(0, posOfPoint);
        } else {
            filename = sourceName;
        }
        if (targetDir == null) {
            return new File(source.getParent(), filename + ".mid");
        } else {
            return new File(targetDir, filename + ".mid");
        }
    }
}
