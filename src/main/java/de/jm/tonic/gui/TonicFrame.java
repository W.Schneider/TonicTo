package de.jm.tonic.gui;

import de.jm.tonic.Tonic2Latex;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;

/**
 * Created by Wolfgang Schneider on 22.06.2017.
 *
 */
class TonicFrame extends JFrame {

    private JTextArea tonicTextArea;
    private JTextArea texTextArea;

    TonicFrame() {
        tonicTextArea = new JTextArea();
        texTextArea = new JTextArea("converted tonic sol-fa");
        tonicTextArea.setFont(new Font("monospaced", Font.PLAIN, 12));
        texTextArea.setFont(new Font("monospaced", Font.PLAIN, 12));

        Tonic2Latex tonic2Latex = new Tonic2Latex();
        JButton latexButton = new JButton("latex");
        latexButton.addActionListener(new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                String tonicText = tonicTextArea.getText();
                String texText = tonic2Latex.toLatex(tonicText);
                texTextArea.setText(texText);
                Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
                clpbrd.setContents(new StringSelection(texText), null);
            }
        });

        JButton newButton = new JButton("new");
        newButton.addActionListener(new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                String tonicText = tonicTextArea.getText();
                String texText = tonic2Latex.newLatex(tonicText);
                texTextArea.setText(texText);
                Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
                clpbrd.setContents(new StringSelection(texText), null);
            }
        });

        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());

        JPanel centerPanel = new JPanel();
        centerPanel.setLayout(new GridLayout(1,2, 5, 10));

        centerPanel.add(new JScrollPane(tonicTextArea));
        centerPanel.add(new JScrollPane(texTextArea));

        JPanel buttonPanel = new JPanel();

        buttonPanel.add(latexButton);
        buttonPanel.add(newButton);

        mainPanel.add(buttonPanel, BorderLayout.SOUTH);
        mainPanel.add(centerPanel, BorderLayout.CENTER);

        getContentPane().add(mainPanel);

        setTitle("Tonic To");
        setSize(1000, 600);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }
}
