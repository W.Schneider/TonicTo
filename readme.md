# Intro
TonicTo can be used to translate a song in "Tonic Sol-Fa" notation to a latex file (luatex).
It can also be used to play and create a midi file from that song. 

## Usage
1. "java -jar TonicTo.jar" will open the GUI version
2. "java -jar TonicTo.jar <tsffile>" will create the latex file
3. "java -jar TonicTo.jar -p <tsffile> <tsffile> ..." will play the song(s)
4. "java -jar TonicTo.jar -w <tsffile> <tsffile> ..." will create midi file(s)


## TSF-file format

### Page
left:, right:, top:, bottom:, landscape:

### Verse
verse:

if there is no empty line between two verses, the first will be left and the second will be on the right half of page

    verse: 1. verse to the left
    second line
    verse: 2. verse to the right
    second line
    
    verse: 3. verse on new line

### Note lines
two notes in one column: :d+r (needed for formatting)

double note: d* (needed for formatting)

Questionmarks are ignored

Triplets are separated with /

key change: - 

_<float> underline number of notes

=<float> underpoint number of notes

,, is ., in a half column  (HQQ - HH)

### voice line
v: Ssatbmf

### Text lines

sign | meaning
:-----|:--------
*text | right aligned to column
text* | left aligned to column
\* | empty column
~ | mark as symbol line
! | measure
!! | double measure
\> | filler  (>text --> left aligned to column)
_ | Space
\1. 2. ... | numbering of verses (column added)

Add verse numbers to text lines:

    * :d
    1. hello
    2. hello

### Information lines
An information line start with one of the following keys.

Note Line = "N:"

Songtext Line = "w:"

Key Line = "K:"

Composer Line = "C:"

Language Line = "L:"

Title Line = "T:"

SongNumber Line = "X:"

Transcription Line = "Z:"

Comment Line = "c:"

SetQLength Line = "sql:"

bpm = "bpm:" - set bpm for playing


### Symbol line
A line with symbols starts with s:
If a ~ is contained in a TextLine it will be treated as a symbol line.
Following symbols are recognized:

sign | meaning
-----|--------
cresc | crescendo
decresc | decrescendo
refrain | Refrain
solo | Solo
^ | fermata sign
% | segno sign
$ | coda sign
ds | dal segno
dc | da capo
DS | dal segno (DS^*7 : box 7 quarter lengths with fermata left aligned and dal segno on right aligned)
DC | da capo (see DS)
\1. | first repeat
\2. | second repeat
tfb | box of width of a quarter
*number | box of width of a quarter
tfub | underlined box of width of a quarter
*number_ | box of width of a quarter
f | forte
mf | mezzo forte
ff | forte fortissimum
p | piano
pp | piano pianissimum
ppp | ppp
\> | filler (right>left*7 box 7 quarter length right right aligned and left left aligned)
number (pt/em) | space of number point/sign length
_ | space

# Latex file
* Currently needs luatex to create pdf.
* Needs latexmk, texlive-latex-recommended, texlive-extra-utils, texlive-pstricks and texlive-latex-extra
* TimesNewRoman from Windows
* needs tonic.sty (https://gitlab.com/tonic-solfa/latex/blob/master/tonic.sty) and a template song.sty (e.g https://gitlab.com/W.Schneider/Lieder/blob/master/song.sty)
